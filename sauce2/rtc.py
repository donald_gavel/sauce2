'''rtc.py - real-time controller
Part of the sauce2 simulator
'''
import os
import sys
from oprint import pprint
import numpy as np
from info_array import InfoArray, ufloat, play
import fits
import ConfigParser
from dotdict import typize
import tqdm

__path__ = os.path.dirname(__file__)
configfile = os.path.join(__path__,'rtc.cfg')
paramDir = os.path.join(os.path.dirname(__file__),'parameterFiles')

class Rtc(object):
    '''Real Time Control object
    '''
    def __init__(self,mode='16x'):
        cp = ConfigParser.ConfigParser()
        cp.optionxform = str
        cp.read(configfile)
        rtc_hdr = typize(dict(cp.items('rtc')))
        self.__dict__.update(rtc_hdr)
        self.mode = mode
        rtc_mode_info = typize(dict(cp.items(mode)))
        self.__dict__.update(rtc_mode_info)
        self.centroider = 'COG'
        self.na = 1024 + 52 + self.nf
        self.na_doc = 'total number of actuators (woofer + tweeter)'
        self.imap = fits.readfits(os.path.join(paramDir,self.imap_file))
        self.umap = fits.readfits(os.path.join(paramDir,self.umap_file))
        self.mapi_to = self.umap.astype(np.int32)
        self.mapi_to_doc = 'map to pixels in CCD array'
        self.n_subaps_doc = 'number of hartmann subaps'
        self.a_bias = fits.readfits(os.path.join(paramDir,self.act_bias_file))
        self.a_bias_doc = 'actuator offsets'
        self.a_lim = fits.readfits(os.path.join(paramDir,self.act_limits_file))
        self.a_lim_doc = 'actuator limits'        
        self._recon_prep()
    
    def pprint(self):
        pprint(self)

    def __getattr_(self,name):
        if name in ('hdr','header'):
            self.pprint()
        
    def _recon_prep(self,centroider='COG',nt=4096):
        """Prepare the reconstructor by setting up centroider weights
        and a data recording system
        
        :param str centroid: centroider, either 'COG', 'BIN' or 'QUAD'
        :param int nt: the maximum number of time steps allowed in data recording. The ShaneAO real-time system allows up to 4096
        """
        if centroider.upper() == 'COG':
            xvec = [-1.5,-0.5,0.5,1.5]
        elif centroider.upper() == 'BIN':
            xvec = [-1,-1,0.,0.]
        elif centroider.upper() == 'QUAD':
            xvec = [0.,-1,0.,0.]
        else:
            raise Exception, 'centroider %s not supported'%centroider
        ivec = [1.,1.,1.,1.]
        self.wx = np.block([xvec+[0]]*4+[[0]*5])
        self.wx_doc = 'centroider x weights'
        self.wy = self.wx.transpose()
        self.wy_doc = 'centroider y weights'
        self.wi = np.block([ivec+[0]]*4+[[0]*5])
        self.wi_doc = 'centroider intensity weights'
        self.s = np.matrix(np.zeros(self.n_subaps*2)).T
        self.s_doc = 'wfs slopes vector'
        self.r = 5.
        self.r_doc = 'centroider regulizer (prevents div by zero)'
        self.ns = self.n_subaps*2
        self.ns_doc = 'total number of sensor values (2 x #subaps + tt)'
        self.R = np.matrix(np.zeros((self.na,self.ns)))
        self.R_doc = 'slopes-to-actuators reconstructor matrix'
        self.da = np.zeros((self.na))
        self.da_doc = 'the increment to actuators in the integral feedback control'
        self.a = np.zeros((self.na))
        self.a_doc = 'actuator values'
        self.gain = 1.0
        self.gain_doc = 'feedback gain'
        self.igain = 0.99
        self.igain_doc = 'integrator gain'
        nt = nt + 1 # always one additional 'bit bucket' bogus value
        self.centroider = centroider
        if self.mode == '16x':
            self.telem_size = (nt,1382)
        elif self.mode == '8x':
            self.telem_size = (nt,1174)
        self.telem = np.zeros(self.telem_size)
        self.telem_index = 0
        self.telem_increment = 0
        self.loop = 'open'
        self.loop_doc = 'AO loop state'
        self.ttloop = 'open'
        self.ttloop_doc = 'AO Tip/Tilt loop state'
        self.rate = 1000
        self.rate_units = 'Hz'
        self.rate_doc = 'WFS frame rate'    
    
    def recon(self,ccd):
        '''run the real-time reconstructor
        Given CCD camera WFS image, compute the DM commands
        '''
        if isinstance(self.telem,InfoArray):
            self.telem = np.array(self.telem)
        n_subaps, n_p, wx, wy, wi,r = self.n_subaps, self.n_p, self.wx, self.wy, self.wi, self.r
        q = np.empty((n_subaps,n_p,n_p)).astype(np.uint16)
        q.flat = ccd.flat[self.mapi_to]
        q = q.astype(float)
        inten = np.clip(np.sum(q*wi,(1,2)),0.,None)+r
        sx = np.sum(q*wx,(1,2))/inten
        sy = np.sum(q*wy,(1,2))/inten
        self.s.flat[:] = np.block([sx,sy])
        da = self.R*self.s
        self.da[:] = np.squeeze(np.asarray(da))
        self.a[:] = self.igain*(self.a-self.a_bias) + self.gain*self.da + self.a_bias
        np.clip(self.a,self.a_lim[0,:],self.a_lim[1,:],self.a)
        
        self.tweeter.actuate(self.a)
        if self.woofer:
            aw = self.a[1024:1024+52]
            self.woofer.actuate(aw)
        
        self.telem[self.telem_index,0:self.ns] = np.squeeze(np.asarray(self.s))
        self.telem[self.telem_index,self.ns:self.ns+self.na] = self.a
        self.telem_index += self.telem_increment
        if self.telem_index >= self.telem_size[0]:
            self.telem_increment = 0
            self.telem_index = 0
    
    def connect(self,tweeter,woofer=None):
        self.tweeter = tweeter
        self.woofer = woofer
    
    def calibrate(self):
        '''run the calibration procedure,
        then calculate the real-time controller reconstruction matrix
        '''
        pass

import wfs
import dm
if __name__ != 'rtc':
    from rtc import Rtc
from info_array import InfoArray, play
from atmos import BlowingScreen
from common_units import units,u,ufloat,as_ufloat
import numpy as np
import tqdm

def test_simulation(n=10,show=False):
    global wfs,dm,atmos
    global r
    global wfs_hist, ph_hist
    
    w = wfs.Wfs()
    tw = dm.Tweeter(w.du,w.d/2.)
    r = Rtc()
    r.tweeter = tw
    r.woofer = None
    ccd = w.ccd
    lam0 = ufloat(0.5,'microns')
    dt = ufloat(10.,'ms')
    L0 = float('Infinity')
    r0 = ufloat(10.,'cm')
    
    b = BlowingScreen(dt=dt,v=10,tmax=4.,name='blowing screen',
                      n=w.N,du=w.du,r0=r0,L0=L0,lam0=lam0)
    
    w.ccd_image_gpu_prep()
    ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
    ph0 = wfs.tilt(w.ap,ttgpu,w.wavelength).to_units(b.screen.units)
    r.telem_increment = 1
    assert tw.surface.units == b.screen.units == ph0.units
    ph_hist = []
    wfs_hist = []

    for k in tqdm.tqdm(range(n)):
        ph = b.next(ap=False)
        ph = ph - tw.surface + ph0
        ph_hist.append(ph)
        w.ccd_image_gpu(ph.to_units('radians',w.wavelength).astype(np.float32))
        wfs_hist.append(w.ccd.copy())
        r.recon(w.ccd)

    if show:
        play(ph_hist)
        play(wfs_hist)
