"""
Deformable mirrors module.
"""

import os
import sys
import numpy as np
import img
from info_array import InfoArray, InfoMatrix, ufloat
import fits
import pyfits
import oprint
from common_units import *
import ConfigParser
import tqdm
import matplotlib.pyplot as plt
import matplotlib.patches as patches

greek_lambda = u'\u03BB'
greek_mu = u'\u03BC'
micron_units = greek_mu+'m'
        
class DM(object):
    """
    Base Deformable mirror class
    """
    def __init__(self,dx=0.01):
        self.dx_fine = dx
        self.dx_fine_units = 'meters'
        self.dx_fine_doc = 'fine grid sampling (aperture plane)'
        
    def pprint(self,**kwargs):
        """Pretty-print the object's instance variables and descriptions
        """
        oprint.pprint(self,**kwargs)
    
    def __getattr_(self,name):
        if name in ('hdr','header'):
            self.pprint()
    
class Tweeter(DM):
    """
    Tweeter DM
    """    
    def __init__(self,dx=0.01,d=0.1,padded=False):
        """create a Tweeter deformable_mirror object
        specific to the Boston Micromachines 32x32 DM
        
        To get this aligned like ShaneAO;
        
        .. code-block:: python
        
            w = wfs.Wfs()
            tw = Tweeter(dx=w.du, d=w.d/2.)
        
        """
        super(Tweeter,self).__init__(dx=dx)  
        self.name = 'BMC KiloDM'
        self.n_across = 32
        self.n_across_doc = 'number of actuators across'
        self.nacts = 1024
        # ----- Tweeter physical dimensions -----
        self.da = 340.
        self.da_units = micron_units
        self.da_doc = 'actuator pitch, physical'
        self.actDNRange = [0,65535]
        self.actDNRange_units = 'DN'
        self.actDNRange_doc = 'actuator command range'
        self.lim = np.kron(np.ones(self.nacts),self.actDNRange)
        self.stroke = 3.5
        self.stroke_units = micron_units
        self.stroke_doc = 'surface deflection peak-to-valley'
        self.nm_per_DN = (self.stroke / float(self.actDNRange[1]-self.actDNRange[0]))*(units.microns/units.nanometers)
        self.nm_per_DN_units = 'nm'
        self.d = d
        self.d_units = 'meters'
        self.d_doc = 'actuator spacing at telescope aperture'
        self.magnification = self.d*units.meters / (self.da*units.microns)
        self.magnification_doc = 'pupil (de)magnification ratio'
        # ------ Fine surface grid -------
        self.padded = padded
        pad = 0
        if padded:
            pad = 1.
        self.n_fine = img.nextpow2(int(np.round((self.n_across+pad)*self.d / self.dx_fine)))
        ph = np.zeros((self.n_fine,self.n_fine))
        self.surface = InfoArray(ph,
                            name='Tweeter surface',
                            dx = self.dx_fine,
                            dx_units = self.dx_fine_units,
                            units = 'nanometers')
        # ----- Locations of the actuators -----
        x = (np.arange(0,self.n_across) - self.n_across/2)*self.d/self.dx_fine + self.n_fine/2
        y = x
        actLocs = []
        for xx in x:
            for yy in y:
                actLocs.append((xx,yy))
        self.actLocs = np.array(actLocs)
        self.actLocs_units = 'fine pixels'
        n = self.n_fine
        self.actLocsIndices = list(self.actLocs[:,0]*n + self.actLocs[:,1])
        self.actLocsIndices_units = 'fine pixels'
        self.actLocsIndices_doc = '1d index of actLocs into dm surface array'
        self._defineTweeterPoke()
        
    def show(self,on=None):
        '''show the DM and actuator locations in a graphic.
        An option is to put the display on top of another
        object display, such as the wavefront sensor.
        
        :param object on: The object on top of which to draw the tweeter display. It must have a :meth:`show` method

        .. image:: Tweeter_acts.png
           :scale: 50%

        **Figure** shown on top of a :class:`~wfs.Wfs` display
                
        '''
        if on is None:
            a = self.surface.copy()
            a.name = 'Tweeter actuator locations'
            a.units = ''
            a *= 0.
            n,m = a.shape
            x = int(np.round((self.n_across/2.)*self.d/self.dx_fine))
            i1,i2 = n/2-x, n/2+x
            a[i1:i2,i1:i2] = 1.
            a.show()
        else:
            on.show()
            
        ax = plt.gca()
        d = self.n_across*self.d
        s = self.d/2.
        ax.add_patch(patches.Rectangle((-d/2-s,-d/2-s),d,d,fill=False,edgeColor='red'))
        du = self.dx_fine
        n = self.n_fine
        for x,y in self.actLocs:
            plt.plot([(x-n/2)*du],[(y-n/2)*du],marker='o',markersize=2,color='red')
        plt.draw()
        
    def _defineTweeterPoke(self,sigma=1.4):
        """generate a generic poke for the tweeter
        on the fine grid
        """
        n = self.n_fine
        a = self.d
        poke = np.zeros((n,n))
        x = (np.arange(n) - n/2)*self.dx_fine
        x,y = np.meshgrid(x,x)
        r2 = x**2 + y**2
        s2 = (sigma*a)**2
        poke = -np.sinc(x/a)*np.sinc(y/a)*np.exp(-r2/s2)
        self.influenceFcn = InfoArray(poke,
                              name = 'Tweeter poke',
                              dx = self.dx_fine,
                              dx_units = self.dx_fine_units)
        poke_f = img.ft(poke)
        self.influenceFcn_f = InfoArray(poke_f,
                                    name = 'FT of Tweeter poke')

    def actuate(self,a):
        '''actuate the deformable mirror given a list
        of actuator voltages to apply. The surface of the DM
        is deformed according to actuator response functions
        
        :param list a: actuator commands
        
        '''
        self.surface.flat[:] = 0.
        self.surface.flat[self.actLocsIndices] = a[0:self.nacts]
        self.surface[:] = img.ftconvolve(self.surface,self.influenceFcn_f,space='sfs').real
    
    def write(self,a_v=None):
        """
        Generate the wavefront phase on the fine grid
        given the set of actuator commands, a_v
        
        :param np.array a_v: actuator commands in a 32 x 32 numpy array, given in the natural units of the DM driver, ranging from 0 to 65535
        
        :ivar InfoArray surface: the result is stored in the instance variable **surface**, a fine grid at **dx** sampling, in units of nanometers
            
        """
        if a_v is None:
            a_v = self.a
        
        if isinstance(a_v,list):
            a_vec = np.array(a_v).astype(float) # this makes a copy
        else:
            a_vec = a_v.flatten().astype(float) # this makes a copy
            
        assert len(a_vec) == self.nacts
        a_vec *= self.nm_per_DN
    
        n = self.n_fine
        u = np.zeros((n,n))
        x0 = n/2
        # ========== GPU function goes here ===========
        for x,a in zip(self.actLocs,a_vec):
            ix,iy = np.array(x) + x0
            if (ix>=0 and ix<n) and (iy>=0 and iy<n):
                u[ix,iy] = a
        ph_f = np.fft.fft2(u)*self.influenceFcn_f
        self.surface[:] = np.real(np.fft.ifft2(ph_f))
        # =============================================
        if self.sauceSimulator is not None:
            self.sauceSimulator.loop() # this generates the CCD image given the state of the DM
    
    @staticmethod
    def c_write(dpio2_instance,data_a,tweeter_map):
        """function that is called at the end of rtc2.c_recon,
        dpio2_output_function(dpio2_instance,data.a,param.tweeter_map)
        """
        global a_v,check_write_once
        self = Tweeter.theOneTweeter
        woofer = Woofer.theOneWoofer
        if check_write_once:
            log('<c_write> called')
            check_write_once = False
        a_v = np.ctypeslib.as_array(data_a,(1024+52,))
        #Tweeter.theOneTweeter.test_c_write()
        self.write(a_v[0:1024])
        if hasattr(self,'woofer_call'):
            if self.woofer_call:
                a_w = a_v[1024:1024+52].copy()
                woofer.write(a_w)

    def test_c_write(self):
        global check_write_once
        if check_write_once:
            log('<test_c_write> called')
            check_write_once = False
            
class Woofer(DM):
    """
    Woofer DM
    """
    theOneWoofer = None
    
    def __init__(self,sampling=None,verbose=False):
        super(Woofer,self).__init__()
        self.name = 'ALPAO DM52'
        self.nacts = 52
        self.dataDir = os.path.join(os.environ['HOME'],'data')
        self.pokesFile = os.path.join(self.dataDir,'wooferPokes.fits')
        self.clearAperture = 15.0
        self.clearAperture_units = 'mm'
        self.clearAperture_doc = 'clear aperture diameter of the woofer mirror surface'
        self.bandwidth = 250.
        self.bandwidth_units = 'Hz'
        self.bandwidth_doc = 'specified half-power bandwidth of the woofer, in Hertz. Default 250Hz'
        self.da = 2.5
        self.da_units = 'mm'
        self.da_doc = 'actuator spacing at the woofer surface'
        self.beamSize = 13.4
        self.beamSize_units = 'mm'
        self.beamSize_doc = 'the ShaneAO beam diameter on the woofer'
        self.range = [-1.,1.]
        self.range_units = 'amperes'
        self.range_doc = 'range of valid command for each actuator'
        # map the actuator locations
        self._aloc()
        # zygo calibration data
        self.zygo_pokesFile = os.path.join(self.dataDir,'wooferInfluenceFunctions.fits')
        self.zygo_modesFile = os.path.join(self.dataDir,'wooferModes.fits')
        self.zygo_modeCoefsFile = os.path.join(self.dataDir,'wooferModeCoefficients.fits')
        self.zygo_apFile = os.path.join(self.dataDir,'ap.fits')
        ap = fits.readfits(os.path.join(self.dataDir,self.zygo_apFile))
        n,m = ap.shape
        d = 2*np.sqrt(np.sum(ap)/np.pi) # diameter in pixels, assumes a solid circular disk
        self.zygo_IllumDiam = 14.3
        self.zygo_IllumDiam_units = 'mm'
        self.zygo_IllumDiam_doc = 'the illuminating beam diameter of the zygo beam on the woofer during data collection'
        self.zygo_dx = self.beamSize/d
        self.zygo_dx_units = 'mm'
        self.zygo_dx_doc = 'the fine grid pixel size in the zygo data'
        self.responseGain = -5.1255
        self.responseGain_units = 'microns/ampere'
        self.responseGain_doc = """straight-line fit to measured deflection of single actuator
        calibrated: Norton SPIE8617 (2013) Figure 3"""
        self.stroke = np.dot(np.array(self.range),np.array([-1,1]))*self.responseGain*(microns/nanometer)
        self.stroke_units = 'nanometers'
        self.stroke_doc = 'range of woofer stroke, peak-to-valley'
        amp = 1.0
        poke_units = micron/(0.2*amp) # the zygo measurement is in these units
        self.nm_per_poke = 2*poke_units/nanometers # surface to wavefront and express in nanometers
        # was (wrong): self.responseGain*0.2*(microns/nanometers)
        self.nm_per_poke_units = 'nanometers/poke/Amp'
        self.nm_per_poke_doc = """converts the zygo-measured
        DM surface response to nanometers phase (2xsurface) per Amp."""
        self.offloading = False
        self.offloading_doc = 'offloading excess tip/tilt to the telescope'
        y = np.arange(n)
        x = np.arange(m)
        x,y = np.meshgrid(x,y)
        norm = np.sum(ap)
        cx = np.sum(ap*x)/norm
        cy = np.sum(ap*y)/norm
        self.zygo_d = d
        self.zygo_d_units = 'pixels'
        self.zygo_d_doc = 'aperture diameter, in pixels on original zygo data'
        self.zygo_center = (cy,cx)
        self.zygo_center_units = 'pixels'
        self.zygo_center_doc = 'aperture center in pixels on original zygo data'
        self.zygo_ap = InfoArray(ap,name='aperture on Zygo',
                                 dx = self.zygo_dx,
                                 dx_units = self.zygo_dx_units,
                                 units = '')
        # ----- samples, poke, and surface -----
        self.set_fine_sampling(sampling=sampling)
        # ----- pokes -----
        # search for a properly sampled pokeFile. If none are found, generate one from the raw zygo data
        directory = os.path.join(home,'data')
        keys = ['NAXIS1','NAXIS2','NAXIS3','DX']
        db = flog.pflog(keys=keys,only=True,wildcard='wooferPokes*',directory=directory)
        filename = None
        for index in db.index:
            if np.isclose(db.loc[index].DX,self.dx):
                filename = db.loc[index].FILE
                break
        if filename is None:
            errstr = '<deformable_mirror Woofer..__init__> no wooferPokes file with required sampling, dx='+str(self.dx)
            print(db)
            say.problem(errstr)
            log(errstr,logging.ERROR)
            self.pokesFile = None
        else:
            self.pokesFile = os.path.join(directory,filename)
        
        if (self.pokesFile is not None and os.path.isfile(self.pokesFile)):
            if verbose:
                log('<deformable_mirror Woofer.__init__> reading the woofer poke set from file '+self.pokesFile)
            #self.wooferPokeSet = fits.readfits(self.pokesFile,verbose=verbose)
            #self.wooferPokeSet_units = micron_units # ???
            self.wooferPokeSet_doc = 'interpolated zygo pokes on fine grid'
            self.wooferPokeSet = InfoArray.fromFits(self.pokesFile)
            if 'DX' not in self.wooferPokeSet.header:
                self.wooferPokeSet.dx = 3./(31.-3.)/16. # the original setting from old sauce
                self.wooferPokeSet.dx_units = 'meters'
            try:
                errstr = '<deformable_mirror Woofer.__init__> woofer sampling mismatch: '+str((self.wooferPokeSet.dx,self.dx))
                assert np.isclose(self.wooferPokeSet.dx,self.dx),errstr
            except:
                say.problem(errstr)
                self.pokesFile = None

        if self.pokesFile is None:
            log('<deformable_mirror Woofer.__init__> cannot find file a properly sampled pokesFile',logging.ERROR)
            log('<deformable_mirror Woofer.__init__> reading the woofer Zygo data set and resampling.',logging.ERROR)
            log('<deformable_mirror Woofer.__init__> (this takes a while)',logging.ERROR)
            say.problem('',voice='go?')
            self._readZygoPokes()
            self._resamplePokes(verbose=True)
            # save it for later
            filename = 'wooferPokes_'+('%6.6f' % self.dx).replace('.','p')+'dx.fits'
            filename = os.path.join(directory,filename)
            self.wooferPokeSet.writefits(filename)
            
        assert (self.n_fine,self.n_fine) == self.wooferPokeSet[0].shape
        ph = np.zeros((self.n_fine,self.n_fine))
        # --- surface ---
        self.surface = InfoArray(ph,name='Woofer surface',
                               dx = self.dx,
                               dx_units = self.dx_units,
                               units = 'nanometers')
        # ----- functional interface -----
        Woofer.theOneWoofer = self
    
    def set_fine_sampling(self,sampling=None,**kwargs):
        """calculate all the fine sampling parameters
        
        :param dict sampling: a dictionary with key 'du_fine'
        :param float du_fine: the fine sampling pixel size, meters
        
        **sampling** takes precidence.        
        """
        if sampling is not None:
            self.dx = sampling['du_fine']
        else:
            if 'du_fine' in kwargs:
                self.dx = kwargs['du_fine']
            else:
                self.dx = 0.01

        self._set_samples(self.dx)
        
    def _aloc(self):
        """set the locations of the actuators
        """
        if (not hasattr(self,'alocs')):
            x_list = list(np.arange(-3.5,3.5+1))
            y0_list = list(np.array([3,5,7,7,7,7,5,3])/2.)
            xy0_list = zip(x_list,y0_list)
            alocs = []
            k = 0
            for x,y0 in xy0_list:
                y_list = np.arange(-y0,y0+1)
                for y in y_list:
                    alocs.append((-x,y))
            self.alocs = alocs
        self.alocs_units = 'actuator spacings'
        self.alocs_doc = 'list of x-y locations of the actuators on the DM surface'

    def show(self):
        """depict actuator locations
        
        .. image:: Woofer_acts.png
           :scale: 50%
        """
        n = int(4*max(max(self.alocs)))
        dx = 0.5
        u = np.zeros((n+1,n+1))
        for x,y in self.alocs:
            u[n/2+2*x,n/2+2*y] = 1
        u = InfoArray(u,name='actuator locations',
                      dx = dx,
                      dx_units = 'actuator spacings')
        u.show()
        ax = plt.gca()
        k = 0
        for x,y in self.alocs:
            ax.annotate(str(k),(x-.25,y-.25),
                        horizontalalignment='center',
                        verticalalignment='center')
            k += 1
        plt.draw()

    def _resamplePokes(self,verbose=0):
        """resample the woofer pokes on the fine grid (dx)
        from the Zygo data sampling
        """
        assert hasattr(self,'zygo_rSet')
        n_fine = self.n_fine
        n,m = self.zygo_ap.shape
        c = self.zygo_center
        r = (self.beamSize/self.zygo_dx) / (shane_telescope_diameter/self.dx) * self.n_fine / 2.
        x = np.array(range(n)-c[0])
        y = np.array(range(n)-c[1])
        nx = scipy.linspace(-r,r,n_fine) 
        ny = nx
        wooferPokeSet = []
        k=0
        nz = len(self.zygo_rSet)
        for u in self.zygo_rSet:
            if (verbose):
                log('<deformable_mirror _resamplePokes> %d/%d'%(k,nz))
                sys.stdout.flush()
            k += 1
            spl = scipy.interpolate.RectBivariateSpline(x,y,u)
            ph = spl(nx,ny)
            wooferPokeSet.append(ph)
        u = self.zygo_ap
        spl = scipy.interpolate.RectBivariateSpline(x,y,u)
        ap = spl(nx,ny)
        if (verbose): log('<deformable_mirror _resamplePokes> done')
        self.wooferPokeSet = InfoArray(wooferPokeSet)
        self.wooferPokeSet.dx = self.dx
        self.wooferPokeSet.dx_units = 'meters'
        self.wooferPokeSet.units = self.zygo_rSet_units
        self.wooferPokeSet.doc = 'interpolated zygo pokes on fine grid'
        self.wooferPokeSet_units = self.zygo_rSet_units
        self.wooferPokeSet_doc = 'interpolated zygo pokes on fine grid'

    def _readZygoPokes(self):
        """Gets the influence functions of actuators
        from the zygo calibration files.
        Internal. Called at init time
        """
        r = fits.readfits(self.zygo_pokesFile)
        self.zygo_rSet = list(r)
        self.zygo_rSet_units = 'microns'
        self.zygo_rSet_doc = 'measured pokes of actuators'

    def write(self,a=None):
        """Generate the wavefront phase on the fine grid
        given the set of woofer actuator commands, a
        
        :param list a: a 52-element vector of commands with values between -1 and +1
        """
        assert hasattr(self,'wooferPokeSet')
        if a is None:
            a = self.a
        n = self.n_fine
        crSet = zip(a,self.wooferPokeSet) # influence function set
        #self.surface[:] = 0.
        # ======= GPU function goes here ========
        surface = np.zeros((n,n))
        for c,r in crSet:
            surface += c*r*self.nm_per_poke
        self.surface[:] = surface
        #print '<woofer.write> ',surface.max(), self.surface.max()
        # =======================================
    
    def set_offload(self,arg):
        """Set the offloading flag. This does nothing
        in sauce, but is there remain compatible with the
        real-time woof_connect object.
        """
        self.offloading = arg

def test():
    global tw
    tw = Tweeter()
    tw.pprint()
    tw.write(np.ones((32,32)))
    tw.surface.pprint()

def test2():
    global rtc,rtc2,a,tw,ccd
    
    import rtc as rtc_module
    import time
    
    here = os.getcwd()
    rtc = rtc_module.rtc('16x')
    os.chdir(here)
    rtc2 = rtc_module.rtc2
    a = rtc2.peek('a')
    a_tw = a[0:1024]
    
    tw = Tweeter()
    librtc2 = ctypes.cdll.LoadLibrary('rtc2.so')
    librtc2.set_recon_callback(tw.c_function)
    
    ccd = np.zeros((160,160)).astype(np.uint16)
    t0 = time.time()
    rtc2.recon(ccd)
    t1 = time.time()
    print('time for recon and callback: ',t1-t0,'seconds')
    print('this timing is ~900 microsec')
    print('check: ',(a_tw == a_v).all())
    print('a_v is the argument in c_write() (globalized)')

def test3(n_trials=10):
    global rtc,rtc2,librtc2,a,tw,ccd
    global check_write
    
    import rtc as rtc_module
    import time
    
    here = os.getcwd()
    rtc = rtc_module.rtc('16x')
    os.chdir(here)
    rtc2 = rtc_module.rtc2
    a = rtc2.peek('a')
    a_tw = a[0:1024]
    
    tw = Tweeter()
    librtc2 = ctypes.cdll.LoadLibrary('rtc2.so')
    librtc2.set_recon_callback(tw.c_function)
    
    ccd = np.zeros((160,160)).astype(np.uint16)
    cd = np.ctypeslib.as_ctypes(ccd)
    nulp = ctypes.c_void_p()

    print('------------- Test3.1 -----------')
    for k in range(n_trials):
        check_write_once = True
        t0 = time.time()
        librtc2.c_recon(nulp,cd)
        t1 = time.time()
    print('time for c_recon and callback: ',t1-t0,'seconds')
    print('this is timing at about 420 microsec when dumping printout')
    check_write_once = False
    print('------------- Test3.2 -----------')
    print(' is it faster without the printing?')
    for k in range(n_trials):
        t0 = time.time()
        librtc2.c_recon(nulp,cd)
        t1 = time.time()
    print('time for c_recon and callback: ',t1-t0,'seconds')
    print('time is about 180 microsec when not dumping printout')

    print('------------- Test3.3 -----------')
    print(' is there any difference between recon and c_recon?')
    check_write_once = False
    for k in range(n_trials):
        t0 = time.time()
        rtc2.recon(ccd)
        t1 = time.time()
    print('time for recon and callback: ',t1-t0,'seconds')
    print('time is also about 180 microsec (not dumping printout)')
    print('check: ',(a_tw == a_v).all())
    print('a_v is the argument in c_write() (globalized)')
