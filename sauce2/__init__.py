'''
GPU based AO simulator
'''
#__all__ = ['wfs','atmos','fits','info_array','zernike']
__all__ = ['atmos','oprint','img','dotdict']
from sauce2 import *
def __run_tests__():
    '''runs the tests
    '''
    import sauce2.tests
    sauce2.tests.__run_tests__()

def show():
    '''show off the sauce/GPU capability
    '''
    import sauce2.atmos
    sauce2.atmos.test_bswfs(count=1000,comp='GPU',play=True)
