import os
import sys
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..','..'))
sauce_dir = os.path.join(parent_dir,'sauce2')
sys.path.insert(0, sauce_dir)
