__all__ = ['test_wfs']
from sauce2.tests import *
import pytest
run_arg = ['-v', __path__[0]]
def __run_tests__():
    '''runs all the tests
    '''
    pytest.main(run_arg)
