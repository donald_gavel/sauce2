'''The neural_net module is an experimental use of pytorch :mod:`torch`.
Here we train a neural net to understand the DM commands needed to fit
atsmospheric wavefronts. This "calibration training" example is just one
of many possible uses for machine learning in AO.
Other possibilities include:

    * Forward-predict wavefronts in wind-blown or otherwise evolving turbulence
    * Auto-adjust the feedback controller in closed loop
    * Learn to identify atmospheric parameters (e.g. r0, tau0) from the telemetry data
    * Learn to correct imaging sensor blemishes, such as bad pixels and cosmic rays, in astronomical images
    * Learn the AO point spread function (PSF) given telemetry data and science images

To run the example::

    from sauce2 import neural_net as nn
    nn.main()

Then show some nice graphics of results::

    nn.showra()
    nn.showr2a()
    
To get an idea of the "old-school" (pre neural-nets) way
of calibrating, run::

    nn.show_push_data(nn.gen_push_data(set='push'))
    nn.show_push_data(nn.gen_push_data(set='modes'))

These generate so-called "push" matrices, :math:`H`.
These were then pseudo-inverted to get a linearization
of the control model, using a formula like
:math:`K = \\left( H^T W H + Q \\right)^{-1} H^T W`    
where :math:`W` and :math:`Q` are user-set requlization
matrices, adjusted heuristically to give the best
AO control results in practice.

Advantages of using a
neural net are that the amount of heuristics is minimized
(deciding the amount of training data and
adjusting the training rates is still required),
the control model is optimally fit to the
statistics of turbulence in the training data,
and the model can apply nonlinearities (through the
neural net's activation functions), which
help compensate for
nonlinearities in the wavefront sensor.
'''
import time
import os
import sys
from sauce2.oprint import pprint
from sauce2 import atmos,wfs
from sauce2.common_units import *
from sauce2.info_array import InfoArray,ufloat,play
from sauce2 import img

if 'sphinx' not in sys.modules:
    import torch
    import numpy as np
    import fits
    import tqdm
    import pyfiglet
    import termcolor
    import colorama    
    colorama.init(strip=not sys.stdout.isatty())
    from termcolor import cprint
    from pyfiglet import figlet_format

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.animation as animation
plt.ion()

try:
    __path__ = os.path.dirname(__file__)
except:
    __path__ = os.getcwd()

class timer(object):
    def __init__(self,count,report_dt=1.):
        self.count = count
        self.report_dt = report_dt
    
    def pprint(self):
        pprint(self)
    
    def start(self):
        self.t0 = time.time()
        self.t = 0.
        self.t_last = 0.
    
    def tick(self,k):
        self.t = time.time() - self.t0
        dt = self.t - self.t_last
        if dt > self.report_dt:
            est_rate = float(k)/self.t
            self.t_last = self.t
            pc = (float(k)/float(self.count))
            t_rem = (1.-pc)*float(self.count)/est_rate
            print '[%3d%%] est done in %0.1f sec\r'%(int(pc*100),t_rem),
            sys.stdout.flush()
            
    def done(self):
        print '[%3d%%] est done in %0.1f sec'%(100,0.)
        sys.stdout.flush()
    
    def reset(self,count):
        self.count = count

def test_timer():
    n = 100
    t = timer(n,report_dt=1.)
    t.start()
    for k in range(n):
        time.sleep(.1)
        t.tick(k)
    
    t.done()

N_batch = 400
N_validate = 100
N_train = 300

def main(show=False,inap=True,N_batch=N_batch,N_validate=N_validate,N_train=N_train):
    '''top-level program: generate training set,
    train, run validation, show results.
    
    These are the global variables generated:
    
    * x = wfs slopes for the training set
    * y = true wavefront phases (sampled on slope grid) for the training set
    * y_pred = model-predicted phases for the training set
    * xv = wfs slopes for the validation set
    * yv = true wavefront phases for the validation set
    * yv_pred = model-predicted phases for the validation set
    * model = the trained neural network that estimates phases given wfs slopes
    
    '''
    global w,s, x,y,y_pred, xv,yv,yv_pred,model,loss_vs_iter
    global screen_hist, ccd_hist, screen_histv, ccd_histv
    w = wfs.Wfs()
    N = w.N
    s = atmos.Screen(n=N)

    cprint(figlet_format('Generate Training Data')); sys.stdout.flush()
    x,y,screen_hist,ccd_hist = gen_data(w,s,N_batch = N_batch,inap=inap)
    cprint(figlet_format('Train Neural Net')); sys.stdout.flush()
    y_pred,model,loss_vs_iter = train(x,y,n_cycles = N_train)
    cprint(figlet_format('Generate Validation Data'))
    xv,yv,screen_hist,ccd_hist = gen_data(w,s,N_batch = N_validate,inap=inap)
    cprint(figlet_format('Validate Neural Net'))
    yv_pred,screen_histv,ccd_histv = validate(xv,yv,w,s,model,inap=inap)

    et = rmse(y,y_pred,w,'training set')
    ev = rmse(yv,yv_pred,w,'validation set')
    #nm_per_radian = w.lam*units[w.lam.unit]/(2*np.pi)/units['nm']
    #loss_vs_iter = np.sqrt(np.array(loss_vs_iter)/np.size(y))*nm_per_radian
    nm_from_radians = ufloat(1.,'radian',w.wavelength).to_units('nm').value
    loss_vs_iter = np.sqrt(np.array(loss_vs_iter)/np.size(y))*nm_from_radians
    
    plot_train()
    
    if show:
        show()
        
def gen_data(w=None,s=None,N_batch=None,inap=True):
    '''generate a set of training or validation data

    :param wfs.Wfs w: the wavefront sensor object
    :param atmos.Screen s: the random phase screen object
    :param int N_batch: the number of frames of data in the (training or validation) data set
    :param bool inap: train only on the phases inside the aperture
    
    :returns: 4-tuple: (x,y,screen_hist,ccd_hist)
    
    where:
    
    * x = measured slopes using wfs, thease are the 'features'
    * y = random screens, these are the 'labels'
    * screen_hist: the history of random phase screens (list of :class:`InfoArrays <info_array.InfoArray>`)
    * ccd_hist: the history of Hartmann wavefront sensor ccd frames (list of :class:`InfoArrays <info_array.InfoArray>`)

    '''    
    mod = sys.modules[__name__]
    if w is None:
        if 'w' in dir(mod):
            w = mod.w
        else:
            print '<gen_data> WARNING generating w locally'
            w = wfs.Wfs()
            mod.w = w
    if s is None:
        if 's' in dir(mod):
            s  = mod.s
        else:
            print '<gen_data> WARNING generating s locally'
            s = atmos.Screen(n=w.N)
            mod.s = s
    if N_batch is None:
        N_batch = mod.N_batch
        
    N = w.N
    y = []
    x = []
    screen_hist = []
    ccd_hist = []
    print('<gen_data> generating data')
    
    if inap:
        ind=((w.subapLocs[0]+7.5)*16 + w.subapLocs[1]+7.5).astype(int)
        
    #t = timer(N_batch)
    #t.start()
    for k in tqdm.tqdm(range(N_batch)):
        s.gen_screen()
        screen = s.screen
        screen_hist.append(screen.copy())
        # simulate the wfs and reconstructor response to this screen
        phi = screen.to_units('radians',wavelength=screen.r0_wavelength)
        w.ccd_image_gpu(phi=phi.astype(np.float32))
        ccd_hist.append(w.ccd.copy())
        w.recon()
        x.append(w.s.copy()) # these are the measured slopes, the input to the neural net
        # down-sample the screen (truth)
        f1 = img.ft(phi)
        f2 = img.crop(f1,(N/2,N/2),(16,16))
        y2 = img.ftinv(f2).real/(N/16)**2 # these are the "true" commands 16x16
        if inap:
            y2 = y2.reshape(16*16)[ind]
        else:
            y2 = y2.reshape(16*16)
        y.append(y2.copy())
        #t.tick(k)
    #t.done()
    return (x,y,screen_hist,ccd_hist)

def gen_tdata(**kwargs):
    '''a wrapper for :func:`gen_data` that
    is specialized for generating **training** data.
    It puts the (features,lables) in globals x, y.
    '''
    global x,y,screen_hist,ccd_hist
    x,y,screen_hist,ccd_hist = gen_data(**kwargs)

def gen_vdata(**kwargs):
    '''a wrapper for :func:`gen_data` that
    is specialized for generating **validation** data.
    It puts the (features,lables) in globals xv, yv.
    '''
    global xv,yv,screen_hist,ccd_hist
    xv,yv,screen_hist,ccd_hist = gen_data(**kwargs)

def gen_push_data(amp=ufloat(200.,'nm'),sd=0.5,set='push'):
    '''generate a set of actuator pushes to simulate old-school calibration,
    then image onto the ccd and reconstruct slopes
    
    :param ufloat amp: the amplitude of the push, in nanometers
    :param float sd: the width of the actuator bump, in fractions of actuator spacing
    :param str set: the mode set to use for pushes: either "push" or "modes"
    
    * set = 'push' means each actuator is pushed one at a time
    * set = 'modes' pushes with actuators set to mode vectors one at a time
    
    The mode vectors are defined in sauce2/data/At.fits.
    
    :returns: a_set, ccd_hist, slope_vecs
    
    '''
    global a_set,ccd_hist,slope_vecs
    global w
    
    mod = sys.modules[__name__]
    if 'w' not in dir(mod):
        w = wfs.Wfs()
    ndm = 256
    ph = np.zeros((ndm,ndm))
    
    if set == 'push':
        a = 16
        aloc = np.arange(1,16)*a
        N = len(aloc)**2
        a_set = []
        for y in aloc:
            for x in aloc:
                c = (x,y)
                psf = amp.value*img.gauss2((ndm,ndm),a*sd,c)
                psf = InfoArray(psf,units=amp.unit)
                a_set.append(psf)

    elif set == 'modes':
        at = fits.readfits(os.path.join(__path__,'data','At.fits'))
        at = list(at)
        N = len(at)
        da = 8
        aloc = np.arange(32)*da
        a_set = []
        for av in tqdm.tqdm(list(at)):
            f1 = img.ft(av)
            f2 = img.zeropad(f1,(ndm,ndm))
            ph = amp.value*img.ftinv(f2).real*(ndm/32)**2
            ph = InfoArray(ph,units=amp.unit)
            a_set.append(ph)

    else:
        raise Exception,"set must be 'push' or 'modes'"
    

    ccd_hist = []
    slope_vecs = []
    
    screen = 0*a_set[0]
    phi = screen.to_units('radians',wavelength=ufloat(500.,'nm'))
    ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
    phtt = wfs.tilt(w.ap,ttgpu,w.wavelength)
    w.ccd_image_gpu(phi=(phi+phtt).astype(np.float32))
    w.recon()
    slope_vecs.append(w.s.copy()) # this is the reference slope vector
    
    for k in tqdm.tqdm(range(N)):
        screen = a_set[k]
        phi = screen.to_units('radians',wavelength=ufloat(500.,'nm'))
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        phtt = wfs.tilt(w.ap,ttgpu,w.wavelength)
        w.ccd_image_gpu(phi=(phi+phtt).astype(np.float32))
        ccd_hist.append(w.ccd.copy())
        w.recon()
        slope_vecs.append(w.s.copy())
    
    return (a_set,ccd_hist,slope_vecs)

def show_push_data(a_set,ccd_hist=None,slope_vecs=None,crop_ccd=True,set='push',fps=30,save_file=None):
    '''Show a movie of the actuator pushes, ccd output, and
    slopes filling in a push matrix
    
    :param list a_set: the set of actuator vectors used
    :param list ccd_hist: the set of corresponding ccd_images
    :param slope_vecs: the set of corresponding slope vectors
    :param crop_ccd: show only the center 80x80 area, which has the Hartmann dots, rather than the whole 120x120 pixel array
    :param str set: 'push' or 'modes' - used only to label the graphs
    :param float fps: display rate in frames per second
    :param str save_file: name of the file to save the animation to. It will be a type `.mpeg` file. If :py:data:`None`, no file is saved
    
    An alternative way aot call this is to pass the tuple returned by :func:`gen_push_data`. Example::
    
        show_push_data(gen_push_data(set='push'))
        show_push_data(gen_push_data(set='modes'))
        
    '''
    mod = sys.modules[__name__]
    if a_set is None:
        a_set = mod.a_set
        ccd_hist = mod.ccd_hist
        slope_vecs = mod.slope_vecs
    else:
        if ccd_hist is None:
            a_set,ccd_hist,slope_vecs = a_set
    N = len(a_set)
    a = normalize(a_set)
    c = normalize(ccd_hist)
    if crop_ccd:
        c = [cc[40:120,40:120] for cc in c]
    
    na = len(a)
    ns = len(slope_vecs[0])
    s_hist = []
    s_filled = np.zeros((ns,na))
    s_ref = slope_vecs[0]
    for k in range(na):
        s_filled[:,k] = slope_vecs[k+1] - s_ref
        s_hist.append(s_filled.copy())
    
    sh = normalize(s_hist)
        
    fig = plt.figure(figsize=(3*5.,5.))
    ax1 = plt.subplot2grid((1,3),(0,0))
    ax2 = plt.subplot2grid((1,3),(0,1))
    ax3 = plt.subplot2grid((1,3),(0,2))
    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)
    ax1.set_title('Deformable Mirror')
    ax2.get_xaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    ax2.set_title('Wavefront Sensor (CCD image)')
    #ax3.get_xaxis().set_visible(False)
    if set == 'push':
        ax3.set_xlabel('actuator',{'fontsize':12})
        ax3.set_title('Push Matrix')
    elif set == 'modes':
        ax3.set_xlabel('mode',{'fontsize':12})
        ax3.set_title('Modal Response Matrix')
    ax3.set_ylabel('slopes Y                   slopes X',{'fontsize':12})
    #ax3.get_yaxis().set_visible(False)
    im1 = ax1.imshow(a[0],cmap='gray')
    im2 = ax2.imshow(c[0],cmap='gray')
    r = sh[0]
    r[0,0] = 0.
    r[1,0] = 1.
    im3 = ax3.imshow(r,cmap='gray')
    
    def update_img(n):
        im1.set_data(a[n])
        im2.set_data(c[n])
        im3.set_data(sh[n])
        return (im1,im2,im3)
    
    ani = animation.FuncAnimation(fig,update_img,N,interval=1000./fps)
    
    if save_file is not None:
        writer = animation.writers['ffmpeg'](fps=fps)
        ani.save(save_file,writer=writer)

    return ani,fig

def example_push(save_file=False,fps=[4,8]):
    '''Example use of the gen and show push_data functions
    demonstrating "old school" WFS calibration

    :param list fps: list of :py:class:`float` display rates, in frames per second, for the "push" and "modes" display respectively
    :param str save_file: name of the file to save the animation to. It will be a type `.mpeg` file. If :py:data:`None`, no file is saved

    '''
    global w
    w = wfs.Wfs()
    a_set,ccd_hist,slope_vecs = gen_push_data(set='push')
    ani1,fig1 = show_push_data(a_set,ccd_hist,slope_vecs,fps=fps[0],set='push')
    a_set,ccd_hist,slope_vecs = gen_push_data(set='modes')
    ani2,fig2 = show_push_data(a_set,ccd_hist,slope_vecs,fps=fps[1],set='modes')
    if save_file:
        print 'writing MPG movie files'; sys.stdout.flush()
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=fps[0], metadata=dict(artist='example_push'), bitrate=1800)
        sys.stdout.write('push_calibrate.mp4...'); sys.stdout.flush()
        ani1.save('push_calibrate.mp4',writer=writer)
        print('done')
        sys.stdout.write('modes_calibrate.mp4...'); sys.stdout.flush()
        writer = Writer(fps=fps[1], metadata=dict(artist='example_push'), bitrate=1800)
        ani2.save('modes_calibrate.mp4',writer=writer)
        print('done')
    return (ani1,ani2)
 
def train(x=None,y=None,n_cycles = N_train):
    '''Performs the neural-net training given the
    training data set, x and y.
    
    :param list x: training set's features
    :param list y: training set's lables
    :param int n_cycles: number of training iterations    
    
    You must generate the data set (x, y) first, using gen_data()
    '''
    global y_pred,model,loss_vs_iter
    
    mod = sys.modules[__name__]
    if x is None:
        x = mod.x
    if y is None:
        y = mod.y
    n_out = y[0].shape[0]
    dtype = torch.float
    device = torch.device('cpu')
    D_in,H,D_out = 288,256,n_out
    x = torch.tensor(np.array(x).astype(np.float32),device=device)
    y = torch.tensor(np.array(y).astype(np.float32),device=device)
    model = torch.nn.Sequential(
        torch.nn.Linear(D_in, H),
        torch.nn.Tanh(),
        torch.nn.Linear(H, D_out),
        torch.nn.Tanh()
    )
    loss_fn = torch.nn.MSELoss(size_average=False)
    #optimizer = torch.optim.SGD(model.parameters(), lr = 1.e-4, momentum=0.9)
    optimizer = torch.optim.Adam(model.parameters(),lr = 1.e-3 )
    losses = []
    for t in range(n_cycles):
        optimizer.zero_grad()
        y_pred = 20.*model(x)
        loss = loss_fn(y_pred,y)
        losses.append(loss.item())

        if t != 0:
            sys.stdout.write('\r'); sys.stdout.flush()
        sys.stdout.write('%r %r'%(t,loss.item())); sys.stdout.flush()
        if t == 0:
            sys.stdout.write('\n'); sys.stdout.flush()

        loss.backward()
        optimizer.step()
    sys.stdout.write('\n'); sys.stdout.flush()
    loss_vs_iter = np.array(losses)
    return y_pred,model,loss_vs_iter

def validate(x=None,y=None,w=None,s=None,model=None,inap=True):
    '''run the validation data set though the trained neural net.
    
    :param list x: the validation set's features
    :param list y: the validation set's lables
    :param wfs.Wfs w: the wavefront sensor object
    :param atmos.Screen s: the random phase screen object
    :param torch.nn.modules.container.Sequential model: the neural net
    :param bool inap: train only on the phases inside the aperture (must match what was used in training)
    
    If the valication data is not provided, it will be generated.
    '''
    global screen_hist,ccd_hist
    global xv,yv,yv_pred
    
    mod = sys.modules[__name__]
    if x is None:
        N_validate = mod.N_validate
        xv,yv,_,_ = gen_data(w,s,N_batch = N_validate,inap=inap)
        x,y = xv,yv
    if w is None:
        w = mod.w
    if s is None:
        s = mod.s
    if model is None:
        model = mod.model

    N_validate = len(x)
    N = w.N
    #screen_hist = []
    #ccd_hist = []
    print('<validate> running validation data through model')

    ind=((w.subapLocs[0]+7.5)*16 + w.subapLocs[1]+7.5).astype(int)
        
    device = torch.device('cpu')
    x = torch.tensor(np.array(x).astype(np.float32),device=device)
    y = torch.tensor(np.array(y).astype(np.float32),device=device)
    yv_pred = 20.*model(x)
    print ('<validate> done')
    return (yv_pred,screen_hist,ccd_hist)

def rmse(u,u_pred,w,title=''):
    '''calculate the rms wavefront error, piston removed,
    of the model prediction
    
    returns the rms wavefront error, in nanometers
    '''
    ind=((w.subapLocs[0]+7.5)*16 + w.subapLocs[1]+7.5).astype(int)
    if isinstance(u,list):
        N_batch = len(u)
    else:
        N_batch = u.shape[0]
    
    if isinstance(u,torch.Tensor):
        u = u.detach().numpy()
        u = list(u)
    if isinstance(u,np.ndarray):
        u = list(u)
    if isinstance(u_pred,torch.Tensor):
        u_pred = u_pred.detach().numpy()
        u_pred = list(u_pred)
    if isinstance(u_pred,np.ndarray):
        u_pred = list(u_pred)
     
    n_out = u[0].shape
    if n_out == 16*16:
        u = [uu[ind] for uu in u]
        u_pred = [uu[ind] for uu in u_pred]
    sd = np.mean([(t-p).std() for t,p in zip(u,u_pred)])
    sd = ufloat(sd,'radian').at(w.wavelength).to_units('nm')
    print('%s wf variance in aperture: %r'%(title,sd))
    return sd

def show():
    '''show both the training and the validation wavefronts
    '''
    showr(y,y_pred,w,title='training set')
    showr(yv,yv_pred,w,title='validation set')

def showr(y=None,y_pred=None,w=None,title=None):
    '''show a movie of the true labels,
    predicted labels, and the errors
    '''
    mod = sys.modules[__name__]
    if y is None:
        y = mod.y
    if y_pred is None:
        y_pred = mod.y_pred
    if w is None:
        w = mod.w
    if isinstance(y,list):
        N_batch = len(y)
        y = np.array(y)
    else:
        N_batch = y.shape[0]
    if isinstance(y,torch.Tensor):
        y = y.detach().numpy()
    if isinstance(y_pred,torch.Tensor):
        y_pred = y_pred.detach().numpy()
    _,n_out = y.shape
    if n_out != 16*16:
        ind=((w.subapLocs[0]+7.5)*16 + w.subapLocs[1]+7.5).astype(int)
        y2 = np.zeros((N_batch,16*16))
        y2_pred = np.zeros((N_batch,16*16))
        y2[:,ind] = y
        y2_pred[:,ind] = y_pred
        y = y2
        y_pred = y2_pred
    one = list(InfoArray(y.reshape(N_batch,16,16)))
    two = list(InfoArray(y_pred.reshape(N_batch,16,16)))
    e = [a-b for a,b in zip(one,two)]
    s = [InfoArray(np.block([a,b,e])) for a,b,e in zip(one,two,e)]
    play(s,title=title)

def showr2(sh,ch,title=None):
    '''show a movie of the original phase screens
    and ccd images
    '''
    sh = normalize(sh)
    ch = normalize(ch)
    s = [InfoArray(np.block([a,img.zeropad(b,(256,256))])) for a,b in zip(sh,ch)]
    play(s,title=title)

def showr2a(screen_hist=None,ccd_hist=None,fps=30,save_file=None, crop_ccd=True):
    '''show an animation of the phase screens and the wavefront sensor ccd
    cropped for 16x16 subaps
    '''
    mod = sys.modules[__name__]
    if screen_hist is None:
        screen_hist = mod.screen_hist
    if ccd_hist is None:
        ccd_hist = mod.ccd_hist
    sh = normalize(screen_hist)
    ch = normalize(ccd_hist)
    if crop_ccd:
        ch = [c[40:120,40:120] for c in ch]
    N = len(ch)
    
    fig = plt.figure(figsize=(10.,5.))
    ax1 = plt.subplot2grid((1,2),(0,0))
    ax2 = plt.subplot2grid((1,2),(0,1))
    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)
    ax2.get_xaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    im1 = ax1.imshow(np.array(sh[0]),cmap='gray')
    im2 = ax2.imshow(np.array(ch[0]),cmap='gray')
    
    def update_img(n):
        im1.set_data(np.array(sh[n]))
        im2.set_data(np.array(ch[n]))
        return (im1,im2)

    ani = animation.FuncAnimation(fig,update_img,N,interval=1000./fps)

    if save_file is not None:
        writer = animation.writers['ffmpeg'](fps=fps)
        ani.save(save_file,writer=writer,dpi=dpi)

    return ani
    
def normalize(q):
    '''normalize a list of arrays to range from 0 to 1
    '''
    mx = [float(qq.astype(np.float).max()) for qq in q]
    mn = [float(qq.astype(np.float).min()) for qq in q]
    mx = max(mx)
    mn = min(mn)
    return [(qq.astype(np.float)-mn)/(mx-mn) for qq in q]

def plot_train(newfig=False):
    '''plot the training residuals vs training iteration
    '''
    #nm_per_radian = w.lam*units[w.lam.unit]/(2*np.pi)/units['nm']
    N_train = len(loss_vs_iter)
    #e_fit = (w.d/s.r0)**(5./6.)*nm_per_radian
    d = w.d
    if not isinstance(d,ufloat):
        d = ufloat(d,w.d_units)
    e_fit = ufloat((d/s.r0)**(5./6.),'radian',w.wavelength).to_units('nm')
    ev = rmse(yv,yv_pred,w,'validation set')
    if newfig:
        plt.figure()
    else:
        plt.clf()
    plt.plot(loss_vs_iter,label='Training')
    plt.plot([0,N_train],[ev,ev],'--',label='Validation')
    plt.plot([0,N_train],[e_fit,e_fit],':',label='Out of band')
    plt.xlabel('training iteration',{'fontsize':14})
    plt.ylabel('RMS nm',{'fontsize':14})
    plt.legend(fontsize=14)
    plt.grid()

def showra(y=None,y_pred=None,w=None,fps=30,animate=True,save_file = None):
    '''generate an animation of the screens and residuals
    with a plot of wf rms along the bottom
    See:
    https://scientificallysound.org/2016/06/09/matplotlib-how-to-plot-subplots-of-unequal-sizes/
    '''
    mod = sys.modules[__name__]
    if y is None:
        y = mod.y
    if y_pred is None:
        y_pred = mod.y_pred
    if w is None:
        w = mod.w
    if isinstance(y[0],torch.Tensor):
        y = [yy.detach().numpy() for yy in y]
    if isinstance(y_pred[0],torch.Tensor):
        y_pred = [yy.detach().numpy() for yy in y_pred]

    e = [a-b for a,b in zip(y,y_pred)]
    #nm_per_radian = w.lam*units[w.lam.unit]/(2*np.pi)/units['nm']
    #rms = [s.std()*nm_per_radian for s in y]
    rms = [ufloat(s.std(),'radians',w.wavelength).to_units('nm') for s in y]
    #rmse = [s.std()*nm_per_radian for s in e]
    rmse = [ufloat(s.std(),'radians',w.wavelength).to_units('nm') for s in e]

    N = len(y)
    assert N == len(y_pred)
    y = np.array(y)
    y_pred = np.array(y_pred)
    
    _,n_out = y.shape
    if n_out != 16*16:
        ind=((w.subapLocs[0]+7.5)*16 + w.subapLocs[1]+7.5).astype(int)
        y2 = np.zeros((N,16*16))
        y2_pred = np.zeros((N,16*16))
        y2[:,ind] = y
        y2_pred[:,ind] = y_pred
        y = y2.reshape(N,16,16)
        y_pred = y2_pred.reshape(N,16,16)
    else:
        y = y.reshape(N,16,16)
        y_pred = y_pred.reshape(N,16,16)
    
    e = y - y_pred

    fig = plt.figure(figsize=(5,5.))
    ax1 = plt.subplot2grid((2,2),(0,0))
    ax2 = plt.subplot2grid((2,2),(0,1))
    ax3 = plt.subplot2grid((2,2),(1,0))
    ax4 = plt.subplot2grid((2,2),(1,1))
    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)
    ax2.get_xaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    ax3.get_xaxis().set_visible(False)
    ax3.get_yaxis().set_visible(False)
    
    gs1 = ax1.get_subplotspec().get_gridspec()
    gs2 = ax2.get_subplotspec().get_gridspec()
    gs3 = ax3.get_subplotspec().get_gridspec()
    gs4 = ax4.get_subplotspec().get_gridspec()
    gs1.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
    gs2.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
    gs3.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
    gs4.update(left=.35,right=.95)
    im1 = ax1.imshow(y[0],cmap='gray')
    im2 = ax2.imshow(y_pred[0],cmap='gray')
    ax2.add_patch(patches.Circle((7.5,7.5),7.,color='red',fill=False))
    ax2.add_patch(patches.Circle((7.5,7.5),2.,color='red',fill=False))
    im3 = ax3.imshow(e[0],cmap='gray')
    line1, = ax4.plot(rms)
    line2, = ax4.plot(rmse)
    plt.ylabel('RMS, nm')
    ax4.grid()
    
    def update_img(n):
        im1.set_data(y[n])
        im2.set_data(y_pred[n])
        im3.set_data(e[n])
        line1.set_data(range(n),rms[0:n])
        line2.set_data(range(n),rmse[0:n])
        return (im1,im2,line1,line2)
    
    if animate:
        ani = animation.FuncAnimation(fig,update_img,N,interval=1000./fps)
    
    if save_file is not None:
        writer = animation.writers['ffmpeg'](fps=fps)
        ani.save(save_file,writer=writer,dpi=dpi)

    return ani

