import numpy as np
import img
import oprint
from info_array import InfoArray

class Zernike(object):
    """A class to hold information and calculations of Zernike sets
    """
    def __init__(self, n=256, R=None, kind='Noll'):
        """initialize the Zernike object, create an aperture
        
        :parm int n: the size of the image
        :parm float R: the aperture radius, in pixels
        :param str kind: Zernike numbering convention
        
        """
        self.kind = kind
        self.n = n
        if R is None:
            R = n/2
        self.R = R
        self.dx = 1./float(R)
        self.ap = img.circle((n,n),r=R)
        x = (np.arange(n) - n/2)/float(R)
        x,y = np.meshgrid(x,x)
        self.r = InfoArray(np.sqrt(x**2+y**2),dx=self.dx,name='r')
        self.th = InfoArray(np.arctan2(y,x),dx=self.dx,name='theta')
    
    def pprint(self):
        oprint.pprint(self)
    
    def ztable(self, kmax=None, nmax=5):
        """create a list of the valid Zernike n,m numbers
        
        :param int kmax: maximum number of entries in the table
        :param int nmax: maximum radial order (if not None, supercedes kmax)

        """
        k = 0
        n = 0
        ztab = []
        if nmax is not None:
            kmax = (nmax+1)*(nmax+2)//2
            
        while k < kmax:
            m = -n
            while m <= n:
                ztab.append((n,m))
                m += 2
                k += 1
            n += 1
        self.ztab = ztab
        
    def calc(self, n,m, fast=False):
        """calculate zernikes n,m and n,-m
        
        :param int n: radial order (must be positive)
        :param int m: azimuthal order (must be postive)
        :param bool fast: fast lookup from a pre-computed table (see py:meth:`calc_zet`)
        
        """
        zrules(n,m)

        if fast:
            if m == 0:
                self.z = [self.get(n,m)]
            else:
                self.z = [self.get(n,mm) for mm in (m,-m)]
            
        r,th = self.r,self.th
        p = 0*r
        for s in range((n-m)/2+1):
            p = p + (-1)**s * (np.math.factorial(n-s) / np.math.factorial(s) ) / \
                              (np.math.factorial((n+m)/2-s) * np.math.factorial((n-m)/2-s)) \
                            * r**(n-2*s)
        p *= np.sqrt(n+1)
        if m != 0:
            p *= np.sqrt(2.)
        if m == 0:
            self.z = [p]
        else:
            self.z = [p*np.cos(m*th), p*np.sin(m*th)]
        for z in self.z:
            z.name = 'Zernike n=%d, m=%d'%(n,m)
            z.dx = self.dx
            z.units = ''
            z.axis_names = ['x','y']
            z.n = n
            z.n_doc = 'radial order'
            z.m = m
            z.m_doc = 'azimuthal order'
            m *= -1
    
    def calc_set(self, kmax=None, nmax=5):
        '''calculate a set of Zernikes
        
        :param int kmax: the maximum number of Zernikes to compute
        :param int nmax: the maximum radial order of Zernikes to compute (supercedes kmax)
        
        results are stored in `self.zset`
        '''
        self.ztable(kmax=kmax,nmax=nmax)
        zset = []
        for n,m in self.ztab:
            if m<0:
                continue
            self.calc(n,m)
            zset.append(self.z[0].copy())
            if m != 0:
                zset.append(self.z[1].copy())
        self.zset = zset
    
    def get(self,n,m):
        """return the n,m Zernike from the precalculated set
        """
        zrules(n,abs(m)) # m may be negative

        r = None
        for z in self.zset:
            if (n,m) == (z.n,z.m):
                r = z.copy()
        return r
    
    def graph(self,nmax=5):
        """Draw a Zernike "Pascal triangle" graphic
        
        :param int nmax: go up to this order
        
        """
        if hasattr(self,'zset'):
            fast = True
        else:
            fast = False
        npix = self.n
        nbuf = npix/5
        nn = (nmax+1)*(npix+nbuf)
        g = np.zeros((nn,nn))
        dx = (npix + nbuf)/2
        dy = npix+nbuf
        kmax = ((nmax+1)*(nmax+2))/2
        self.ztable(kmax=kmax)
        ztab = self.ztab
        ap = img.circle((npix,npix),c=(npix/2,npix/2),r=self.R)
        for k in range(kmax):
            n,m = ztab[k]
            x = m*dx + nn/2
            y = nn - (n*dy + npix/2)
            s = {0:0, 1:0, -1:1}[np.sign(m)]
            self.calc(n,abs(m),fast=fast)
            z = ap*self.z[s]
            g[y-npix/2:y+npix/2,x-npix/2:x+npix/2] = z
        return g
    
    def fit(self, p):
        """compute the Zernike fit to an image. return the coefficient set:
        p = sum(c_i*z_i)
        
        :param array p: the image to fit to
        
        """
        assert p.shape == (self.n,self.n)
        cset = []
        den = self.ap.sum()
        for zz in self.zset:
            c = float((zz*self.ap*p).sum()/den)
            cset.append(c)
        return cset
    
    def series(self, cset):
        """calculate an image from a Zernike coefficient set
        p = sum(c_i*z_i)
        
        :param list cset: the list of coefficients
        """
        assert len(cset) == len(self.zset)
        p = 0.
        for c,zz in zip(cset,self.zset):
            p += c*zz*self.ap
        return p
    
def is_odd(n):
    return (n%2)

def is_even(n):
    return 1 - (n%2)

def zrules(n,m):
    """check if valid Zernike numbering
    """
    assert n>=0
    assert m>=0
    if is_even(n):
        assert is_even(m) and m<=n
    else:
        assert is_odd(m) and m<=n

def zernike(n,m,N=256,R=100,ap=True):
    """helper routine to compute the n,m and n,-m zernikes
    
    :param int n: radial order (must be positive)
    :param int m: azimuthal order (must be positive)
    :param int N: number of fine pixels in the generated image
    :param float R: radius of circle in pixels
    :param bool ap: whether or not the returned images are multiplied by the aperture
    
    """
    global z
    
    zrules(n,m)
    z = Zernike(n=N,R=R)
    z.calc(n,m)
    if ap:
        r = [zz*z.ap for zz in z.z]
        return r
    return [zz.copy() for zz in z.z ]
    
def test(n=2,m=2,show=False):
    global z
    z = Zernike()
    z.calc(n,m)
    if show:
        (z.z[0]*z.ap).show()
        if m != 0:
            (z.z[1]*z.ap).show()

def test2():
    global z
    z = Zernike()
    z.calc_set()
    r = z.get(2,2)
    assert r.n == 2 and r.m == 2
    r = z.get(2,0)
    assert r.n == 2 and r.m == 0
    z.calc(2,2)
    r = z.z
    assert len(r) == 2
    assert r[0].n == 2 and r[0].m == 2
    assert r[1].n == 2 and r[1].m == -2
    z.calc(2,0)
    r = z.z
    assert len(r) == 1
    assert r[0].n == 2 and r[0].m == 0

def testc(verbose=False):
    """check the fit and series methods
    """
    global z,test_result
    z = Zernike()
    z.calc_set()
    n = len(z.zset)
    c = np.random.normal(size=(n))
    p = z.series(c)
    cfit = z.fit(p)
    cfit = np.array(cfit)
    e = c - cfit
    if verbose:
        print(e)
    test_result = [InfoArray(p),c,cfit]
    assert np.isclose(c,cfit,rtol=1.e-1,atol=5.e-2).all()

def testcor():
    """check the cross corelation of zernike modes
    """
    global z,test_result
    z = Zernike()
    z.calc_set()
    n = len(z.zset)
    R = []
    for zz in z.zset:
        c = z.fit(zz)
        R.append(c)
    R = InfoArray(R)
    E = R - np.identity(n)
    test_result = R,E
    assert np.linalg.norm(E.flatten(),np.inf) < .01
