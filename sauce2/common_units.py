"""
Defines a set of commonly used unit conversion factors for use
in physical modeling and display

Typical usage::

    >>> from common_units import *
    
Then for example, access the 'micron' unit conversion factor with::

    >>> units['micron']
    1.e-06
    >>> units.micron
    1.e-06
    >>> micron
    1.e-06
    
which are all= 1.e-6

The import * adds a lot of variable names to the local
environment, but these are only the unit names
such as ``mm``, ``cm``, etc. (those listed by
:func:`Units.list_all`) and the name :data:`units`.
If you don't want this, you can::

    >>> from common_units import units
    
Then::

    >>> units['micron'] # <-- works okay
    1e-06
    >>> units.micron  # <-- works okay
    1e-06
    >>> micron  # <-- doesnt work
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    NameError: name 'micron' is not defined

You can always load them into the local environment later with a call to
:py:meth:`put_into <common_units.Units.put_into>`.

Unitized Floats
---------------

While a :data:`unit` is a fixed (floating point) value
that acts only as a scale factor into the CGS system
of units (e.g. `cm --> m`),
**This module also defines sub class of float**,
:class:`ufloat`, 
**that is a scalar that carries units.**

See :class:`ufloat` below.

See also :class:`~info_array.InfoArray` for
**arrays** (tensors) that can carry units.

"""
import sys
import dotdict
from numpy import pi,isclose
import numpy as np
from oprint import pprint
from copy import copy
import warnings

class Units(object):
    """The Units class defines an object with
    unit conversion factors
    """
    def __init__(self):
        self.arcsec = pi/(180.*3600.),'angle'
        self.arcmin = pi/(180.*60.),'angle'
        self.meter = 1.,'length'
        self.meters = self.meter
        self.degree = pi/180.,'angle'
        self.degrees = self.degree
        self.centimeter = 1.e-2,'length'
        self.centimeters = self.centimeter
        self.cm = self.centimeter
        self.millimeters = 1.e-3,'length'
        self.millimeter = self.millimeters
        self.mm = self.millimeters
        self.micron = 1.e-6,'length'
        self.microns = self.micron
        self.nanometer = 1.e-9,'length'
        self.nanometers = self.nanometer
        self.nm = self.nanometers
        self.kilometer = 1.e3,'length'
        self.kilometers = self.kilometer
        self.km = self.kilometers
        self.second = 1.,'time'
        self.seconds = self.second
        self.millisecond = 0.001,'time'
        self.milliseconds = self.millisecond
        self.microsecond = 1.e-6,'time'
        self.microseconds = self.microsecond
        self.ms = self.millisecond,'time'
        self.radians_angle = 1.,'angle'
        self.rad = 1.,'angle'
        self.millirad = 0.001,'angle'
        self.mrad = self.millirad
        self.microrad = 1.e-6,'angle'
        self.urad = self.microrad
        self.radian = 1.,'phase'
        self.radians = self.radian
        self.wave = 1.,'phase'
        self.waves = self.wave
        
        d = self.get_dict()
        s = {}
        kind = {}
        for key,val in d.items():
            if isinstance(val[0],tuple): val = val[0]
            s[key],kind[key] = val
        self.__dict__ = s
        
        self.kind = kind
        self.kind_doc = 'dictionary mapping unit names to unit kind)'
        u = dotdict.DotDict()
        for key in d.keys():
            u[key] = ufloat(1.,key)
            u[key].kind = kind[key]
        self.u = u
        self.u_doc = 'DotDictionary of base ufloats'
        kinds = set(self.kind.values())
        self.kindl = {}
        self.kindl_doc = 'dictionary maping kind to lists of units of that kind'
        for kind in kinds:
            self.kindl[kind] = [ key  for key,val in self.kind.items() if val == kind]
        
        self._load()
    
    def pprint(self):
        pprint(self)
    
    def list_all(self):
        """List all the unit conversion factors
        and their values.
        
        The current set of units is:
        
            >>> units.list_all()
            arcsec 4.8481368111e-06
            centimeter 0.01
            centimeters 0.01
            cm 0.01
            kilometer 1000.0
            kilometers 1000.0
            meters 1.0
            micron 1e-06
            microns 1e-06
            microsecond 1e-06
            microseconds 1e-06
            millimeter 0.001
            millimeters 0.001
            millisecond 0.001
            milliseconds 0.001
            mm 0.001
            ms 0.001
            nanometer 1e-09
            nanometers 1e-09
            nm 1e-09
            second 1.0
            seconds 1.0
        
        """
        keys = sorted(self.__dict__.keys())
        for key in keys:
            print key,self[key]
    
    def get_dict(self):
        """returns a dictionary with all the
        defined unit conversion factors
        """
        return self.__dict__
    
    def keys(self):
        """returns the list of unit names
        """
        return self.__dict__.keys()
    
    def __getitem__(self,key):
        return self.__dict__[key]
    
    def __setitem__(self,key,val):
        self.__dict__[key] = val
    
    def _load(self,module=None):
        if module is None:
            module = sys.modules[__name__]
        elif isinstance(module,str):
            module = sys.modules[module]
        for name,val in self.get_dict().iteritems():
            setattr(module,name,val)
        setattr(module,'units',self)
    
    def put_into(self,local_dict):
        """update a dictionary with the units.
        For example, to import them into the top-level (globals)::
        
            >>> from common_units import units
            >>> units.put_into(globals())
            
        """
        local_dict.update(self.get_dict())
        local_dict.update({'units':units})

units = None

class ufloat(float):
    """A :class:`ufloat` is a kind of :py:class:`float` that can have units and other attributes.
    ufloats behave like ordinary :class:`float` but carry
    out unit coversions in simple math combinations with
    :class:`float` and other :class:`ufloats <ufloat>`, and in print
    representations include their unit names.
    
    The units are grouped according to kind, where kind is 'time', 'length',
    'angle', etc.
    Right now it is possible to add and subtract ufloats
    of like kind, to divide like kind ufloats to produce
    float ratios, and to multiply :class:`float` by a
    :class:`ufloat` to produce a scaled :class:`ufloat`.
    Mixed units, like "meters/second" and
    multiplication like "(meters/second)*second = meters"
    are not supported yet, but work is in progress.
    
    ufloats may be assigned names, documentation strings,
    or any other attributes you like. The attributes :attr:`unit`
    :attr:`kind` and :attr:`wavelength` are reserved.
    
    Examples of use::
    
        >>> a = ufloat(1.,'meter')
        >>> a
        1.0 meter
        >>> a.to_units('cm')
        100.0 cm
        
    A special case of a kind of unit is the 'phase' kind, such as 'radians'
    and 'waves'. These carry a :class:`ufloat` type
    attribute :attr:`wavelength` in order that they can be converted
    to and from 'length' kinds unambigously.
    
    Example::
    
        >>> a = ufloat(1.,'radian',wavelength=ufloat(0.5,'micron'))
        >>> a
        1.0 radian at 0.5 micron wavelength
        >>> a.to_units('nm')
        79.57747154594766 nm
        >>> a.to_units('waves')
        0.15915494309189535 waves at 0.5 micron wavelength
        
    :mod:`common_units` defines a variable :data:`u` from which you
    can reference all the common units as :class:`ufloat` values using a
    dot notation.
    For example::
    
        >>> a = u.cm
        >>> a
        1.0 cm
        >>> b = 10.*u.cm
        >>> b
        10.0 cm
        >>> (10.*u.cm).to_units('mm')
        100.0 mm
    
    Some simple math with :class:`ufloat` is supported. For example::
    
        >>> a = 1.*u.cm + 2.*u.mm
        >>> a
        1.2 cm
        >>> r = 1.*u.cm / (2.*u.mm)
        >>> r
        5.0
    
    There are several ways :class:`ufloat` can be coerced to :class:`float`::
    
        >>> (10.*u.mm).to_float() # converts to meters, then strips the units
        0.01
        >>> (10.*u.mm).value # strips the units, with no conversion
        10.0
        >>> (10.*u.mm)/u.mm # the ratio of 10mm to 1mm
        10.0
        
    """
    def __new__(cls,value,unit=None,wavelength=None,doc=None):
        """create a :class:`ufloat` with a value and optional unit and
        wavelength (if the unit is a 'phase' kind like 'waves' or 'radians').
        
        :param float value: the floating point value
        :param str unit: the unit. must be a member of :py:data:`common_units.units <common_units.units>`
        :param ufloat wavelength: an associated wavelength to disambiguate 'phase' units
        
        See :mod:`common_units`
        """
        f = super(ufloat, cls).__new__(cls,value)
        if unit is not None:
            f.unit = unit
            if units:
                try:
                    f.kind = units.kind[unit]
                    if f.kind == 'phase':
                        f.wavelength = copy(wavelength)
                except:
                    f.kind = '_%s_kind'%unit
                    units[unit] = 1.
                    units.kind[unit] = f.kind
                    units.u[unit] = f.copy()
        if doc is not None:
            f.doc = doc
        return f

    def copy(self):
        return copy(self)
    
    def __repr__(self):
        '''The representation for a :class:`ufloat`
        includes its floating point value, its unit
        and associated wavelength, if any
        '''
        r = super(ufloat,self).__repr__()
        if hasattr(self,'unit'):
            r += ' '+ self.unit
            if hasattr(self,'wavelength'):
                r += ' at %r wavelength'%self.wavelength
        return r
    
    def pprint(self):
        '''Pretty-print the object, revealing
        all its attributes.
        '''
        pprint(self)
    
    def at(self,wavelength):
        '''Sets the wavelength for a phase-kind (radians or waves) unit
        '''
        assert self.kind == 'phase'
        assert wavelength.kind == 'length'
        if not hasattr(self,'wavelength') or self.wavelength is None:
            r = self.copy()
            r.wavelength = wavelength.copy()
            return r
        else:
            cf = self.wavelength / wavelength
            r = cf*self
            r.wavelength = wavelength.copy()
            return r
    
    def to_units(self,new_unit,wavelength=None):
        '''Return a :class:`ufloat` with units converted
        
        new_unit can be a :class:`string <str>` (one of the
        anes in :data:`units`) or a :class:`ufloat` (whose
        attributes will be applied to define the conversion)
        
        Example::
        
            >>> (1.*u.micron).to_units('mm')
            0.001 mm
            >>> a = 25.*u.mm
            >>> (1.*u.micron).to_units(a) # converts to a's units
            0.001 mm
            >>> a.to_units(u.micron) # convert a to microns
            0.025 micron
            
        '''
        if isinstance(new_unit,ufloat):
            if hasattr(new_unit,'wavelength'):
                wavelength = new_unit.wavelength
            new_unit = new_unit.unit
        new_kind = units.kind[new_unit]
        if new_kind != self.kind:
            if self.kind == 'phase' and new_kind == 'length':
                r = float(self)*self.wavelength.to_units(new_unit)
                if self.unit.startswith('radian'):
                    r /= 2*pi
                return r
            elif self.kind == 'length' and new_kind == 'phase':
                assert wavelength is not None,'<ufloat.to_units requires a wavelength to convert a length to a phase'
                new_value = self/wavelength
                if new_unit.startswith('radian'):
                    new_value *= 2*pi
                r = ufloat(new_value,new_unit,wavelength)
                return r
            else:
                raise Exception,"<ufloat.to_units> cannot covert units '%s' to '%s' because they are not the same kind"%(self.unit,new_unit)
        both_are_phase = [self.kind, new_kind] == ['phase','phase']
        neither_is_phase = not both_are_phase
        if neither_is_phase:
            cf = units[self.unit]/units[new_unit]
            r = self._setvalue(cf*float(self))
            r.unit = new_unit
            return r
        if both_are_phase:
            if wavelength is None:
                wavelength = self.wavelength
            cf = self.wavelength / wavelength
            if self.unit.startswith('wave'):
                cf *= 2*pi
            if new_unit.startswith('wave'):
                cf /= 2*pi
            r = self._setvalue(cf*float(self))
            r.unit = new_unit
            r.wavelength = wavelength
            return r
    
    def to_float(self):
        '''Return a :class:`float` whose value is scaled by the units
        
        Example::
        
            >>> 2.0*u.micron.to_float()
            2e-06
        
        '''
        if hasattr(self,'unit'):
            return float(self)*units[self.unit]
        else:
            return float(self)
        
    def asfits(self,sep=','):
        '''Generate FITS card fields from the value, units, and doc attributes
        
        :param str sep: separator between units and doc in the comment
        
        returns (value, comment) tupple
        '''
        val = float(self)
        comment = ''
        if hasattr(self,'unit'):
            comment += self.unit
        if hasattr(self,'wavelength'):
            comment += ' at %r'%self.wavelength
        if hasattr(self,'doc'):
            comment += '%s %s'%(sep,self.doc)
        return val,comment
    
    def equal(self,other):
        '''Test equality of two :class:`ufloat` objects. This tests
        the numerical equality after accounting for the units
        
        :param ufloat other: the :class:`ufloat` object to compare self to
        
        Note that direct comparison to a (unitless) :class:`float` is forbidden. This is to force
        explicit consideration of the units.
        
        Comparing 'phase' kinds either of which don't have a :attr:`wavelength` attribute will cause an :class:`~exceptions.AttributeError`
        '''
        if type(other) is float:
            raise Exception,'<ufloat.equal> cannot test equality of ufloat to float'
        if self.kind != other.kind:
            if self.kind == 'phase' and other.kind == 'length':
                return self.to_units(other).equal(other)
            elif self.kind == 'length' and other.kind == 'phase':
                return self.equal(other.to_units(self))
            else:
                return False
        else:
            return isclose(self.value, other.to_units(self).value)
    
    def _setvalue(self,value):
        r = ufloat(value,'none')
        d = self.__dict__
        keys = d.keys()
        for key,val in d.items():
            setattr(r,key,val)
        return r
    
    def __getattr__(self,attr):
        if attr == 'value':
            return float(self)
        else:
            raise AttributeError, '%s is not an attribute of %s'%(attr,str(type(self)))
    
    def __eq__(self,other):
        '''Test equality of two :class:`ufloat` objects. This tests
        the numerical equality after accounting for the units
        
        Note that comparison of a (unitless) :class:`float` to a :class:`ufloat` is forbidden. This is to force
        explicit consideration of the units.
        
        .. code-block:: python
        
            >>> a = ufloat(5.0,'mm')
            >>> b = ufloat(0.5,'cm')
            >>> a == b
            True
        
        Comparing 'phase' kinds either of which don't have a :attr:`wavelength` attribute will cause an :class:`~exceptions.AttributeError`
        '''
        return self.equal(other)
    
    def __mul__(self,other):
        #print '<ufloat> __mul__'
        if isinstance(other,ufloat):
            raise NotImplementedError,'<ufloat.__mul__> SORRY ufloat binary multiply not supported yet'
        elif isinstance(other,np.ndarray):
            if hasattr(other,'units'): # an InfoArray
                if other.units == '':
                    r = other.copy()*self.value
                    r.units = self.unit
                    if hasattr(self,'wavelength'):
                        r.wavelength = self.wavelength
                    return r
                else:
                    if hasattr(self,'unit'):
                        raise NotImplementedError,'<ufloat.__mul__> SORRY unit*unit multiply not supported yet'
            else: # a numpy array
                r = self.value*other
                warnings.warn('ufloat units lost in multiply by %r'%type(other))
                return r
        else: # other is a float (or something else)
            return self._setvalue(self.value*other)
    
    def __rmul__(self,other):
        #print '<ufloat> __rmul__'
        return self.__mul__(other)
    
    def __add__(self,other):
        if not isinstance(other,ufloat) or (isinstance(other,ufloat) and other.kind != self.kind):
            raise Exception,'<ufloat.__add__> unit kinds must match in order to add ufloats'
        return self._setvalue(float(self)+float(other.to_units(self)))
    
    def __radd__(self,other):
        if not isinstance(other,ufloat) or (isinstance(other,ufloat) and other.kind != self.kind):
            raise Exception,'<ufloat.__radd__> units must match in order to add ufloats'
        return self._setvalue(float(self)+float(other.to_units(self)))
    
    def __div__(self,other):
        if isinstance(other,ufloat):
            if other.kind == self.kind:
                return float(self)/float(other.to_units(self))
            else:
                raise Exception,'<ufloat.__div__> SORRY ufloat divide of non identical units not supported yet'
        return self._setvalue(float(self)/float(other))
    
    def __rdiv__(self,other):
        if isinstance(other,ufloat) and other.kind == self.kind:
            return float(other)/float(self.to_units(other))
        else:
            raise Exception,'<ufloat.__rdiv__> SORRY ufloat divide of non identical units not supported yet'
    
    def __sub__(self,other):
        if not isinstance(other,ufloat) or (isinstance(other,ufloat) and other.kind != self.kind):
            raise Exception,'<ufloat.__sub__> unit kinds must match in order to subtract ufloats'
        return self._setvalue(float(self)-float(other.to_units(self)))
    
    def __rsub__(self,other):
        if not isinstance(other,ufloat) or (isinstance(other,ufloat) and other.unit != self.unit):
            raise Exception,'<ufloat.__rsub__> units must match in order to subtract ufloats'
        return self._setvalue(float(self)-float(other.to_units(self)))
    
def as_ufloat(v,unit):
    '''Create a :class:`ufloat` given **either**
    a :class:`float` and a unit name, **or**
    convert the :class:`ufloat` to the given units
    
    This is useful in converting function arguments that
    may be either type
    '''
    if isinstance(v,ufloat):
        return v.to_units(unit)
    else:
        return ufloat(v,unit)

units = Units()
'''an instance of :class:`Units` so units can be accessed
with simple "dot attribute" notation
'''

def load_units(module='__main__'):
    '''load the fixed versions of units
    into the present module or interactive session.
    This defines a bunch of new variables in
    the current context, like 'cm', 'mm', etc.
    
    It also exposes :data:`units` into the present
    context.
    '''
    if module is None:
        module = sys.modules[__name__]
    elif isinstance(module,str):
        module = sys.modules[module]
    for name,val in units.__dict__.iteritems():
        setattr(module,name,val)
    q = {'units':units}
    for name,val in q.iteritems():
        setattr(module,name,val)    

def load_ufloats(module='__main__'):
    '''load the ufloat versions of units
    into the present module or interactive session.
    It also exposes 'u', 'ufloat', and 'units'
    into the present context.
    
    Note that this over-writes the fixed unit names
    with the ufloat names in the present
    context. So, for example:
    
    .. code-block:: python
    
        >>> from common_units import *
        >>> ms
        0.001
        >>> load_ufloats()
        >>> ms
        1.0 ms
    
    You can go back (bring back the fixed units) with
    a call to :func:`load_units`
    
    A cleaner way to import the units or ufloats locally might be
    
    .. code-block:: python
    
        >>> from common_units import load_ufloats, load_units
        >>> load_ufloats()
        ... or, alternatively, ...
        >>> load_units()
    
    When you mmultiply by the fixed unit `ms`, it
    scales the multiplicand by 0.001. When you multiply by the ufloat
    unit `ms` it creates a :class:`ufloat` having a
    value of the multiplicand and units of ms (millisecond time).
    '''
    if module is None:
        module = sys.modules[__name__]
    elif isinstance(module,str):
        module = sys.modules[module]
    for name,val in u.iteritems():
        setattr(module,name,val)
    q = {'u':u,'ufloat':ufloat,'units':units}
    for name,val in q.iteritems():
        setattr(module,name,val)

if 'sphinx' in sys.modules:
    u = dotdict.DotDict()
'''a dot-dictionary of fixed unit scale factors
that allows accessing them by dot notation.
For example, u.cm = 0.01 and u.mm = 0.001
'''
    
def test_units():
    print units.get_dict()
    print units['mm']
    print units.mm
    print mm
    print 'passed'

def test2():
    a = 1.*u.cm
    b = 1.*u.mm
    a + b
    b + a
    a = 1.*u.radian.at(0.5*u.micron)
    b = 1.*u.radian.at(1.0*u.micron)
    a + b
    b + a
    a = 1.*u.wave.at(1.0*u.micron)
    b = 1.*u.radian.at(1.0*u.micron)
    a + b
    b + a
    a.to_units('micron')
    b = 1.*u.micron
    b.to_units('radians',1.*u.micron)
    b = 1.*u.nm
    b.to_units('radians',1.*u.micron)
    1.*u.radian.at(0.5*u.micron).to_units('nm')
    a == b
    a == 1000.*b
    print 'passed'

def etest(testcode=test2):
    import inspect
    tc = inspect.getsourcelines(testcode)[0][1:]
    for c in tc:
        c = c.strip()
        if ' = ' in c:
            print c
            exec(c)
        elif len(c) > 0:
            print eval(c)

