"""
demo_fft.py - test the pyfft interface
https://pythonhosted.org/pyfft/
"""
import os
from pyfft.cl import Plan
import numpy
import pyopencl as cl
import pyopencl.array as cl_array
import time
import pylab
import tabulate

pylab.ion()
print_table = True

def cl_batch_test(n=32,n_subaps=144,n_trials=1,verbose=True):
    name = '<cl_batch_test> '
    ctx = cl.create_some_context(interactive=False)
    queue = cl.CommandQueue(ctx)
    
    plan = Plan((n, n), queue=queue)
    data = numpy.ones((n_subaps,n, n), dtype=numpy.complex64)
    data[1,:,:] = 2.0
    data[n_subaps-1,:,:] = -3.0
    gpu_data = cl_array.to_device(queue, data)
    #print gpu_data
    
    if verbose: print name+'starting',n_trials,'trials for',n_subaps,'subaps'
    t0 = time.time()
    for i in range(n_trials):
        gpu_data = cl_array.to_device(queue, data)
        plan.execute(gpu_data.data,batch=n_subaps,wait_for_finish=True) 
        result = gpu_data.get()
        
    t1 = time.time()
    if verbose: print name+'done - time = ',t1-t0,'seconds. Did it work?'
    if verbose: print name, result[0,0,0], result[0,0,1], result[1,0,0], result[1,0,1], result[n_subaps-1,0,0],result[n_subaps-1,0,1]
    return t1-t0

def cl_batch_wfs_test(n=64,n_subaps=144,n_trials=1,verbose=True):
    name = '<cl_batch_wfs_test> '
    ctx = cl.create_some_context(interactive=False)
    queue = cl.CommandQueue(ctx)
    
    plan = Plan((n, n), queue=queue)
    data = numpy.ones((n_subaps,n, n), dtype=numpy.complex64)
    data[1,:,:] = 2.0
    data[n_subaps-1,:,:] = -3.0
    gpu_data = cl_array.to_device(queue, data)
    #print gpu_data
    imap = range(160*160)
    imap.reverse()
    if verbose: print name+'starting',n_trials,'trials for',n_subaps,'subaps'
    t0 = time.time()
    tset = [('start',0)]
    
    for i in range(n_trials):
        
        # exponentiation ~20 ms
        data = numpy.exp(1j*data)
        tset.append(('exponentiation',time.time()-t0))
        # 1st FFT ~8 ms
        gpu_data = cl_array.to_device(queue, data)
        plan.execute(gpu_data.data,batch=n_subaps,wait_for_finish=True)
        tset.append(('first FFT (computation)',time.time()-t0))
        result = gpu_data.get()
        tset.append(('first FFT (get result)',time.time()-t0))
        # square ~6 ms
        u = result*numpy.conj(result)
        tset.append(('square',time.time()-t0))
        # square ~6 ms
        u = result*numpy.conj(result)
        tset.append(('2nd square',time.time()-t0))
        # 2nd FFT ~8 ms
        gpu_data = cl_array.to_device(queue, data)
        plan.execute(gpu_data.data,batch=n_subaps,wait_for_finish=True) 
        result = gpu_data.get()
        tset.append(('2nd FFT',time.time()-t0))
        # multiply
        data = data*data
        tset.append(('multiply',time.time()-t0))
        # 3ed FFT ~8 ms
        gpu_data = cl_array.to_device(queue, data)
        plan.execute(gpu_data.data,batch=n_subaps,wait_for_finish=True) 
        result = gpu_data.get()
        tset.append(('3ed FFT',time.time()-t0))
        # readoff loop ~1 ms
        v = numpy.zeros((n_subaps,5,5))
        for l in range(n_subaps):
            for k in range(5):
                for j in range(5):
                    v[l,k,j] = result[l,0,0]
        tset.append(('readoff loop',time.time()-t0))
        # indirect map ~1 ms
        ccd = numpy.zeros((160*160))
        ccd[imap] = ccd.copy()
        ccd = ccd.reshape((160,160))
        tset.append(('indirect map',time.time()-t0))
        
    t1 = time.time()
    t2 = time.time() # measures time to take time
    if verbose: print name+'done - time = ',t1-t0,'seconds. Did it work?'
    if verbose: print name,result[0,0,0], result[0,0,1], result[1,0,0], result[1,0,1], result[n_subaps-1,0,0],result[n_subaps-1,0,1]
    tset = numpy.array(tset)
    
    names = tset[:,0]
    cum_times = tset[:,1].astype(float)
    delta_times = cum_times - numpy.roll(cum_times,1)
    delta_times[0] = 0
    ms = 0.001

    table = []
    for name,cum_time,delta_time in zip(names,cum_times,delta_times):
        table.append([name,'%6.1f'%(cum_time/ms),'%6.1f'%(delta_time/ms)])
        #print t
    table.append(['total','%6.1f'%((cum_time+delta_time)/ms),''])
    headers = ['operation','cum time','delta time']
    if print_table: print name+'\n'
    if print_table: print(tabulate.tabulate(table,headers=headers,tablefmt='pipe'))
        
    return t1-t0, t2-t0,tset

def cl_batch_wfs_graph(n=64,n_trials=1,verbose=True):
    
    r = cl_batch_wfs_test(n=64,n_subaps=144,n_trials=1,verbose=verbose)
    tset = r[2]
    tset = numpy.array(tset)
    names = tset[:,0]
    cum_times = tset[:,1].astype(float)
    delta_times = cum_times - numpy.roll(cum_times,1)
    delta_times[0] = 0
    pylab.figure()
    pylab.plot(delta_times[1:]*1000,'.')    
    pylab.xticks(range(len(names[1:])),names[1:],rotation=23)
    pylab.xlim([-1,len(names)])
    pylab.grid('on')
    pylab.ylabel('time, millisec')
    if n_trials > 1:
        for trial in range(n_trials):
            r = cl_batch_wfs_test(n=64,n_subaps=144,n_trials=1,verbose=verbose)
            tset = r[2]
            tset = numpy.array(tset)
            names = tset[:,0]
            cum_times = tset[:,1].astype(float)
            delta_times = cum_times - numpy.roll(cum_times,1)
            delta_times[0] = 0
            pylab.plot(delta_times[1:]*1000,'.')            

def cl_test(n=32,n_subaps=1,n_trials=1,wff=None,verbose=True):
    name = '<cl_test> '
    ctx = cl.create_some_context(interactive=False)
    queue = cl.CommandQueue(ctx)
    
    plan = Plan((n, n), queue=queue, wait_for_finish=wff)
    data = numpy.ones((n, n), dtype=numpy.complex64)
    gpu_data = cl_array.to_device(queue, data)
    #print gpu_data
    
    if verbose: print name+'starting',n_trials,'trials for',n_subaps,'subaps'
    t0 = time.time()
    for i in range(n_trials):
        for subap in range(n_subaps):
            plan.execute(gpu_data.data) 
            result = gpu_data.get()
            
            #print result
            plan.execute(gpu_data.data, inverse=True) 
            result = gpu_data.get()
    t1 = time.time()
    if verbose: print name+'done - time = ',t1-t0,'seconds. Did it work?'
    error = numpy.abs(numpy.sum(numpy.abs(data) - numpy.abs(result)) / data.size)
    if verbose: print name,error < 1e-6
    return t1-t0

def py_test(n=32,n_subaps=144,n_trials=10,verbose=True):
    name = '<test> '
    data = numpy.ones((n,n),dtype=numpy.complex64)
    t0 = time.time()
    for i in range(n_trials):
        for subap in range(n_subaps):
            result = numpy.fft.fft2(data)
            result = numpy.fft.ifft2(result)
    t1 = time.time()
    if verbose: print name+'done - time = ',t1-t0,'seconds. Did it work?'
    error = numpy.abs(numpy.sum(numpy.abs(data) - numpy.abs(result)) / data.size)
    if verbose: print name,error < 1e-6
    return t1-t0

# >>> cl_test(n_trials=1,n_subaps=1,n=512)
# starting 1 trials for 1 subaps
# done - time =  0.00525307655334 seconds. Did it work?
# True
# >>> cl_test(n_trials=1,n_subaps=144,n=32)
# starting 1 trials for 144 subaps
# done - time =  0.113995075226 seconds. Did it work?
# True
# >>> py_test(n_trials=1,n_subaps=1,n=512)
# done - time =  0.0363340377808 seconds. Did it work?
# True
# >>> py_test(n_trials=1,n_subaps=144,n=32)
# done - time =  0.0257980823517 seconds. Did it work?
# 
# 1x512 cl beats py by 7x
# 144x32 py beats cl by 5x
# cl 1x512 beats py 144x32 by 5x
# py 144x32 slightly beats py 1x512
# cl 1x512 beats cl 144x32 by 20x
#
#             py        cl
# 1x512      35         5.5
# 144x32     39       150
#

def graph(f=cl_test,verbose=True):
    n_trials = 1
    n_subaps = 1
    n_set = [32,64,128,256,512,1024,2048]
    t_set = []
    for n in n_set:
        t_set.append(f(n=n,n_subaps=n_subaps,n_trials=n_trials,verbose=verbose))
    return n_set,t_set

def graph_batch(f=cl_batch_test,n=32,verbose=True):
    n_trials = 1
    n_subaps_set = [5,10,20,50,144]
    t_set = []
    for n_subaps in n_subaps_set:
        t_set.append(f(n=n,n_subaps=n_subaps,n_trials=n_trials,verbose=verbose))
    return n_subaps_set,t_set

plt = pylab
def graphit(verbose=True):
    mach = os.uname()[1]
    u = graph(f=cl_test,verbose=verbose)
    v = graph(f=py_test,verbose=verbose)
    plt.figure()
    plt.loglog(v[0],v[1],label='python')
    plt.loglog(u[0],u[1],label='opencl')
    plt.title('2d NxN FFT'+' ('+mach+')')
    plt.legend()
    plt.xlabel('N')
    plt.ylabel('time, sec')
    plt.grid('on')
    plt.draw()
    plt.show()

def graphit_batch(n=32,verbose=True,nfig=1):
    mach = os.uname()[1]
    u = graph_batch(f=cl_test,n=n,verbose=verbose)
    v = graph_batch(f=py_test,n=n,verbose=verbose)
    w = graph_batch(f=cl_batch_test,n=n,verbose=verbose)
    if nfig == 1:
        plt.figure()
        line = '-'
    elif nfig == 2:
        line = '--'
    ax = plt.gca()
    plt.plot(u[0],u[1],'b'+line,label='cl loop n=%d'%n)
    plt.plot(v[0],v[1],'r'+line,label='py loop n=%d'%n)
    plt.plot(w[0],w[1],'g'+line,label='cl batch n=%d'%n)
    ax.set_yscale('log')
    plt.title(r'Batch $n\times n$ FFTs'+' ('+mach+')')
    plt.legend()
    plt.xlabel('number in batch')
    plt.ylabel('time, sec')
    plt.grid('on')
    plt.draw()
    plt.show()
