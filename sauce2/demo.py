#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from __future__ import absolute_import, print_function
import sys
import os
home = os.environ['HOME']
sys.path.insert(1,os.path.join(home,'local/pymodules'))
import os
import numpy as np
import img
import fits
import pyopencl as cl
import pyopencl.array as cl_array
import pyopencl.clmath as cl_math
from pyfft.cl import Plan
import time
import pylab
import atmos
import tabulate

pylab.ion()

home = os.environ['HOME']
param_dir = os.path.join(home,'parameterfiles')

# ===== parameters =====
nsub = 5 # ccd pixels across subap
n_subaps = 144 # for the 16x case
oversample = 16 # fine pixels per tweeter actuator spacing
nsa = oversample*2 # number of fine pixels per subap in the pupil plane
n = nsa*2 # number of fine pixels per subap, zero-padded (in subap data cube)
oversample_p = 6 # oversample on the focal plane
D_tele = 3.0
D_sec = 0.8
a_tw = D_tele/(31.-3.)      # tweeter actuator spacing
du_fine = a_tw/oversample   # fine pixel size
n_fine = 512
n_across = 14
delta = 0
d_wfs = D_tele/n_across
n_ccd = 160
# =====================

# ---------- CCD map ----------
readoff = (np.arange(nsub)-2)*oversample_p + nsa
x,y = np.meshgrid(readoff,readoff)
readoff2 = (x+2*nsa*y).flatten()
mapi_from = np.zeros((n_subaps,nsub*nsub))
for k in range(n_subaps):
    mapi_from[k,:] = k*nsa*2*nsa*2+readoff2

mapi_from = mapi_from.flatten().astype(np.int32)
umap = fits.readfits(os.path.join(param_dir,'u_map_subaps_16x.fits'))
mapi_to = umap.astype(np.int32)
ccd_mapi = np.array([mapi_from,mapi_to]).transpose().flatten().astype(np.int32)
# -----------------------------

# --------- CCD pixel ---------
pixel = np.ones((oversample_p,oversample_p))
pixel = img.zeropad(pixel,(nsa*2,nsa*2))
fpixel0 = np.fft.fft2(pixel).astype(np.complex64)
fpixel = np.zeros((n_subaps,n,n)).astype(np.complex64)
fpixel[:,:,:] = fpixel0
# ---------------------------------

# -------- Wavefront map ----------
x1 = (np.arange(0,n_across)-(n_across/2)+0.5)*d_wfs
sloc = []
for x in x1:
    for y in x1:
        r = np.sqrt(x**2 + y**2)
        if (r < (D_tele/2+delta)) and (r > D_sec/2):
            sloc.append((x,y))

isloc = (np.array(sloc)/du_fine + n_fine/2).astype(int)
ns = len(sloc)

x = range(n_fine)
x,y = np.meshgrid(x,x)
ind_from = x + y*n_fine

x = range(n)
x,y = np.meshgrid(x,x)
ind_to = (x + y*n)

pmap_from = []
pmap_to = []
# careful: there's an assumption the zero-padding is 2x here
for iloc,k in zip(isloc,range(n_subaps)):
    sub_from = ind_from[iloc[0]-nsa/2:iloc[0]+nsa/2,iloc[1]-nsa/2:iloc[1]+nsa/2]
    pmap_from.append(sub_from)
    sub_to = ind_to[nsa-nsa/2:nsa+nsa/2,nsa-nsa/2:nsa+nsa/2]
    pmap_to.append(sub_to + k*n*n)

pmap_from= np.array(pmap_from).flatten()
pmap_to = np.array(pmap_to).flatten()
wf_mapi = np.array([pmap_from,pmap_to]).transpose().flatten().astype(np.int32)

#check:
check = False
if check:
    import dimg
    u = np.zeros((n_fine,n_fine)).flatten()
    u[pmap_from] = 1.0
    u = u.reshape((n_fine,n_fine))
    dimg.show(u)
    dimg.plt.title('pmap_from')
    u = np.zeros((n_subaps,n,n)).flatten()
    u[pmap_to] = 1.0
    u = u.reshape((n_subaps,n,n))
    dimg.show(u)

# ---------------------------------

# ~~~~~~~ generate example data ~~~~~~~
a0 = atmos.Screen()
a0.gen_screen()
ph = (a0.screen*2.e-2).astype(np.float32)
ccd = np.zeros((n_ccd,n_ccd),dtype=np.float32)
# ---- aperture ----
L = n_fine*du_fine/2.
x1 = np.linspace(-L,L,n_fine)
x,y = np.meshgrid(x1,x1)
r = np.sqrt(x**2 + y**2)
ap = np.where(r<(D_tele/2.),1.,0.) - np.where(r<(D_sec/2.),1.,0.)
# ------------------
ap = ap.astype(np.float32)
aps = np.zeros((n_subaps,n,n),dtype=np.float32).flatten()
aps[pmap_to] = ap.flatten()[pmap_from]
aps = aps.reshape((n_subaps,n,n))
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# data = []
# for iloc in isloc:
#     ph_sub = phi[iloc[0]-nsa/2:iloc[0]+nsa/2,iloc[1]-nsa/2:iloc[1]+nsa/2]
#     data_sub = np.ones((nsa*2,nsa*2)).astype(np.complex)*(-1.e7)
#     data_sub[nsa-nsa/2:nsa+nsa/2,nsa-nsa/2:nsa+nsa/2] = ph_sub*(1j)
#     data.append(data_sub)
# 
# data = np.array(data).astype(np.complex64)
# ccd = np.zeros((n_ccd,n_ccd),dtype=np.float32).flatten()

def cl_test_plot(n_trials=50):
    cl_test(n_trials = n_trials, map_in = 'CPU')
    r1 = np.array(trial_stat)*1000.
    cl_test(n_trials = n_trials, map_in = 'GPU', n_cpu_trials = n_trials)
    r2 = np.array(trial_stat)*1000.
    r3 = np.array(cpu_trial_stat)*1000.
    pylab.plot(r1,'--',label='CPUmap')
    pylab.plot(r2,'-',label='GPUmap')
    pylab.plot(r3,'-.',label='CPUall')
    pylab.grid('on')
    pylab.xlabel('iteration')
    pylab.ylabel('time, msec')
    pylab.title('Learning curve with indirect map in CPU vs GPU (GPU is ultimately faster)')
    pylab.legend()
    
def cl_test(device=1,n_trials = 20,check=False,map_in = 'GPU',n_cpu_trials = 1):
    """Test all the components of the sauce.wfs method
    
    device = 1 => run cl code on the GPU
    device = 0 => run cl code on the CPU
    """
    global pipe,tpipe,gpu_wf,ccd_result,trial_stat, cpu_trial_stat
    
    pipe = []
    tpipe = []
    trial_stat = []
    gpu_wf = None
    ccd_result = None
    
    print '<cl_test> ===================== start ==========================='
    platform = cl.get_platforms()[0]
    devs = platform.get_devices()
    ctx = cl.Context(devices=[devs[device]])
    print ctx
    #ctx = cl.create_some_context(interactive=False)
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
    
    plan = Plan((n, n), queue=queue)

    map_in_CPU =  map_in == 'CPU'
    map_in_GPU = not map_in_CPU
    print ' mapping done in ',map_in
    
    prg = cl.Program(ctx, """
    __kernel void rzero(__global float *gpu_data) {
        // set complex array to a constant
        int gid = get_global_id(0);
        gpu_data[gid] = 0.f;
    };
    __kernel void czero(__global float2 *gpu_data) {
        // set complex array to a constant
        int gid = get_global_id(0);
        gpu_data[gid].x = 0.f;
        gpu_data[gid].y = 0.f;
    };
    __kernel void cset(__global float2 *gpu_data, const float2 val) {
        // set complex array to a constant
        int gid = get_global_id(0);
        gpu_data[gid].x = val.x;
        gpu_data[gid].y = val.y;
    };
    __kernel void cmsq(__global float2 *gpu_data) {
        // complex magnitude-squared into real
        int gid = get_global_id(0);
        float2 v,r;
        v = gpu_data[gid];
        r.x = v.x*v.x + v.y*v.y;
        //r.y = z;
        gpu_data[gid] = r;
    };
    __kernel void cmul(__global float2 *gpu_data1, __global float2 *gpu_data2) {
        // complex multiply
        int gid = get_global_id(0);
        float2 v,w,r;
        v = gpu_data1[gid];
        w = gpu_data2[gid];
        r.x = v.x*w.x - v.y*w.y;
        r.y = v.x*w.y + v.y*w.x;
        gpu_data1[gid] = r;
    };
    __kernel void cexp(__global float2 *gpu_data) {
        // complex exponential
        int gid = get_global_id(0);
        float2 v,r;
        float a;
        v = gpu_data[gid];
        a = exp(v.x);
        r.x = a*cos(v.y);
        r.y = a*sin(v.y);
        gpu_data[gid] = r;
    };
    __kernel void cmpri(__global float2 *gpu_data) {
        // complex mag-phase to real-imag
        int gid = get_global_id(0);
        float2 v,r;
        float a;
        v = gpu_data[gid];
        a = v.x;
        r.x = a*cos(v.y);
        r.y = a*sin(v.y);
        gpu_data[gid] = r;
    };
    __kernel void movi_cr(__global float2 *src, __global int2 *map, __global float *res) {
        // move-indirect, complex(real part) to real
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y] = src[j.x].x; /* move only the real part */
    };
    __kernel void movi_rr(__global float *src, __global int2 *map, __global float *res) {
        // move-indirect, real to real
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y] = src[j.x];
    };
    __kernel void movi_rc(__global float *src, __global int2 *map, __global float2 *res) {
        // move-indirect, real into complex
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y].x = src[j.x];
        res[j.y].y = 0.0f;
    };
    __kernel void movi_mp(__global float *rsrc, __global float *isrc, __global int2 *map, __global float2 *res) {
        // move-indirect magnitude and phase into complex
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y].x = rsrc[j.x];
        res[j.y].y = isrc[j.x];
    };
    __kernel void copy_rc0(__global float *rsrc, __global float2 *res){
        // copy real part into complex
        int gid = get_global_id(0);
        res[gid].x = rsrc[gid];
        res[gid].y = 0.f;
    };
    __kernel void movi_imag(__global float *isrc, __global int2 *map, __global float2 *res) {
        // move-indirect imaginary part into complex
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y].y = isrc[j.x];
    };
    """).build()
    # test the complex load value
    #global test_data, gpu_test_data
    #test_data = np.ones((20,)).astype(np.complex64)
    #gpu_test_data = cl_array.to_device(queue,test_data)
    #evt = prg.cset(queue,(20,),None,gpu_test_data.data,np.complex64(0.+1.j))
    # -------- Pull out the program kernels ----------
    rzero = prg.rzero
    czero = prg.czero
    cset = prg.cset
    cmsq = prg.cmsq
    cmul = prg.cmul
    cexp = prg.cexp
    cmpri = prg.cmpri
    movi_cr = prg.movi_cr
    movi_rr = prg.movi_rr
    movi_rc = prg.movi_rc
    movi_mp = prg.movi_mp
    copy_rc0 = prg.copy_rc0
    movi_imag = prg.movi_imag
    
    # --------- Load data into GPU ---------
    wf = np.zeros((n_subaps,n,n)).astype(np.complex64).flatten()
    if map_in_GPU:
        gpu_ap = cl_array.to_device(queue,ap)
        gpu_aps = cl_array.to_device(queue,aps)
        gpu_wf = cl_array.to_device(queue,wf)
    gpu_fpixel = cl_array.to_device(queue,fpixel)
    gpu_ccd = cl_array.to_device(queue,ccd)
    gpu_wf_mapi = cl_array.to_device(queue,wf_mapi)
    gpu_ccd_mapi = cl_array.to_device(queue,ccd_mapi)
    
    # --------- Start GPU operations -------
    n_subaps_n_n = (n_subaps*n*n,)
    n_subaps_nsub_nsub = (n_subaps*nsub*nsub,)
    nind = (len(gpu_wf_mapi),)
    trial_stat = []
    t00 = time.time()
    for trial in range(n_trials):
        t0 = time.time()
        tset = [('start',0,None)]
        pipe = []
        
        if map_in_CPU:
            wf[pmap_to] = ap.flatten()[pmap_from] + (1j)*ph.flatten()[pmap_from]
            gpu_wf = cl_array.to_device(queue,wf)
            tset.append(('mag-phase copy-indirect in CPU and load into GPU',time.time()-t0,None))
        
        if map_in_GPU:
            # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            gpu_ph = cl_array.to_device(queue,ph)
            if check: pipe.append(gpu_wf.get().copy())
            tset.append(('load phase into GPU',time.time()-t0,None))
            
            # Step 1: zero the wavefront
            evt = cset(queue, n_subaps_n_n, None, gpu_wf.data, np.complex64(0.))
            #evt = prg.copy_rc0(queue, n_subaps_n_n, None, gpu_aps.data, gpu_wf.data)
            if check: pipe.append(gpu_wf.get().copy())
            tset.append(('zero-out the wavefront',time.time()-t0,evt))
            
            # Step 2: move phases into the wavefront
            evt = movi_mp(queue, nind, None, gpu_ap.data, gpu_ph.data, gpu_wf_mapi.data, gpu_wf.data)
            #evt = prg.movi_imag(queue, nind, None, gpu_ph.data, gpu_wf_mapi.data, gpu_wf.data)
            if check: pipe.append(gpu_wf.get().copy())
            tset.append(('move mag & phase to wavefront',time.time()-t0,evt))
            # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        
        # Step 3: calculate complex wavefront given magnitude and phase
        evt = cmpri(queue, n_subaps_n_n, None, gpu_wf.data)
        if check: pipe.append(gpu_wf.get().copy())
        tset.append(('mag-phase to real-imag',time.time()-t0,evt))
        
        # Step 4: take in-place FFT of wavefront data cube
        plan.execute(gpu_wf.data,batch=n_subaps,wait_for_finish=False)
        evt = None
        if check: pipe.append(gpu_wf.get().copy())
        tset.append(('1st FFT',time.time()-t0,evt))
        
        # Step 5: take magnitude squared of focal plane complex wavefront
        evt = cmsq(queue, n_subaps_n_n, None, gpu_wf.data)
        if check: pipe.append(gpu_wf.get().copy())
        tset.append(('abs square',time.time()-t0,evt))
        
        # Step 6: FFT to prepare for pixel mtf convolution     
        plan.execute(gpu_wf.data,batch=n_subaps,wait_for_finish=False)
        evt = None
        if check: pipe.append(gpu_wf.get().copy())
        tset.append(('2nd FFT',time.time()-t0,evt))
        
        # Step 7: convolve with pixel mtf
        evt = cmul(queue, n_subaps_n_n, None, gpu_wf.data, gpu_fpixel.data)
        if check: pipe.append(gpu_wf.get().copy())
        tset.append(('multiply',time.time()-t0,evt))
        
        # Step 8: inverse FFT to get to pixel-convolved focal plane
        plan.execute(gpu_wf.data,batch=n_subaps,inverse=True,wait_for_finish=False)
        evt = None
        if check: pipe.append(gpu_wf.get().copy())
        tset.append(('3ed FFT',time.time()-t0,evt))
        
        if map_in_GPU:
            # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            # Step 9: initialize the CCD to zero
            evt = rzero(queue, (n_ccd*n_ccd,), None, gpu_ccd.data)
            if check: pipe.append(gpu_ccd.get().copy())
            tset.append(('zero ccd',time.time()-t0,evt))
            
            # Step 9: select points in pixel-convolved focal plane to put in CCD pixels
            evt = movi_cr(queue, n_subaps_nsub_nsub, None, gpu_wf.data, gpu_ccd_mapi.data, gpu_ccd.data)
            if check: pipe.append(gpu_ccd.get().copy())
            tset.append(('indirct transfer to ccd',time.time()-t0,evt))
            
            queue.finish()
            ccd_result = gpu_ccd.get()
            if check: pipe.append(ccd_result.copy())
            tset.append(('unload ccd from GPU',time.time()-t0,None))
            # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        
        #queue.finish()
        
        if map_in_CPU:
            queue.finish()
            fp = gpu_wf.get()
            ccd_result = np.zeros((n_ccd,n_ccd)).flatten()
            ccd_result[mapi_to] = fp[mapi_from]
            ccd_result = ccd_result.reshape((n_ccd,n_ccd))
            tset.append(('read out of GPU and copy indirect into CCD (python)',time.time()-t0,None))

        trial_stat.append(time.time()-t0)
    trial_stat = np.array(trial_stat)
    tff = time.time()
    headers = ['','n_trials','Total, sec','Average, sec']
    table = []
    table.append( ['GPU time', n_trials, np.sum(trial_stat), np.average(trial_stat)] )
    #print 'GPU time: ',tff-t00,'sec total, ',(tff-t00)/float(n_trials),'sec average'
    
    t1 = time.time()
    t2 = time.time() # measures time to take time
    wf_result = gpu_wf.get()
    
    # ---------- end of GPU operations -------
    
    # --------- CPU operations, comparison timing -----------
    cpu_trial_stat = []
    for cpu_trial in range(n_cpu_trials):
        t0 = time.time()
        wf = np.zeros((n_subaps,n,n)).astype(np.complex64).flatten()
        wf[pmap_to] = ap.flatten()[pmap_from] + (1j)*ph.flatten()[pmap_from]
        wf = wf.reshape((n_subaps,n,n))
        wf = np.exp(wf)
        wf = np.fft.fft2(wf)
        wf = wf*wf.conj()
        wf = np.fft.fft2(wf)
        wf = wf*fpixel
        wf = np.fft.ifft2(wf)
        wf = wf.flatten()
        ccd_result_t = np.zeros((n_ccd,n_ccd)).flatten()
        ccd_result_t[mapi_to] = np.real(wf)[mapi_from]
        ccd_result_t = ccd_result_t.reshape((n_ccd,n_ccd))
        t1 = time.time()
        cpu_trial_stat.append(t1-t0)
        # print 'CPU time comparison: ',t1-t0,'sec'
    cpu_trial_stat = np.array(cpu_trial_stat)
    table.append( [ 'CPU time', n_cpu_trials, np.sum(cpu_trial_stat), np.average(cpu_trial_stat) ] )
    print '--------- Summary, Commparison of GPU to CPU -----------------'
    print tabulate.tabulate(table,headers=headers,tablefmt='pipe')
    # --------- start "Truth" operations -------
    if check:
        tpipe = []
        
        # Step 1: zero out the wavefront data cube
        twf = np.zeros((n_subaps,n,n),dtype=np.complex)
        tpipe.append(twf.copy())
        
        # Step 2: move magnitudes and phases into data cube
        mp_sub = np.zeros((n,n),dtype = np.complex)
        for iloc,k in zip(isloc,range(n_subaps)):
            ph_sub = ph[iloc[0]-nsa/2:iloc[0]+nsa/2,iloc[1]-nsa/2:iloc[1]+nsa/2]
            mag_sub = ap[iloc[0]-nsa/2:iloc[0]+nsa/2,iloc[1]-nsa/2:iloc[1]+nsa/2]
            mp_sub[nsa-nsa/2:nsa+nsa/2,nsa-nsa/2:nsa+nsa/2] = mag_sub + ph_sub*(1j)
            twf[k,:,:] = mp_sub
        tpipe.append(twf.copy())
        
        # Step 3: calculate complex wavefront given magnitude and phase
        tmp = np.real(twf)*np.exp(np.imag(twf)*(1j))
        tpipe.append(tmp.copy())
        
        # Step 4: take in-place FFT of wavefront data cube
        for k in range(n_subaps):
            tmp[k] = np.fft.fft2(tmp[k])
        tpipe.append(tmp.copy())
        
        # Step 5: take magnitude squared of focal plane complex wavefront
        tmp = tmp*tmp.conj()
        tpipe.append(tmp.copy())
        
        # Step 6: FFT to prepare for pixel mtf convolution
        for k in range(n_subaps):
            tmp[k] = np.fft.fft2(tmp[k])
        tpipe.append(tmp.copy())
        
        # Step 7: convolve with pixel mtf
        tmp = tmp*fpixel
        tpipe.append(tmp.copy())
        
        # Step 8: inverse FFT to get to pixel-convolved focal plane
        for k in range(n_subaps):
            tmp[k] = np.fft.ifft2(tmp[k])
        tpipe.append(tmp.copy())
        
        # Step 9: select points in pixel-convolved focal plane to put in CCD pixels
        u = []
        for k in range(n_subaps):
            u.append(tmp[k,readoff,:][:,readoff].flatten())
        u = np.array(u).flatten()
        tccd = np.zeros((n_ccd*n_ccd))
        tccd[umap] = u
        tccd = tccd.reshape((n_ccd,n_ccd))
        tpipe.append(tccd.copy())
        
    # ---------- end of "Truth" operations -------
        
    tset = np.array(tset)
    names = tset[:,0]
    cum_times = tset[:,1].astype(float)
    delta_times = cum_times - np.roll(cum_times,1)
    delta_times[0] = 0
    print '------------------------ GPU timing profiles -------------------------'
    print 'GPU Profiles of GPU operation steps (those steps available), last trial'
    pr = []
    for r in tset:
        evt = r[2]
        if (evt):
            evt.wait()
            dt = evt.profile.end - evt.profile.start
            pr.append([r[0],dt*1.e-6])
            #print r[0],dt*1.e-6,'ms'
    print tabulate.tabulate(pr,tablefmt='pipe',headers=['operation','time, ms'])
    print 'CPU Clock timing of GPU operation steps, last trial'
    headers = ['operation','cum time, ms','delta time, ms']
    pr=[]
    for t in zip(names,cum_times,delta_times):
        #print '|'+t[0]+'|'+str(t[1])+'|'+str(t[2])+'|'
        pr.append([t[0],t[1]*1000,t[2]*1000])
        #print '|'+t[0]+'|'+'%3.2f'%(t[1]*1000)+' ms|'+'%3.2f'%(t[2]*1000)+' ms|'
    print tabulate.tabulate(pr,tablefmt='pipe',headers=headers)
    print '<cl_test> ========================= end ========================='
    return 0,0,tset

def cl_batch_wfs_graph(n=64,n_trials=1):
    tf_set = []
    r = cl_test(check=False)
    tf_set.append(r[0])
    tset = r[2]
    tset = np.array(tset)
    names = tset[:,0]
    cum_times = tset[:,1].astype(float)
    delta_times = cum_times - np.roll(cum_times,1)
    delta_times[0] = 0
    pylab.figure()
    pylab.plot(delta_times[1:]*1000,'.')    
    pylab.xticks(range(len(names[1:])),names[1:],rotation=23)
    pylab.xlim([-1,len(names)])
    pylab.grid('on')
    pylab.ylabel('time, millisec')
    pylab.title('demo.py/cl_test')
    if n_trials > 1:
        for trial in range(n_trials):
            r = cl_test()
            tf_set.append(r[0])
            tset = r[2]
            tset = np.array(tset)
            names = tset[:,0]
            cum_times = tset[:,1].astype(float)
            delta_times = cum_times - np.roll(cum_times,1)
            delta_times[0] = 0
            pylab.plot(delta_times[1:]*1000,'.')
    tf_set = np.array(tf_set)
    print 'Final Times:'
    print 'average',np.average(tf_set),'max',np.max(tf_set),'min',np.min(tf_set)

def cmsq_test(n=64,n_subaps=144):
    """Use the GPU to compute the squared absolute value of a complex array
    """
    ctx = cl.create_some_context(interactive=False)
    queue = cl.CommandQueue(ctx)
    data = np.ones((n_subaps,n,n),dtype=np.complex64)*np.complex64(1j)
    data[0,0,0] = 1.+1j
    data_original = data.copy()
    #result = np.empty_like(data)
    data2 = data.copy()
    mf = cl.mem_flags
    gpu_data = cl_array.to_device(queue,data)
    gpu_data2 = cl_array.to_device(queue,data2)
    #gpu_data = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=data)
    #gpu_result = cl.Buffer(ctx, mf.WRITE_ONLY, data.nbytes)
    
    prg = cl.Program(ctx, """
    __kernel void cmsq(__global float2 *gpu_data) {
      int gid = get_global_id(0);
      float2 v,r;
      v = gpu_data[gid];
      r.x = v.x*v.x + v.y*v.y;
      //r.y = z;
      gpu_data[gid] = r;
    };
    __kernel void cmul(__global float2 *gpu_data1, __global float2 *gpu_data2) {
        int gid = get_global_id(0);
        float2 v,w,r;
        v = gpu_data1[gid];
        w = gpu_data2[gid];
        r.x = v.x*w.x - v.y*w.y;
        r.y = v.x*w.y + v.y*w.x;
        gpu_data1[gid] = r;
    }
    """).build()
        
    t0 = time.time()
    prg.cmsq(queue, (n_subaps*n*n,), None, gpu_data.data)
    #result = gpu_data.get()
    #print 'result after square',result[0,0,0],result[0,0,1],result[n_subaps-1,n-1,n-1]
    prg.cmul(queue, (n_subaps*n*n,), None, gpu_data.data, gpu_data2.data)
    t1 = time.time()
    #cl.enqueue_copy(queue, result, gpu_result)
    #result = gpu_data.get()
    result = gpu_data.get()
    t2 = time.time()
    print 'original data',data_original[0,0,0],data_original[0,0,1],data_original[n_subaps-1,n-1,n-1]
    print 'result',result[0,0,0],result[0,0,1],result[n_subaps-1,n-1,n-1]
    print 'time: ',t1-t0,t2-t0
    
def vadd_test(n=50000):
    global a_np, b_np
    a_np = np.random.rand(n).astype(np.float32)
    b_np = np.random.rand(n).astype(np.float32)
    
    ctx = cl.create_some_context()
    queue = cl.CommandQueue(ctx)
    
    mf = cl.mem_flags
    a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a_np)
    b_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=b_np)
    
    prg = cl.Program(ctx, """
    __kernel void sum(__global const float *a_g, __global const float *b_g, __global float *res_g) {
      int gid = get_global_id(0);
      res_g[gid] = a_g[gid] + b_g[gid];
    }
    """).build()
    
    res_g = cl.Buffer(ctx, mf.WRITE_ONLY, a_np.nbytes)
    t0 = time.time()
    prg.sum(queue, a_np.shape, None, a_g, b_g, res_g)
    t1 = time.time()
    res_np = np.empty_like(a_np)
    cl.enqueue_copy(queue, res_np, res_g)
    
    # Check on CPU with Numpy:
    print(res_np - (a_np + b_np))
    print(np.linalg.norm(res_np - (a_np + b_np)))
    return t1-t0

def vmm_test(device='GPU',n=4,m=4):
    """example code from
    http://www.drdobbs.com/open-source/easy-opencl-with-python/240162614?pgno=2
    """
    valid_devices = ['CPU','GPU']
    device = device.upper()
    assert device in valid_devices, 'device must be one of '+str(valid_devices)
    device_id = valid_devices.index(device)
    
    # vector = np.zeros((1, 1), cl.array.vec.float4)
    # matrix = np.zeros((1, 4), cl.array.vec.float4)
    # matrix[0, 0] = (1, 2, 4, 8)
    # matrix[0, 1] = (16, 32, 64, 128)
    # matrix[0, 2] = (3, 6, 9, 12)
    # matrix[0, 3] = (5, 10, 15, 25)
    # vector[0, 0] = (1, 2, 4, 8)
    
    M = np.matrix(np.random.normal(size=(m,n)))
    V = np.matrix(np.random.normal(size=(n,1)))
    MV = M*V
    print MV.T
    vector = np.zeros((1, 1), cl.array.vec.float4)
    matrix = np.zeros((1, m), cl.array.vec.float4)
    for k in range(m):
        matrix[0,k] = M[k,:].astype(np.float32)
    
    vector[0,0] = V.astype(np.float32)

    platform = cl.get_platforms()[0]
    device = platform.get_devices()[device_id]
    print 'using device '+str(device)
    context = cl.Context([device])
    program = cl.Program(context, """
        __kernel void matrix_dot_vector(__global const float4 *matrix,
        __global const float4 *vector, __global float *result)
        {
          int gid = get_global_id(0);
          result[gid] = dot(matrix[gid], vector[0]);
        }
        """).build()
    queue = cl.CommandQueue(context)
     
    mem_flags = cl.mem_flags
    matrix_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=matrix)
    vector_buf = cl.Buffer(context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=vector)
    #matrix_dot_vector = np.zeros(4, np.float32)
    matrix_dot_vector = np.zeros(m, np.float32)
    destination_buf = cl.Buffer(context, mem_flags.WRITE_ONLY, matrix_dot_vector.nbytes)
     
    program.matrix_dot_vector(queue, matrix_dot_vector.shape, None, matrix_buf, vector_buf, destination_buf)
    cl.enqueue_copy(queue, matrix_dot_vector, destination_buf)
    print(matrix_dot_vector)
    res = matrix_dot_vector - MV.T
    print(np.isclose(res,0,atol=1.e-5)).all()
    
#-------------- tests --------------

def test():
    cl_test()

