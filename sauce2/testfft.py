# Tested with Python 2.7.17 and Python 3.8.0
# using scipy 1.3.3 (1.2.2 for Python2)
# numpy 1.16.2
# reikna 0.7.4
# pyopencl 2018.2.5

import os
import sys
import inspect
import numpy as np
from numpy.linalg import norm
import reikna.cluda as cluda
from reikna.fft import FFT
import pyopencl as cl
import pyopencl.array as cl_array
import time
import tqdm
import sauce2
from sauce2 import fits
from sauce2 import img
#import atmos
from sauce2 import atmos
import astropy.units as u
from sauce2 import oprint
from sauce2.dotdict import DotDict
from sauce2 import dimg

plt = dimg.plt
plt.ion()
#========================================================
# The following block was copied from sauce2.wfs
# It defines units radians phase and conversions within astropy Quantities

def pprint(obj):
    """
    Pretty-print the object's contents
    """
    oprint.pprint(obj,short=True)
    print(DotDict(sorted(obj.__dict__.items())))

def copyattrs(source,dest,x=[]):
    x += ['_unit']
    d = dest.__dict__
    for key,val in source.__dict__.items():
        if key in x:
            continue
        d[key] = val

def copyattrs_from(self,other,x=[]):
    copyattrs(other,self,x=x)
    return self

def radiansphase(lam):
    '''creates an equivalence for unit conversion (see astropy.units)'''
    if isinstance(lam,u.Quantity):
        lam = lam.to('m').value
    return [(u.radian, u.m,
            lambda x: x*lam/(2*np.pi),
            lambda x: x*2*np.pi/lam
            )]

def to_radphase(self,wavelength):
    q = self.to('rad',radiansphase(wavelength))
    q._wavelength = wavelength
    copyattrs(self,q)
    return(q)

def to_height(self,unit):
    q = self.to(unit,radiansphase(self._wavelength))
    copyattrs(self,q,x=['_wavelength'])
    return(q)

u.Quantity.pprint = pprint
u.Quantity.show = dimg.show
u.Quantity.to_radphase = to_radphase
u.Quantity.to_height = to_height
u.Quantity.copyattrs = copyattrs_from
#=====================================================
#param_dir = os.getcwd()
param_dir = os.path.join(os.path.dirname(inspect.getfile(sauce2)),'parameterFiles')

# ===== parameters =====
nsub = 5 # ccd pixels across subap
n_subaps = 144 # for the 16x case
oversample = 16 # fine pixels per tweeter actuator spacing
nsa = oversample*2 # number of fine pixels per subap in the pupil plane
n = nsa*2 # number of fine pixels per subap, zero-padded (in subap data cube)
oversample_p = 6 # oversample on the focal plane
D_tele = 3.0
D_sec = 0.8
a_tw = D_tele/(31.-3.)      # tweeter actuator spacing
du_fine = a_tw/oversample   # fine pixel size
n_fine = 512
n_across = 14
delta = 0
d_wfs = D_tele/n_across
n_ccd = 160
# =====================

# ---------- CCD map ----------
readoff = (np.arange(nsub)-2)*oversample_p + nsa
x,y = np.meshgrid(readoff,readoff)
readoff2 = (x+2*nsa*y).flatten()
mapi_from = np.zeros((n_subaps,nsub*nsub))
for k in range(n_subaps):
    mapi_from[k,:] = k*nsa*2*nsa*2+readoff2

mapi_from = mapi_from.flatten().astype(np.int32)
umap = fits.readfits(os.path.join(param_dir,'u_map_subaps_16x.fits'))
mapi_to = umap.astype(np.int32)
ccd_mapi = np.array([mapi_from,mapi_to]).transpose().flatten().astype(np.int32)
# -----------------------------

# --------- CCD pixel ---------
pixel = np.ones((oversample_p,oversample_p))
pixel = img.zeropad(pixel,(nsa*2,nsa*2))
fpixel0 = np.fft.fft2(pixel).astype(np.complex64)
fpixel = np.zeros((n_subaps,n,n)).astype(np.complex64)
fpixel[:,:,:] = fpixel0
# ---------------------------------

# -------- Wavefront map ----------
x1 = (np.arange(0,n_across)-(n_across/2)+0.5)*d_wfs
sloc = []
for x in x1:
    for y in x1:
        r = np.sqrt(x**2 + y**2)
        if (r < (D_tele/2+delta)) and (r > D_sec/2):
            sloc.append((x,y))

isloc = (np.array(sloc)/du_fine + n_fine/2).astype(int)
ns = len(sloc)

x = range(n_fine)
x,y = np.meshgrid(x,x)
ind_from = x + y*n_fine

x = range(n)
x,y = np.meshgrid(x,x)
ind_to = (x + y*n)

pmap_from = []
pmap_to = []
# careful: there's an assumption the zero-padding is 2x here
for iloc,k in zip(isloc,range(n_subaps)):
    sub_from = ind_from[iloc[0]-nsa//2:iloc[0]+nsa//2,iloc[1]-nsa//2:iloc[1]+nsa//2]
    pmap_from.append(sub_from)
    sub_to = ind_to[nsa-nsa//2:nsa+nsa//2,nsa-nsa//2:nsa+nsa//2]
    pmap_to.append(sub_to + k*n*n)

pmap_from= np.array(pmap_from).flatten()
pmap_to = np.array(pmap_to).flatten()
wf_mapi = np.array([pmap_from,pmap_to]).transpose().flatten().astype(np.int32)
#--------------------------------------------

# ~~~~~~~ generate example data ~~~~~~~
a0 = atmos.Screen()
a0.gen_screen()
#ph = (a0.screen*2.e-2).astype(np.float32)
lam = a0.seeing.lam0
ph = (a0.screen.copy()).astype(np.float32).to_radphase(lam)
ccd = np.zeros((n_ccd,n_ccd),dtype=np.float32)
# ---- aperture ----
L = n_fine*du_fine/2.
x1 = np.linspace(-L,L,n_fine)
x,y = np.meshgrid(x1,x1)
r = np.sqrt(x**2 + y**2)
ap = np.where(r<(D_tele/2.),1.,0.) - np.where(r<(D_sec/2.),1.,0.)
# ------------------
ap = ap.astype(np.float32)
aps = np.zeros((n_subaps,n,n),dtype=np.float32).flatten()
aps[pmap_to] = ap.flatten()[pmap_from]
aps = aps.reshape((n_subaps,n,n))
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ==== Prep the GPU and define GPU programs ====
platform = cl.get_platforms()[0]
devs = platform.get_devices()
ctx = cl.Context(devices=[devs[1]])
queue = cl.CommandQueue(ctx,properties=cl.command_queue_properties.PROFILING_ENABLE)

prg = cl.Program(ctx, """
    __kernel void rzero(__global float *gpu_data) {
        // set complex array to a constant
        int gid = get_global_id(0);
        gpu_data[gid] = 0.f;
    };
    __kernel void czero(__global float2 *gpu_data) {
        // set complex array to a constant
        int gid = get_global_id(0);
        gpu_data[gid].x = 0.f;
        gpu_data[gid].y = 0.f;
    };
    __kernel void cset(__global float2 *gpu_data, const float2 val) {
        // set complex array to a constant
        int gid = get_global_id(0);
        gpu_data[gid].x = val.x;
        gpu_data[gid].y = val.y;
    };
    __kernel void cmsq(__global float2 *gpu_data) {
        // complex magnitude-squared into real
        int gid = get_global_id(0);
        float2 v,r;
        v = gpu_data[gid];
        r.x = v.x*v.x + v.y*v.y;
        //r.y = z;
        gpu_data[gid] = r;
    };
    __kernel void cmul(__global float2 *gpu_data1, __global float2 *gpu_data2) {
        // complex multiply
        int gid = get_global_id(0);
        float2 v,w,r;
        v = gpu_data1[gid];
        w = gpu_data2[gid];
        r.x = v.x*w.x - v.y*w.y;
        r.y = v.x*w.y + v.y*w.x;
        gpu_data1[gid] = r;
    };
    __kernel void cexp(__global float2 *gpu_data) {
        // complex exponential
        int gid = get_global_id(0);
        float2 v,r;
        float a;
        v = gpu_data[gid];
        a = exp(v.x);
        r.x = a*cos(v.y);
        r.y = a*sin(v.y);
        gpu_data[gid] = r;
    };
    __kernel void cmpri(__global float2 *gpu_data) {
        // complex mag-phase to real-imag
        int gid = get_global_id(0);
        float2 v,r;
        float a;
        v = gpu_data[gid];
        a = v.x;
        r.x = a*cos(v.y);
        r.y = a*sin(v.y);
        gpu_data[gid] = r;
    };
    __kernel void movi_cr(__global float2 *src, __global int2 *map, __global float *res) {
        // move-indirect, complex(real part) to real
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y] = src[j.x].x; /* move only the real part */
    };
    __kernel void movi_rr(__global float *src, __global int2 *map, __global float *res) {
        // move-indirect, real to real
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y] = src[j.x];
    };
    __kernel void movi_rc(__global float *src, __global int2 *map, __global float2 *res) {
        // move-indirect, real into complex
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y].x = src[j.x];
        res[j.y].y = 0.0f;
    };
    __kernel void movi_mp(__global float *rsrc, __global float *isrc, __global int2 *map, __global float2 *res) {
        // move-indirect magnitude and phase into complex
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y].x = rsrc[j.x];
        res[j.y].y = isrc[j.x];
    };
    __kernel void copy_rc0(__global float *rsrc, __global float2 *res){
        // copy real part into complex
        int gid = get_global_id(0);
        res[gid].x = rsrc[gid];
        res[gid].y = 0.f;
    };
    __kernel void movi_imag(__global float *isrc, __global int2 *map, __global float2 *res) {
        // move-indirect imaginary part into complex
        int gid = get_global_id(0);
        int2 j = map[gid];
        res[j.y].y = isrc[j.x];
    };
    """).build()

api = cluda.ocl_api()
thr = api.Thread(queue)
a = np.ones((n_subaps,n,n)).astype(np.complex64)
fft = FFT(a,axes=(1,2))
fftc = fft.compile(thr)
# ========================================

map_in_GPU = True
# --------- Load data into GPU ---------
wf = np.zeros((n_subaps,n,n)).astype(np.complex64).flatten()
if map_in_GPU:
    gpu_ap = cl_array.to_device(queue,ap)
    gpu_aps = cl_array.to_device(queue,aps)
    gpu_wf = cl_array.to_device(queue,wf)
gpu_fpixel = cl_array.to_device(queue,fpixel)
gpu_ccd = cl_array.to_device(queue,ccd)
gpu_wf_mapi = cl_array.to_device(queue,wf_mapi)
gpu_ccd_mapi = cl_array.to_device(queue,ccd_mapi)
# --------------------------------------

# --------- Start GPU operations -------
n_subaps_n_n = (n_subaps*n*n,)
n_subaps_nsub_nsub = (n_subaps*nsub*nsub,)
nind = (len(gpu_wf_mapi),)
trial_stat = []
t00 = time.time()

# xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
ccds = []
phs = []
ntrials = 100
for trial in range(ntrials):
    print('--- trial: %d ---'%trial)
    print('generate new screen'); sys.stdout.flush()
    a0.gen_screen()
    lam = a0.seeing.lam0
    ph = (a0.screen.copy()).astype(np.float32).to_radphase(lam)
    phs.append(ph.copy())
    gpu_ph = cl_array.to_device(queue,ph)
    # 
    # # Step 1: zero the wavefront
    evt = prg.cset(queue, n_subaps_n_n, None, gpu_wf.data, np.complex64(0.))
    # 
    # # Step 2: move phases into the wavefront
    print('moving phases into wavefront'); sys.stdout.flush()
    evt = prg.movi_mp(queue, nind, None, gpu_ap.data, gpu_ph.data, gpu_wf_mapi.data, gpu_wf.data)
    
    # Step 3: calculate complex wavefront given magnitude and phase
    print('calculating wavefront'); sys.stdout.flush()
    evt = prg.cmpri(queue, n_subaps_n_n, None, gpu_wf.data)
    
    # Step 4: take in-place FFT of wavefront data cube
    print('taking FFT'); sys.stdout.flush()
    fftc(gpu_wf,gpu_wf)
    evt = None
    
    # Step 5: take magnitude squared of focal plane complex wavefront
    print('calculating magnitude squared at focal plane'); sys.stdout.flush()
    evt = prg.cmsq(queue, n_subaps_n_n, None, gpu_wf.data)
    
    # Step 6: FFT to prepare for pixel mtf convolution
    print('FFT in prep for pixel mtf convolution'); sys.stdout.flush()
    fftc(gpu_wf,gpu_wf)
    evt =  None
    
    # Step 7: convolve with pixel mtf
    print('convolve with pixel mtf'); sys.stdout.flush()
    evt = prg.cmul(queue, n_subaps_n_n, None, gpu_wf.data, gpu_fpixel.data)
    
    # Step 8: inverse FFT to get to pixel-convolved focal plane
    print('inverse FFT for pixels in focal plane'); sys.stdout.flush()
    fftc(gpu_wf,gpu_wf,1)
    evt =  None
    
    # Step 9: initialize the CCD to zero
    print('initialize CCD to zero'); sys.stdout.flush()
    evt = prg.rzero(queue, (n_ccd*n_ccd,), None, gpu_ccd.data)
    
    # Step 10: select points in pixel-convolved focal plane to put in CCD pixels
    print('select points in pixel-convolved focal plane to put in CCD'); sys.stdout.flush()
    evt = prg.movi_cr(queue, n_subaps_nsub_nsub, None, gpu_wf.data, gpu_ccd_mapi.data, gpu_ccd.data)
    
    ccd = gpu_ccd.get()
    ccds.append(ccd.copy())
    queue.finish()

ani = dimg.SubplotAnimation([phs,ccds])

# xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

# res_dev = thr.array(shape,dtype=np.complex64)
# 
# wf = np.zeros(shape).astype(np.complex64)
# wf[:,0,0] = 1.
# gpu_wf = cl_array.to_device(queue,wf)
# 
# evt = prg.cset(queue, (n_subaps*n*n,), None, gpu_wf.data, np.complex64(1.))
# 
# a = np.ones((n_subaps,n,n)).astype(np.complex64)
# fft = FFT(a,axes=(1,2))
# fftc = fft.compile(thr)
# fftc(res_dev,gpu_wf)
# 
# res_reference = np.fft.fft2(np.ones((n_subaps,n,n)))
# res = res_dev.get()
# 
# queue.finish()
# 
# fit =norm(res - res_reference) / norm(res_reference)
# print(fit < 1e-6)
