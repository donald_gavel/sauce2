import time
import sys
from sauce2.oprint import pprint
from sauce2 import atmos,wfs
from sauce2.common_units import *
from sauce2.info_array import InfoArray,ufloat,play
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.animation as animation
from sauce2 import img
import torch
import numpy as np
import tqdm

plt.ion()

def main(N_batch=2000,n_cycles=200,n_taps=3,N_validate=200,v=20):
    global w,b
    global x,y,y_pred,ind,screen_hist,model,xv,yv,yv_pred,losses

    w = wfs.Wfs()
    n = 256
    du = w.du
    tmax = N_batch*du/v
    b = atmos.BlowingScreen(n=n,du=du,tmax=tmax,v=v)
    x,y,ind,_ = gen_data(w,b,N_batch=N_batch,n_taps=n_taps)
    y_pred,model,losses =  train(x,y,n_cycles=n_cycles)
    tmax = N_validate*du/v
    b = atmos.BlowingScreen(n=n,du=du,tmax=tmax,v=v)
    xv,yv,ind,_ = gen_data(w,b,N_batch=N_validate,n_taps=n_taps)
    xv,yv,yv_pred = validate(xv,yv,ind,w,b,model,N_validate=N_validate)
    
def init():
    '''top level program for neural net experiment
    to do wind prediction with blowing phase screen
    '''
    global w,b
    w = wfs.Wfs()
    n = 256
    b = atmos.BlowingScreen(n=n,v=20.)

def gen_data(w,b,N_batch=250,n_taps=3):
    '''generate a blowing-wind phase screen
    and run it through the sauce2 simulator to
    get a set of Hartmann dot images and slope vectors.
    
    w = a wavefront sensor object
    b = a blowing wind object
    N_batch = number of frames to generate
    n_taps = number of prior screens in the tapped delay line
    
    return:
    y = true "labels": the phase at this time step
    x = "features": the tapped delay line of prior phases
    '''
    N = w.N
    y = []
    x = []
    screen_hist = None
    #screen_hist = []
    naa = 16 # phase points across
    ap = img.circle((naa,naa),c=(naa/2,naa/2),r=naa/2)
    ind = np.where(ap.flatten())
    n_ph = len(ind[0])
    tdl = np.zeros((n_ph*n_taps)).astype(np.float32)
    for k in tqdm.tqdm(range(N_batch)):
        x.append(tdl.copy())
        screen = img.detilt(img.depiston(b.next(),b.ap),b.ap)
        #screen_hist.append(screen.copy())
        phi = screen.to_units('radians',lam=ufloat(screen.r0_wavelength,screen.r0_wavelength_units))
        f1 = img.ft(phi)
        f2 = img.crop(f1,(N/2,N/2),(naa,naa))
        y2 = img.ftinv(f2).real/(N/naa)**2 # these are the "true" commands 16x16
        y2 = y2.flatten()[ind].astype(np.float32)
        y.append(y2.copy())
        tdl = np.roll(tdl,n_ph)
        tdl[0:n_ph] = y2

    return (x,y,ind,screen_hist)

def showr(y,y_predo,ind,w,title=None):
    '''show a movie of the true labels,
    predicted labels, and the errors
    '''
    if isinstance(y,list):
        y = np.array(y)
    if isinstance(y,torch.Tensor):
        y = y.detach().numpy()
    if isinstance(y_predo,list):
        y_pred = np.array(y_predo).copy()
    if isinstance(y_predo,torch.Tensor):
        y_pred = y_predo.detach().numpy().copy()
        
    nm_per_radian = w.lam*units[w.lam.unit]/(2*np.pi)/units['nm']
    y *= nm_per_radian
    y_pred *= nm_per_radian

    N_batch,n_out = y.shape
    naa = 16
    if n_out != naa*naa:
        y2 = np.zeros((N_batch,naa*naa))
        y2[:,ind[0]] = y
        y = y2
        y2_pred = np.zeros((N_batch,naa*naa))
        y2_pred[:,ind[0]] = y_pred
        y_pred = y2_pred
    y = y.reshape((N_batch,naa,naa))
    y = [InfoArray(yy) for yy in list(y)]
    y_pred = y_pred.reshape((N_batch,naa,naa))
    y_pred = [InfoArray(yy) for yy in list(y_pred)]
    e = [y1-y2 for y1,y2 in zip(y,y_pred)]
    s = [InfoArray(np.block([y1,y2,ee])) for y1,y2,ee in zip(y,y_pred,e)]
    play(s,title=title)

def train(x,y,n_cycles = 200):
    ''' perform the neural-net training given the
    training data set
    
    y = 'labels': true phase screens
    x = 'features': tapped delay line of prior screens
    n_cycles = number of training steps each time step
    n_train = number of prior times to train on (the "look-back" batch)
    '''
    n_in = x[0].shape[0]
    n_out = y[0].shape[0]
    nt = len(x)
    dtype = torch.float
    device = torch.device('cpu')
    D_in,H,D_out = n_in,256,n_out
    model = torch.nn.Sequential(
        torch.nn.Linear(D_in, H),
        torch.nn.Tanh(),
        torch.nn.Linear(H, D_out),
        torch.nn.Tanh()
    )
    loss_fn = torch.nn.MSELoss(size_average=False)
    #optimizer = torch.optim.SGD(model.parameters(), lr = 1.e-4, momentum=0.9)
    optimizer = torch.optim.Adam(model.parameters(),lr = 1.e-3 )

    losses = []
    first_time = True
    x = torch.tensor(np.array(x).astype(np.float32),device=device)
    y = torch.tensor(np.array(y).astype(np.float32),device=device)
    for tc in range(n_cycles):
        optimizer.zero_grad()
        y_pred = 20.*model(x)
        loss = loss_fn(y_pred,y)
        if tc != 0:
            sys.stdout.write('\r'); sys.stdout.flush()
        sys.stdout.write('%r %r'%(tc,loss.item())); sys.stdout.flush()
        if first_time:
            sys.stdout.write('\n'); sys.stdout.flush()
            first_time = False
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    losses = np.array(losses)
    sys.stdout.write('\n'); sys.stdout.flush()
    return y_pred,model,losses

def plot_train():
    nm_per_radian = w.lam*units[w.lam.unit]/(2*np.pi)/units['nm']
    loss_vs_iter = np.sqrt(np.array(losses)/np.size(y))*nm_per_radian
    N_train = len(loss_vs_iter)
    e_fit = (w.d/b.r0)**(5./6.)*nm_per_radian
    ev = rmse(yv,yv_pred,w,'validation set')
    plt.figure()
    plt.plot(loss_vs_iter,label='Training')
    plt.plot([0,N_train],[ev,ev],'--',label='Validation')
    plt.plot([0,N_train],[e_fit,e_fit],':',label='Out of band')
    plt.xlabel('training iteration')
    plt.ylabel('RMS nm')
    plt.legend()
    plt.grid()

def validate(x,y,ind,w,b,model,N_validate=200):
    N = w.N
    if x is None:
        print('generating validation data')
        x,y,ind,screen_hist = gen_data(w,b,N_validate)

    device = torch.device('cpu')
    loss_fn = torch.nn.MSELoss(size_average=False)
    x = torch.tensor(np.array(x).astype(np.float32),device=device)
    y = torch.tensor(np.array(y).astype(np.float32),device=device)
    y_pred = 20.*model(x)
    loss = loss_fn(y_pred,y)
    print('loss: %r'%loss.item())
    
    return (x,y,y_pred)

def meval():
    '''look at the model weights after training
    '''
    p = list(model.parameters())
    A = np.matrix(p[0].detach().numpy())
    B = np.matrix(p[2].detach().numpy())
    H = B*A
    n = H.shape[0]
    InfoArray(H).show()
    plt.gca().add_patch(patches.Rectangle( (-3*(n/2),-n/2), n,n, fill=False, edgeColor='red'))
    plt.gca().add_patch(patches.Rectangle( (-n/2,-n/2), n,n, fill=False, edgeColor='red'))
    plt.gca().add_patch(patches.Rectangle( (3*(n/2),-n/2), n,n, fill=False, edgeColor='red'))

def weight_plot(i,j,animate=False,rate=2.,clip=.5):
    p = list(model.parameters())
    A = np.matrix(p[0].detach().numpy())
    B = np.matrix(p[2].detach().numpy())
    H = B*A
    n = H.shape[0]
    naa = 16
    n_taps=3
    i2 = naa*i+j
    k = np.where(ind[0]==i2)[0][0]
    a = np.zeros((n_taps,naa*naa))
    for ti in range(n_taps):
        a[ti,ind[0]] = H[k,n*ti:n*(ti+1)]
    a = np.array(a).reshape((3,naa,naa))
    an = normalize(list(a))
    an = [aa.clip(clip,1.) for aa in an]
    an += [an[0],an[0],an[0]]
    if not animate:
        fig = plt.figure(figsize=(6,2))
        ax1 = plt.subplot2grid((1,3),(0,0))
        ax2 = plt.subplot2grid((1,3),(0,1))
        ax3 = plt.subplot2grid((1,3),(0,2))
        ax1.get_xaxis().set_visible(False)
        ax1.get_yaxis().set_visible(False)
        ax2.get_xaxis().set_visible(False)
        ax2.get_yaxis().set_visible(False)    
        ax3.get_xaxis().set_visible(False)
        ax3.get_yaxis().set_visible(False)    
        gs1 = ax1.get_subplotspec().get_gridspec()
        gs2 = ax2.get_subplotspec().get_gridspec()
        gs3 = ax3.get_subplotspec().get_gridspec()
        gs1.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
        gs2.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
        gs3.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
        im1 = ax1.imshow(a[0],cmap='gray')
        im2 = ax2.imshow(a[1],cmap='gray')
        im3 = ax3.imshow(a[2],cmap='gray')
        return None,a
    else:
        fig = plt.figure(figsize=(2,2))
        ax1 = plt.subplot2grid((1,1),(0,0))
        ax1.get_xaxis().set_visible(False)
        ax1.get_yaxis().set_visible(False)
        gs1 = ax1.get_subplotspec().get_gridspec()
        im1 = ax1.imshow(an[0],cmap='gray')
        
        def update_img(n):
            im1.set_data(an[n])
        
        ani = animation.FuncAnimation(fig,update_img,len(an),interval=1000./rate)
        return ani,a
    
def rmse(u,u_pred,w,title=''):
    '''calculate the rms wavefront error, piston removed,
    of the model prediction
    
    returns the rms wavefront error, in nanometers
    '''
    if isinstance(u,list):
        N_batch = len(u)
    else:
        N_batch = u.shape[0]
    
    if isinstance(u,torch.Tensor):
        u = u.detach().numpy()
        u = list(u)
    if isinstance(u,np.ndarray):
        u = list(u)
    if isinstance(u_pred,torch.Tensor):
        u_pred = u_pred.detach().numpy()
        u_pred = list(u_pred)
    if isinstance(u_pred,np.ndarray):
        u_pred = list(u_pred)
     
    n_out = u[0].shape
    if n_out == 16*16:
        u = [uu[ind] for uu in u]
        u_pred = [uu[ind] for uu in u_pred]
    sd = np.mean([(t-p).std() for t,p in zip(u,u_pred)])
    sd *= w.lam*units[w.lam.unit]/(2*np.pi)/units['nm']
    print('%s wf variance in aperture: %r nm'%(title,sd))
    return sd

def normalize(q):
    '''normalize a list of arrays to range from 0 to 1
    '''
    mx = [float(qq.astype(np.float).max()) for qq in q]
    mn = [float(qq.astype(np.float).min()) for qq in q]
    mx = max(mx)
    mn = min(mn)
    return [(qq.astype(np.float)-mn)/(mx-mn) for qq in q]

