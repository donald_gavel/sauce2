// GPU cl kernel functions to support wavefront sensing

__kernel void rzero(__global float *gpu_data) {
    // set real array to zero
    int gid = get_global_id(0);
    gpu_data[gid] = 0.;
};
__kernel void uszero(__global ushort *gpu_data) {
    // set ushort array to zero
    int gid = get_global_id(0);
    gpu_data[gid] = 0;
};
__kernel void rplusr(__global float *gpu_data, __global float *gpu_data2) {
    // add real array to real array
    int gid = get_global_id(0);
    gpu_data[gid] = gpu_data[gid] + gpu_data2[gid];
}
__kernel void czero(__global float2 *gpu_data) {
    // set complex array to a constant
    int gid = get_global_id(0);
    gpu_data[gid].x = 0.f;
    gpu_data[gid].y = 0.f;
};
__kernel void cset(__global float2 *gpu_data, const float2 val) {
    // set complex array to a constant
    int gid = get_global_id(0);
    gpu_data[gid].x = val.x;
    gpu_data[gid].y = val.y;
};
__kernel void cmsq(__global float2 *gpu_data) {
    // complex magnitude-squared into real
    int gid = get_global_id(0);
    float2 v,r;
    v = gpu_data[gid];
    r.x = v.x*v.x + v.y*v.y;
    //r.y = z;
    gpu_data[gid] = r;
};
__kernel void cmul(__global float2 *gpu_data1, __global float2 *gpu_data2) {
    // complex multiply
    int gid = get_global_id(0);
    float2 v,w,r;
    v = gpu_data1[gid];
    w = gpu_data2[gid];
    r.x = v.x*w.x - v.y*w.y;
    r.y = v.x*w.y + v.y*w.x;
    gpu_data1[gid] = r;
};
__kernel void cexp(__global float2 *gpu_data) {
    // complex exponential
    int gid = get_global_id(0);
    float2 v,r;
    float a;
    v = gpu_data[gid];
    a = exp(v.x);
    r.x = a*cos(v.y);
    r.y = a*sin(v.y);
    gpu_data[gid] = r;
};
__kernel void cmpri(__global float2 *gpu_data) {
    // complex mag-phase to real-imag
    int gid = get_global_id(0);
    float2 v,r;
    float a;
    v = gpu_data[gid];
    a = v.x;
    r.x = a*cos(v.y);
    r.y = a*sin(v.y);
    gpu_data[gid] = r;
};
__kernel void movi_cr(__global float2 *src, __global int2 *map, __global float *res) {
    // move-indirect, complex(real part) to real
    int gid = get_global_id(0);
    int2 j = map[gid];
    res[j.y] = src[j.x].x; /* move only the real part */
};
__kernel void movi_cis(__global float2 *src, __global int2 *map, const float s, __global ushort *res) {
    // move-indirect, complex(real part) to ushort after scale
    int gid = get_global_id(0);
    int2 j = map[gid];
    res[j.y] = src[j.x].x*s; /* move only the real part */
};
__kernel void movi_crs(__global float2 *src, __global int2 *map, const float s, __global float *res) {
    // move-indirect, complex(real part) to real after scale
    int gid = get_global_id(0);
    int2 j = map[gid];
    res[j.y] = src[j.x].x*s; /* move only the real part */
}
__kernel void movi_rr(__global float *src, __global int2 *map, __global float *res) {
    // move-indirect, real to real
    int gid = get_global_id(0);
    int2 j = map[gid];
    res[j.y] = src[j.x];
};
__kernel void movi_rc(__global float *src, __global int2 *map, __global float2 *res) {
    // move-indirect, real into complex
    int gid = get_global_id(0);
    int2 j = map[gid];
    res[j.y].x = src[j.x];
    res[j.y].y = 0.0f;
};
__kernel void movi_mp(__global float *rsrc, __global float *isrc, __global int2 *map, __global float2 *res) {
    // move-indirect magnitude and phase into complex
    int gid = get_global_id(0);
    int2 j = map[gid];
    res[j.y].x = rsrc[j.x];
    res[j.y].y = isrc[j.x];
};
__kernel void copy_rc0(__global float *rsrc, __global float2 *res){
    // copy real part into complex
    int gid = get_global_id(0);
    res[gid].x = rsrc[gid];
    res[gid].y = 0.f;
};
__kernel void movi_imag(__global float *isrc, __global int2 *map, __global float2 *res) {
    // move-indirect imaginary part into complex
    int gid = get_global_id(0);
    int2 j = map[gid];
    res[j.y].y = isrc[j.x];
};
