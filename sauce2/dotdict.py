"""dotdict - a dictionary object that supports dot access notation
"""
from sauce2 import oprint

class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    
    def __repr__(self):
        return oprint.ppd(self,print_=False)
    
    def copy(self):
        return DotDict(super(DotDict,self).copy())
    
    #__getattr__ = dict.__getitem__
    def __getattr__(self,attr):
        if attr == '__name__':
            return 'DotDict'
        return dict.__getitem__(self,attr)
    
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

def typize(d):
    """convert dictionary values from strings to strings, ints, and floats, or lists thereof
    according to representation
    """
    for key,val in d.items():
        if val.startswith("'"):
            val = val.strip("'")
        elif val.startswith('('):
            val = eval(val)
        else:
            try:
                val = int(val)
            except:
                try:
                   val = float(val)
                except:
                    pass
        d[key] = val
    return DotDict(d)
