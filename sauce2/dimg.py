"""Dimg is the Display IMaGe package
wrapping some most-used parts of the matplolib library
and pyfits library, and making graphics more interactive.

Author: Don Gavel
"""
from __future__ import print_function
import os
import sys
import time
import numpy as np
import platform
from astropy.io import fits
from astropy import units as u
from astropy.nddata import NDData
from sauce2.dotdict import DotDict
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.patches import FancyArrow
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1 import make_axes_locatable

def surface(z,**kwargs):
    dimg(z,surface=True,**kwargs)
    
def contour(z,fig=None,title=None,**kwargs):
    default_kwargs = {
        'image': False,
        'origin': 'lower',
    }
    default_kwargs.update(kwargs)
    kwargs = default_kwargs
    meta = get_meta(z)
    fig = plt.figure(fig)
    dimg(z,contour=True,**kwargs)
    
def tv(z,grid=True,cmap='bone',origin='lower',dx=1):
    """New simplified version of dimg
    Displays only 2-D images
    X and Y coordinates start at 0,0 lower-left of 0,0 pixel and end at n,n upper right of n-1,n-1 pixel
    Display origin is always lower left
    Always shows a color bar
    
    keyword args:
     cmap: color map
     origin: 'lower' puts 0,0 at the lower left corner
             'center' puts 0,0 at the lower left of pixel n/2 (pixels are numbered 0 to n-1)
     dx: increment of one pixel
    """
    assert type(z) == np.ndarray
    assert len(z.shape) == 2

    n,m = z.shape
    extent = np.array([0,n,0,n])
    if (origin == 'center'):
        extent = extent - n/2
    extent *= dx

    def format_coord(xx, yy):
        col = int(xx+0.5)
        row = int(yy+0.5)
        l,r,b,t = extent
        row = int(((yy-b)/(t-b))*m)
        col = int(((xx-l)/(r-l))*n)
        if col>=0 and col<n and row>=0 and row<m:
            return '%1.2f,%1.2f[%1.0f,%1.0f] %1.2f' % (xx,yy, row,col, z[row,col])
        else:
            return 'x=%1.4f, y=%1.4f' % (xx, yy)
    
    plt.ion()
    plt.figure()
    ax = plt.subplot(111)
    im = ax.imshow(z,interpolation='nearest',origin='lower',extent=extent, cmap=cmap)
    ax.format_coord = format_coord
    plt.colorbar(im)
    if (grid):
        plt.grid('on')
    return im

def copyattrs(self,other):
    sd = self.__dict__
    od = other.__dict__
    for key,val in od.items():
        if not key.startswith('_'):
            sd[key] = val
    return self

def get_meta(z):
    '''unpack an object array z of its meta data and place in
    a structure that show, play, etc. can use to decorate the plot
    '''
    meta = {
        'name': '',
        'axis_labels': ['X','Y'],
        'dx': [1,1],
        'x0': [0,0],
    }
    if isinstance(z,u.Quantity):
        for key in meta.keys():
            if hasattr(z,key):
                meta[key] = getattr(z,key)
                if key in ['dx','x0'] and u.Quantity(meta[key]).isscalar:
                    meta[key] = [meta[key]]*2
        meta['data'] = z.value
        meta['unit'] = z.unit
        meta['shape'] = z.shape
    elif isinstance(z,NDData):
        for key in meta.keys():
            if key in z._meta:
                meta[key] = z._meta[key]
        meta['data'] = z.data
        meta['unit'] = z.unit
        meta['shape'] = z.data.shape
    elif isinstance(z,np.ndarray):
        meta['data'] = z
        meta['unit'] = u.Quantity(1).unit
        meta['shape'] = z.shape
    else:
        try:
            meta['shape'] = z.shape
            meta['unit'] = u.Quantity(1).unit
            meta['data'] = z
        except:
            pass
    return DotDict(meta)

complex_display = 'real'
complex_display_kinds = ['real','imag','abs','angle']

def show(z,fig=None,title='',scale='linear',**kwargs):
    '''display an array as a "density" plot (e.g. grayscale).
    
    :param array_object z: a 2d array of data, with attributes (see below)
    :param int fig: optional Id number of matplotlib.pyplot.figure to draw in. Otherwise create a new figure.
    :param str scale: display stretch. Can be a tuple (str,float) that has a stretch parameter
    
    Works with numpy.ndarray, astropy.units.Quantity arrays and astropy.nddata.NDData.
    Attributes 'name' 'x0' 'dx' and 'axis_labels' are useful::
    
        z.x0 = [x0,y0] - lower left coordinate (floats or Quantities)
        z.dx = [dx,dy] - pixel delta coordinate (floats or Quantities)
        z.axis_labels = [labelx,labely] - string labels for the axes
        z.name = string, used as the title in the display
    
    The scale can be 'linear','log', or 'pow'. It can also be a tuple
    with the second item being a scale parameter::
    
        ('linear',1.) - intensity = value (parameter not used)
        ('log',decades) - intensity = log(value), with decades of display (default: 3 decades)
        ('pow',power) - intensity = val**power (default power=0.5 (square root stretch))
    
    Remaining keywords are sent to plt.imshow. useful ones are::
    
        cmap - color map, default: 'gray'
        interpolation - default: 'nearest'
        origin - default: 'lower'
        
    '''
    if isinstance(z,list) or (isinstance(z,np.ndarray) and z.ndim > 2):
        play(z,**kwargs)
        return
    elif isinstance(z,NDData) and z.data.ndim > 2:
        play(z.data,**kwargs)
        return
    
    meta = get_meta(z)
    n,m = meta.shape
    im_kwargs = {
                'cmap':'gray',
                'interpolation':'nearest',
                'origin':'lower',
                'extent':[0,m,0,n],
                }
    im_kwargs.update(kwargs)
    extent = im_kwargs['extent']
    origin = im_kwargs['origin']
    
    fig = plt.figure(fig)
    
    dx = meta.dx
    dx = [dx[k] for k in [0,0,1,1]]
    extent = [v*w for v,w in zip([0,m,0,n],dx)]
    x0 = meta.x0
    x0 = [x0[k] for k in [0,0,1,1]]
    extent = [v+w for v,w in zip(x0,extent)]
    labels = meta.axis_labels
    try:
        labels = [(v+', %s'%w.unit).lstrip(', ') for v,w in zip(labels,extent[::2])]
    except:
        pass
    extent = [v.value if isinstance(v,u.Quantity) else v for v in extent]
    im_kwargs['extent'] = extent
    plt.xlabel(labels[0])
    plt.ylabel(labels[1])
    
    def format_coord(xx, yy):
        col = int(xx+0.5)
        row = int(yy+0.5)
        if extent is not None:
            l,r,b,t = extent
            row = int(((yy-b)/(t-b))*n)
            col = int(((xx-l)/(r-l))*m)
            if (origin != 'lower'):
                row = n-row-1
        if col>=0 and col<m and row>=0 and row<n:
            zz = z[row,col]
            return 'x%1.2f y%1.2f (%1.0f,%1.0f) z%1.2f%s'%(xx,yy, row,col, zz,zunit)
        else:
            return 'x=%1.4f, y=%1.4f'%(xx, yy)

    ax = plt.gca()
    if not title:
        title = meta.name
    unit = meta.unit
    title = (title + ', %s'%unit).strip(', ')
    zunit = '%s'%unit # for format_coord
    
    z = meta.data
    if np.iscomplexobj(z):
        assert complex_display in complex_display_kinds, 'ERROR: complex_display must be one of %r'%complex_display_kinds
        if complex_display == 'real':
            z = z.real
            msg = 'real part'
        if complex_display == 'imag':
            z = z.imag
            msg = 'imaginary part'
        if complex_display == 'abs':
            z = np.absolute(z)
            msg = 'absolute value'
        if complex_display == 'angle':
            z = np.angle(z)
            msg = 'complex angle'
        print('WARNING: complex data type. Displaying the %s.'%msg)
        title = title + ' (%s)'%msg
        
    plt.title(title)
    
    try:
        scale,scale_param = scale
    except:
        scale_param = {'log':4,'pow':0.5,'linear':1.}[scale]
    if scale != 'linear':
        z = z.astype(float)
        zs = z - z.min()
    if scale == 'log':
        zs = np.log(zs+10**(-scale_param)*zs.max())
    elif scale == 'pow':
        zs = zs**scale_param
    else:
        zs = z
    im = ax.imshow(zs,**im_kwargs)
    dx,dy = meta.dx
    aspect = dx/dy
    if isinstance(aspect,u.Quantity):
        aspect = aspect.decompose().value
    ax.set_aspect(aspect)
    ax.format_coord = format_coord
    im.format_cursor_data = lambda x: '' # gets rid of the [ ] at the end

def colorbar(im=None,position='right'):
    if im is None:
        im = plt.gci()
    ax = plt.gca()
    divider = make_axes_locatable(ax)
    pad = 0.1
    orientation = 'vertical'
    if position in ['left','bottom']:
        orientation = 'horizontal'
        pad = 0.2
    cax = divider.append_axes(position,size='5%',pad=pad)
    plt.colorbar(im,cax=cax, orientation = orientation)
    if position == 'left':
        cax.yaxis.set_ticks_position('left')
    if position == 'bottom':
        cax.xaxis.set_ticks_position('bottom')
    
def dimg(z,fig=None,sub=None,title=None,ap=None,extent=None,stride=None,image=True,contour=False,levels=None,surface=False,cmap='bone',origin='upper',colorbar=False,geometry=None):
    """Displays an array graphically.
    
    Usage: ret = dimg(theArray,[options])
    options:
        fig (None) - the existing figure window to draw in
        sub (None) - the subplot region to draw in, if any
        image - create a grey-scale image of the 2d data (default)
        contour - draw contour lines. can be combined with image
        surface - create a 3d surface rendering
        cmap ('bone') can be 'hot'
    returns:
        image object, contour object, (image,contour), or surface object
        depending on the option choices
    """
    if surface:
        image = False
        contour = False
    
    if contour:
        origin = 'lower'
    
    if (type(z) == list):
        movie(z,ap=ap)
        return
    
    if plt is None:
        print('<dimg> not available on this machine %s'%platform.node())
        print('<dimg> use dimg.movie([arg]) instead to force display using ds9')
        return
    
    assert isinstance(z,np.ndarray),'<dimg> ERROR argument must be a 2d array'
    
    if (len(z.shape) == 3):
        movie(z,ap=ap)
        return
    
    if (ap != None):
        z = z*ap
    
    plt.ion()
    
    try:
        thefig = plt.figure(fig)
    except:
        if (type(fig) == type('string')):
            if (title is None):
                title = fig
            fig = None
    
        thefig = plt.figure(fig)

    if (fig is not None) and (sub is None):
        plt.clf()
    
    if (sub is not None):
        ax = plt.subplot(sub)
    else:
        ax = plt.subplot(111)

    if (title is not None):
        plt.title(str(title))    
    
    def format_coord(xx, yy):
        col = int(xx+0.5)
        row = int(yy+0.5)
        if extent is not None:
            l,r,b,t = extent
            row = int(((yy-b)/(t-b))*numrows)
            col = int(((xx-l)/(r-l))*numcols)
            if (origin != 'lower'):
                row = numrows-row-1
        if col>=0 and col<numcols and row>=0 and row<numrows:
            #return 'x%1.2fy%1.2f[r%1.0fc%1.0f]v%1.2f'%(xx,yy, row,col, z[row,col])
            return 'x%1.2f y%1.2f [r%1.0fc%1.0f] z%1.2f'%(xx,yy, row,col, z[row,col])
        else:
            return 'x=%1.4f, y=%1.4f'%(xx, yy)

    numrows, numcols = z.shape
    if extent is None:
        x = range(numcols)
        y = range(numrows)
    else:
        x = np.linspace(extent[0],extent[1],numcols)
        y = np.linspace(extent[2],extent[3],numrows)
    
    im = None
    if image:
        im = ax.imshow(z,extent=extent,cmap=cmap,interpolation='nearest',origin=origin)
        ax.format_coord = format_coord
        if (colorbar):
            plt.colorbar(im)
        ret = im
    if contour:
        if levels is None:
            if image:
                cs = plt.contour(z,extent=extent,colors='lightgreen')
            else:
                cs = plt.contour(z,extent=extent,colors='black')
        else:
            if image:
                colors = None
            else:
                colors = 'black'
                ax.format_coord = format_coord
            cs = plt.contour(z,extent=extent,levels=levels,colors=colors)
            plt.clabel(cs)
        plt.clabel(cs)
        if image:
            ret = (im,cs)
        else:
            ret = cs
    if surface:
        # set the stride to keep from plotting too many points, unless the stride is overridden
        surface_maxpts = 20
        if stride is None:
            n,m = z.shape
            stride = np.array([n,m])/surface_maxpts
            stride = np.clip(stride,1,np.inf).astype(int)
        ax = plt.gca(projection='3d')
        x2,y2 = np.meshgrid(x,y)
        z2 = z.copy()
        if (isinstance(z,np.ma.core.MaskedArray)):
            x2 = np.ma.array(x2,mask=z.mask).compressed()
            y2 = np.ma.array(y2,mask=z.mask).compressed()
            z2 = z2.compressed()
            surf = ax.plot_trisurf(x2,y2,z2,cmap=cmap)
        else:
            surf = ax.plot_surface(x2,y2, z2, rstride = stride[0], cstride = stride[1], cmap=cmap)
        ret = surf

    if geometry is not None:
        pass
        #mgr = plt.get_current_fig_manager()
        #mgr.window.geometry(geometry)
    
    def _onclick(event):
        #print 'figure = '+thefig.__repr__()
        #print 'button=%d, x=%d, y=%d, xdata=%f, ydata=%f'%(
        #    event.button, event.x, event.y, event.xdata, event.ydata)
        #print 'xlimits: '+str(plt.xlim())+'  ylimits: '+str(plt.ylim())
        if (not image):
            return
        toolstate = thefig.canvas.manager.toolbar._active
        if (toolstate is None and event.button == 3):
            z_range = np.array([z.min(),z.max()])
            x_range = plt.xlim()
            y_range = plt.ylim()
            brightness = (event.ydata - y_range[0])/(y_range[1]-y_range[0])
            contrast = (event.xdata - x_range[0])/(x_range[1]-x_range[0])
            dz = z_range[1]-z_range[0]
            mid = (z_range[0]+z_range[1])/2.
            z1 = z_range[1] - ((brightness - 0.5)/0.5)*dz
            dz = dz*contrast*4
            z0 = z1 - dz
            #print 'brightness = '+str(100*brightness)+' %'
            #print 'contrast = '+str(100*contrast)+' %'
            ax = plt.subplot(111)
            ax.imshow(z,vmin=z0,vmax=z1,extent=extent,cmap=cmap,interpolation='nearest',origin=origin)

    cid = thefig.canvas.mpl_connect('button_press_event', _onclick)
    return ret

def ds9(arr2d,ap=None):
    """
    Displays a 2-d array using ds9
    """
    movie([arr2d],ap)

def movie(arr3d,ap=None):
    """
    Displays a 3-d data cube using ds9
    The array can be a 3-dimensional array or a list of 2-d arrays
    ap is an optional aperture window outside of which values are not displayed
    """
    if (type(arr3d) == list):
        movie(np.array(arr3d),ap=ap)
        return
    if (type(arr3d) != np.ndarray):
        print('<movie> ERROR argument must be an array')
        return
    if (len(arr3d.shape) != 3):
        print('<movie> ERROR array argument must have 3 dimensions')
        return
    if (ap != None):
        for i in range(arr3d.shape[0]):
            arr3d[i,:,:] *= ap
    os.system("rm -f temp_ds9_array.fits")
    fits.writefits(arr3d,'temp_ds9_array.fits')
    os.system("ds9 temp_ds9_array.fits &")

def play(arr3d,dt_ms=33,ap=None,cmap='bone',label = 'time',times=None,tunits='',**kwargs):
    """play(arr3d,ap=None,rate=1000,loop=False)
    Displays a 3-d data cube using matplotlib
    The array can be a 3-dimensional numpy array or a list of 2-d numpy arrays
    ap is an optional aperture window outside of which values are not displayed
    
    :param ndarray arr3d: the data cube
    :param int dt_ms: frame interval, in milliseconds
    :param ndarray ap: an optional aperture, same size as one frame
    :param str cmap: color map
    :param str label: prepend the title above the picture
    :param times list: a list of floats, ints, or strings - the variable field in the title, one per frame
    :param str tunits: postpend the title above the picture
    :param dict kwargs: additional arguments to be passed to imshow
    
    """
    #global k,frames,ani,pause, step
    
    arr3d = np.array(arr3d)
    if ap is not None:
        arr3d = arr3d*ap
    
    vmin = arr3d.min()
    vmax = arr3d.max()
    
    shape = arr3d.shape
    if (len(shape) == 3):
        nt,nr,nc = shape
        frames = arr3d
    if (len(shape) == 4): # this is a block display
        na,nt,nr,nc = shape
        frames = arr3d.transpose([1,2,0,3]).reshape((nt,nr,na*nc))
    if (len(shape) == 5):
        na,nb,nt,nr,nc = shape
        frames = arr3d.transpose([2,0,3,1,4]).reshape((nt,na*nr,nb*nc))

    numrows, numcols = frames[0].shape
    
    fig = plt.figure()
    ax = fig.gca()

    im = plt.imshow(frames[0],vmin=vmin,vmax=vmax,cmap=cmap,interpolation='nearest',animated=True,**kwargs)

    tr = fig.transFigure
    wi,hi = 0.024, 0.02
    rarrow = FancyArrow(0.95,0.5,0.01,0.0,transform=tr,width=wi,head_length=hi,fill=None,color='black')
    larrow = FancyArrow(0.05,0.5,-0.01,0.0,transform=tr,width=wi,head_length=hi,fill=None,color='black')
    fig.patches.extend([rarrow,larrow])

    n = len(frames)
    if times is None:
        times = np.arange(float(n))
    t0 = times[0]
    ttype = type(t0)
    if isinstance(t0,float):
        ttype = float
        tstr = '%8.4f'%float(times[0])
    elif isinstance(t0,int):
        ttype = int
        tstr = '%8d'%int(times[0])
    elif isinstance(t0,str):
        ttype = str
        tstr = '%s'%times[0]
    else:
        tstr = 'ERROR1 %r'%ttype
    plt.title(label+' '+tstr+' '+tunits)
    
    k = 0
    z = frames[0]
    n = len(frames)
    step = +1
    
    def updatefig(*args):
        #global k
        #print '<updatefig> ---------------'
        #print args
        c = args[1]
        fig = c.figure
        plt.figure(fig.number)
        #print c
        #print '---------------------------'

        if hasattr(c,'now'):
            now = time.time()
            if now - c.now > 0.5: # seconds
                rarrow.set_fill(False)
                larrow.set_fill(False)

        z = c.frames[c.k]
        im.set_array(z)
        if ttype == float:
            tstr = '%8.4f'%float(times[c.k])
        elif ttype == int:
            tstr = '%8d'%int(times[c.k])
        elif ttype == str:
            tstr = '%s'%times[c.k]
        else:
            tstr = 'ERROR2 %r'%ttype
        #tstr = '{:8.4f}'.format(float(times[c.k]))
        plt.title(label+' '+tstr+' '+tunits)
        if not c.pause:
            if c.report_time:
                if (c.k == 0):
                    c.t0 = time.time()
                if (c.k+c.step) >= n:
                    t1 = time.time()
                    print('display rate:',float(n)/(t1-c.t0),'Hz')
                    c.report_time = False
            c.k = (c.k+c.step) % n
        return im,
    
    def onClick(event):
        #global pause
        c = event.canvas
        #c.pause ^= True
        #---------
        c.now = time.time()
        fsize = fig.get_size_inches()*fig.dpi
        direc = ['left','right'][event.x > fsize[0]/2.]
        if direc == 'left':
            larrow.set_fill(True)
            c.pause = False
            c.step = -1
            updatefig(0,c)
            c.pause = True
            c.step = +1
        if direc == 'right':
            rarrow.set_fill(True)
            c.pause = False
            c.step = +1
            updatefig(0,c)
            c.pause = True
    
    def onKey(event):
        #global pause, step
        #print event.key
        c = event.canvas
        c.now = time.time()
        if event.key == 'escape':
            c.frames = 0
            plt.close()
        if event.key == 'right':
            rarrow.set_fill(True)
            c.pause = False
            c.step = +1
            updatefig(0,c)
            c.pause = True
        if event.key == 'left':
            larrow.set_fill(True)
            c.pause = False
            c.step = -1
            updatefig(0,c)
            c.pause = True
            c.step = +1
        if event.key == ' ':
            c.pause ^= True        
    
    pause = False
    fig.canvas.mpl_connect('button_press_event',onClick)
    fig.canvas.mpl_connect('key_press_event',onKey)
    fig.canvas.k = k
    fig.canvas.frames = frames
    fig.canvas.pause = pause
    fig.canvas.step = step
    fig.canvas.report_time = True
    fig.canvas.t0 = 0.
    ani = animation.FuncAnimation(fig,updatefig,fargs = [fig.canvas],interval=dt_ms,blit=False)
    fig.canvas.ani = ani
    plt.show()
    print('instructions:')
    print('  space or left mouse click: pause')
    print('  right arrow: advance one frame forward')
    print('  left arrow: go back one frame')
    print('  escape: exit')

class SubplotAnimation(animation.TimedAnimation):
    '''Plot multiple array lists in an animation
    '''
    def __init__(self,dl,cmap=None,origin='lower',title='',stride=1,fig=None):
        ''' dl is a list ('listA') of data-cubes (list of 2-d arrays).
        Each data cube must have identical depth - this will be the 'time duration'
        of the animation. The display will have subplots, one per data-cube
        displayed in one horizontal row on the figure
        (so it only makes sense to have a few, say < ~5, of them)
        '''
        n_axes = len(dl)
        n_frames = len(dl[0])
        for d in dl:
            assert len(d) == n_frames,'Error: all lists must be the same length'
        if fig is None:
            fig = plt.figure()
        self.fig = fig
        self.title = title
        self.axes = []
        for i in range(n_axes):
            self.axes.append(fig.add_subplot(1,n_axes,i+1))
        self.n_frames = n_frames
        self.data = dl
        self.im = []
        for ax,d in zip(self.axes,self.data):
            z=np.array(d)
            vmin = z.min()
            vmax = z.max()
            im = ax.imshow(z[0],cmap=cmap,vmin=vmin,vmax=vmax)
            self.im.append(im)
        
        fig.canvas.mpl_connect('key_press_event',self.onKey)
        self.paused = False
        self.frame = 0
        self.stride = stride
            
        animation.TimedAnimation.__init__(self,fig,interval=50, blit=False)
    
    def _draw_frame(self, i):
        if self.paused: return
        for im,d in zip(self.im,self.data):
            z = np.array(d[self.frame])
            im.set_array(z)
        self.fig.suptitle(self.title+' frame %i'%self.frame)
        self.frame = (self.frame+self.stride) % self.n_frames
    
    def new_frame_seq(self):
        return iter(range(self.n_frames))
    
    def onKey(self,event):
        c = event.canvas
        if event.key in ['escape','q']:
            plt.close(self.fig)
        elif event.key == ' ':
            self.paused ^= True
        elif event.key in ['up','u']:
            self.paused = False
            self.frame = (self.frame-self.stride+1) % self.n_frames
            self._draw_frame(0)
            self.paused = True
        elif event.key in ['down','d']:
            self.paused = False
            self.frame = (self.frame-self.stride-1) % self.n_frames
            self._draw_frame(0)
            self.paused = True
        elif event.key == '0':
            self.frame = 0
            if self.paused:
                self.paused = False
                self._draw_frame(0)
                self.paused = True

#---------------------- Tests ----------------

def test_play():
    global frames
    frames = np.random.normal(size=(100,100,100))
    play(frames)

def test_example(surface=False):
    """Run a simple example with generated dummy data
    """
    global x,y,z,extent
    
    npts = 100
    stride = [1,1]
    if surface:
        stride = np.fix(np.array([1,1])*(npts/20.)).astype('int')
    x = np.linspace(-2*np.pi,2*np.pi,npts)
    x,y = np.meshgrid(x,x)
    z = x**2*y**3
    extent = np.array([-1,1,-1,1])*2*np.pi
    dimg(z,origin='lower',extent=extent,surface=surface,stride=stride,colorbar=True)

def test():
    """Regression-test the dimg package
    """
    f = '(x^2)*(y^2)'
    test_example(); plt.title('image '+f)
    test_example(surface=True); plt.title('surface '+f)
    levels = np.logspace(0,5,num=6)
    levels = np.hstack([-levels[::-1],0,levels])
    img,cs = dimg(z,contour=True,origin='lower',extent=extent,levels=levels); plt.title('image & contour '+f)
    cs = dimg(z,image=False,contour=True,extent=extent,levels=levels); plt.title('contour '+f)
    plt.grid('on')
