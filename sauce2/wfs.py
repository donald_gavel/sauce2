"""
Simulate the ShaneAO Hartmann Wavefront Sensor

*New in version 3*: Option to use the GPU to speed up the Shack-Hartmann wave
optic simulation.

The class :class:`Wfs` will support the ability to do imaging of a single-shot
wavefront onto the wfs CCD, imaging and wavefront sensing through a centroiding and
reconstruction algorithm, feedback correction using a :class:`dm.DM` object,
to full-up simulation both open and closed loop in conjunction with :class:`atmos.Screen`
and :class:`atmos.BlowingScreen` objects.

**Use Case 1**

Open loop simulation with Blowing Screen of atmosphere

.. code-block:: python

    from sauce2 import wfs,atmos
    w = wfs.Wfs()
    bs = atmos.BlowingScreen(n=w.N,du=w.du)
    run_summary = w.simulate(bs,nt=100)
    w.save_telemetry('Data_0000.fits',directory='saved_data')

**Use Case 2**

A full-up simulation would take the form

.. code-block:: python
    
    w = wfs.Wfs()
    bs = atmos.BlowingScreen()
    dm = deformable_mirror.DM()
    w.deformable_mirror = dm
    w.simulate(bs,nt=4096,loop='closed')
    w.save_telemetry('Data_0000.fits')

**Use Case 3**

Fitting a phase screen with a Zernike series, given the wfs response to the phase screen:

.. code-block:: python

    w = wfs.Wfs()
    a = atmos.Screen()
    a.gen_screen(shape=(w.N,w.N))
    w.ccd_image(a.screen.to_units('radians',w.wavelength))
    w.recon()
    c = w.mode_fit(slopes=w.s, mode_kind='Zernike')

**Use Case 4**

Fitting a telemetry data set to a Zernike series

.. code-block:: python

    import wfs,atmos,zernike
    from info_array import InfoArray, InfoMatrix, ufloat
    
    w = wfs.Wfs()
    b = atmos.BlowingScreen(r0=ufloat(12,'cm'))
    z = zernike.Zernike(n=w.N, R=w.ap.r)
    z.calc_set(nmax=5)
    H = InfoMatrix(np.empty(( len(z.zset), 2*w.n_subaps)),
                   name='H',
                   units = 'ccd pixel per nm',
                   doc='Zernike responses',)
    k = 0
    for ph in z.zset:
        w.simulate(b, ph=ph, nt=4096)
        H[:,k] = w.mean_slopes()
        k += 1
    H.r0 = b.r0
    H.navg = 4096
    H.writefits('H_12.0cm.fits')

    H = InfoArray.fromFits('H_12.0cm.fits')
    w.simulate(b, nt=4096, save_to='data_000.fits')
    z_coefs_0 = mystical_analysis('data_000.fits',H)
    w.simulate(b, nt=4096, save_to='data_001.fits')
    z_coefs_1 = mystical_analysis(s,'data_001.fits',H)
    ...
    
**Use Case 5**

Interactive command session with simulator running in a separate thread

.. code-block:: python

    import wfs
    import terminal_user_interface as tui
    # uses the cmd package from https://pymotw.com/2/cmd/
    # see interp.py in the sauce directory
    
    w = wfs.Wfs()
    b - atmos.BlowingScreen(r0 = ufloat(12,'cm'))
    w.simulate(b,thread=True)
    tui.start(w)
    > open
    > close
    > gain 0.9
    > data
    ...
    > sim atmos r0 10 cm v 10 m/s
    > sim star mv 10
    > close
    ...
    > sim help
    > sim fiber mv 8
    > sim help
    > sim fiber dark
    > help
    > dark
    > sim fiber mv 8
    > refcent
    ...
    > script cmds.txt
    ...
    > quit
    
"""
import os
import sys
import numpy as np
from sauce2 import oprint
from sauce2.oprint import gist
from sauce2 import fits
import configparser
from sauce2 import dotdict
from sauce2.dotdict import DotDict
from sauce2 import img
from sauce2 import dimg
import pyopencl as cl
import pyopencl.array as cl_array
import pyopencl.clmath as cl_math
if sys.version_info.major == 3:
    import reikna
else:
    from pyfft.cl import Plan
import time
import tqdm
import platform
import re
import astropy.units as u
import astropy.io.fits as pyfits
import matplotlib.pyplot as plt
import matplotlib.patches as patches

greek_lambda = u'\u03BB'
greek_mu = u'\u03BC'
micron_units = greek_mu+'m'

machine = platform.node().split('.')[0]
__path__ = os.path.dirname(__file__)
CLfile = os.path.join(__path__, 'wfs.cl')
defaultConfigfile = os.path.join(__path__, 'wfs.cfg')

telescope = DotDict({
    'name': 'Shane 3m',
    'D': 3.0*u.m,
    'Ds': 0.8*u.m,
    'fno': 17.,
    'f': 51.*u.m,
    'platescale': (1.*u.radian/(51.*u.m)).to(u.arcsec/u.mm),
})

dangles = u.dimensionless_angles()

class Wfs(object):
    """
    Wfs is the wavefront sensor class. This is generally configured for
    ShaneAO, but should be adapable to any wavefront sensor geometry.
    In ShaneAO subapertures on the Hartmann wavefront sensor are aligned to actuators
    on the tweeter DM. See also :py:mod:`dm`.
    
    Future possibility include expanding to Pyramid wavefront sensor models.
    
    The instantiaiton of a wavefront sensor object requires a configuration file,
    `wfs.cfg` which can be in the user's directory or with fallback to a default
    configuration, which is ShaneAO 16-across ('16x').
    """
    def __init__(self,mode='16x',configfile=defaultConfigfile,CLfile=CLfile,real_ccd_pix=False):
        """
        :param str mode: choice of '8x', '16x', or '30x'
        :param str configfilename: name of the configuration file
        :param bool real_ccd_pix: use a ccd that holds real values, rather the the default uint16
        """
        assert os.path.isfile(configfile)
        # todo: valid_modes = ['8x','16x','30x']
        valid_modes = ['16x']
        assert mode in valid_modes, 'mode must be one of '+valid_modes
        mode_index = valid_modes.index(mode)
        self.mode = mode
        self.n_zernike_modes = 45
        if not os.path.isfile(configfile):
            print('<wfs.__init__> %s not in current directory, using system default at %s'%(configfile,defaultConfigfile))
            configfile = defaultConfigfile
        self.configfile = configfile
        self.CLfile = CLfile
        
        cp = configparser.ConfigParser()
        cp.optionxform = str # keep the keys case sensitive
        cp.read(configfile)
        
        ccd_hdr = typize(dict(cp.items('ccd')))
        with u.set_enabled_equivalencies([(u.pix,None)]):
            shape = [int(x) for x in [ccd_hdr.nx, ccd_hdr.ny]]
        dtype = np.__dict__[ccd_hdr.data_type]
        if real_ccd_pix:
            dtype = np.float
        ccd_hdr.bias = u.Quantity(ccd_hdr.bias,dtype=dtype)
        self.ccd = u.Quantity(np.zeros(shape),ccd_hdr.unit,dtype=dtype)
        self.ccd.__dict__.update(ccd_hdr)
        self.ccd.x0 = [ccd_hdr.x0, ccd_hdr.y0]

        wfs_hdr = typize(dict(cp.items('wfs')))
        wfs_hdr.update(typize(dict(cp.items(mode))))
        
        self.__dict__.update(wfs_hdr)
        self.paramDir = os.path.join(__path__,self.paramDir)
        
        self.imap_file = os.path.join(self.paramDir,self.imap_file)
        self.imap,self.imap_hdr = fits.readfits(self.imap_file,return_header=True)
        self.n_subaps = self.imap_hdr['NS']
        self.n_subaps_doc = 'number of subaps'
        
        self.umap_file = os.path.join(self.paramDir,self.umap_file)
        self.umap,self.umap_hdr = fits.readfits(self.umap_file,return_header=True)
        assert self.n_subaps == self.umap_hdr['NS'],'umap and imap files dont match'
        
        assert self.n_subaps*self.n_p.value**2 == self.umap.shape[0],'umap and subap definitions dont match'

        self._fine_sample(cp)
        self._gen_subaps()
        self._subap_windows()
        self.ccd_image_prep()
        self.ccd_image_gpu_prepped = False
        self.phi = None
        self.ccd_image(calibrate=True)
        
    def pprint(self):
        """
        Pretty-print the object's contents
        """
        pprint(self)

    def __getattr_(self,name):
        if name in ('hdr','header'):
            self.pprint()
        
    def set_gs_brightness(self,nph,per='frame'):
        """
        Set the brightness of the guide star source
        in total photons per frame. Internally it accordingly adjusts the photon_factor.
        
        :param int nph: number of photons
        :param str per: interpretation of nph - 'frame', 'subap'
        """
        nph = nph*u.ph
        if per == 'frame':
            self.nph = nph/u.frame
        elif per == 'subap':
            self.nph = nph*self.n_subaps/u.frame
    
    def _fine_sample(self,cp):
        """
        Set the fine sample parameters that will make Fraunhofer propagtion
        consistent with sampling in both pupil and focal planes. The wavelength
        and zero-pad buffer sizes will be adjusted.
        
        This uses the algorithm described in the jupyter notebook Sauce.ipynb
        
        This is an internal routine called during object initialization.
        """
        sampling = typize(dict(cp.items('%s_sampling'%self.mode)))
        # make angles dimensionless:
        with u.set_enabled_equivalencies(u.dimensionless_angles()):
            
            d,d_pri,p,n_p = self.d, self.d_pri, self.pixel_scale, self.n_p
            n_sa,n_fp = sampling.n_sa, sampling.n_fp
            
            fov = n_p*p ; fov.doc = 'field of view of subaperture in focal plane'
            du = d / n_sa ; du.doc = 'fine sample, pupil plane'
            N = img.nextpow2(d_pri / du)
            N = u.Quantity(N,dtype=int)
            N.doc = 'number of fine pixels, pupil plane'
            dtheta = p / n_fp ; dtheta.doc = 'fine sample, focal plane'
            wavelength = 0.5*u.micron
            n = wavelength / (du*dtheta)
            n = int(n)
            n = img.nextpow2(n)
            n = u.Quantity(n,dtype=int) ; n.doc = 'fine pixels in subap FFT'
            wavelength = (n*du*dtheta).to('micron') ; wavelength.doc = 'wavelength of guidestar light'
        
        self.sampling = sampling
        self.sampling_doc = 'original requested fine sampling parameters'
        self.__dict__.update({'N':N,
                              'n_sa':n_sa,
                              'n_fp': n_fp,
                              'fov':fov,
                              'n_fft': n,
                              'dtheta':dtheta,
                              'wavelength':wavelength,
                              'du':du,
                              'subap_FOV':fov,
                                })

    def _create_aperture(self):
        """
        Define the wfs aperture
        
        This is used internally, called at object initialization.
        """
        N,D,Ds,du = self.N,self.d_pri,self.d_sec,self.du

        c = np.array([N,N])/2. - 0.5
        r = D / 2.0 / du
        rs = Ds / 2.0 / du
        ap = img.circle((N,N),c=c,r=r) - img.circle((N,N),c=c,r=rs)
        ap = u.Quantity(ap)
        ap.name = 'WFS aperture'
        ap.longName = 'ShaneAO Wavefront Sensor Aperture'
        ap.dx = du
        ap.x0 = [-N*du/2,-N*du/2]
        self.ap = ap

    def _calc_subap_locs(self):
        """
        Figure out the locations of each subaperture.
        This is an internal routine, called during object initialization
        """
        mode = self.mode
        if mode in ['16x','30x']:
            n = self.n_across
            ns = self.n_across_secondary
            c = np.array([n,n])/2. - 0.5
            ap = img.circle((n,n),c=c,r=n/2) - img.circle((n,n),c=c,r=ns/2)
        elif mode == '8x':
            n = 8
            ns = 2
            c = np.array([n,n])/2. - 0.5
            ap = img.circle((n,n),c=c,r=n/2-.25) - img.circle((n,n),c=c,r=ns/2)
        x = np.arange(n) - c[0]
        y = x
        x,y = np.meshgrid(x,y)
        x = np.ma.MaskedArray(x,mask=1-ap)
        y = np.ma.MaskedArray(y,mask=1-ap)
        self.subapLocs = [x.compressed(),y.compressed()]
        self.subapLocs_units = 'subap diameters'
        self.subapLocs_doc = 'center of subap relative to center of ap'
        assert self.n_subaps == len(self.subapLocs[0]),str(self.n_subaps)+','+str(len(self.subapLocs[0]))
        self._gen_subaps()

    def _gen_subaps(self):
        """
        Generate a set of windows that define the subaperture regions on the fine grid.
        This is an internally used routine called during object initialization.
        """
        if not hasattr(self,'subapLocs'):
            self._calc_subap_locs()
        subaps = []
        subap_ind = []
        if not hasattr(self,'ap'):
            self._create_aperture()
        ap = self.ap
        n,m = ap.shape
        # define a base subap
        n_sa = int(self.n_sa)
        for i in range(self.n_subaps):
            x0 = int((self.subapLocs[0][i]-0.5)*self.d / self.du) + n//2
            y0 = int((self.subapLocs[1][i]-0.5)*self.d / self.du) + n//2
            subap_ind.append([y0,y0+n_sa,x0,x0+n_sa])
        
        self.subap_ind = subap_ind
        self.subap_ind_units = '(fine grid pixels)'
        self.subap_ind_doc = 'bounding boxes [first row,last row,first col,last col] of each subap on the fine grid'

    def _subap_windows(self):
        '''create a data cube of subap windows, apertured by the overall aperture, and a
        dataset of subaperture information, including illumination fraction,
        that is stored internally in a structure which can be converted easily to a pandas DataFrame.
        '''
        n_sa = self.n_sa
        df_cols = ['subap_number','loc_x','loc_y','subap_ind','illum_frac']
        df_col_units = ['','subap diameters','subap diamgeters','fine pixels','fraction 0...1']
        df_data = []
        nf = float(n_sa)**2
        winds = []
        subapLocs = map(list,list(np.array(self.subapLocs).transpose()))
        for k, (x,y), bbox in zip(range(self.n_subaps),subapLocs,self.subap_ind):
            y0,y1,x0,x1 = bbox
            winds.append( self.ap[y0:y1,x0:x1] )
            illum_frac = float(self.ap[y0:y1,x0:x1].sum() / nf)
            df_data.append([k,x,y,bbox,illum_frac])
        doc_str = "convert this to pandas data frame with df = pd.DataFrame(w.subap_info['data'],columns=w.subap_info['columns'])"
        self.subap_info = {'columns':df_cols,'col_units':df_col_units,'data':df_data,'doc':doc_str}
        self.subap_info_doc = doc_str
        ws = []
        for k,df_row in enumerate(df_data):
            d = dict(zip(df_cols,df_row))
            k,f = d['subap_number'], d['illum_frac']
            label = 'subap# %d, illum= %0.0f%%'%(k,f*100.)
            w = winds[k]
            w.subap_info = d
            w.label = label
            w.name = w.longName = 'subaperture window'
            ws.append(w)
        self.subap_info['windows'] = ws

    def illum_frac(self,frac=0.99):
        '''return a list of subaperture numbers that have illumination greater than frac
        '''
        return [x[0] for x in self.subaps['data'] if x[self.subaps['columns'].index('illum_frac')] > frac]
        
    def ccd_image_prep(self,check=(False,False)):
        """
        Help the process of CCD imaging
        by preparing several indirect maps
        See: sauce_worksheet.xlsx
        """
        N,n_subaps,n_fft,n_sa = [int(x) for x in [self.N,self.n_subaps,self.n_fft,self.n_sa]]
        
        # ------- fine grid to zero-padded subap block ------
        ys0,xs0 = (n_fft//2-n_sa//2,n_fft//2-n_sa//2) # locations in zero-padded block
        ys1,xs1 = (ys0+n_sa,xs0+n_sa)
        
        x = range(N)
        x,y = np.meshgrid(x,x)
        ind_from = x + y*N
        x = range(n_fft)
        x,y = np.meshgrid(x,x)
        ind_to = (x + y*n_fft)
        pmap_from = [] # from positions on the fine phase grid
        pmap_to = [] # to positions in the data cube of zero-padded subaps
        for box,k in zip(self.subap_ind,range(n_subaps)):
            y0,y1,x0,x1 = box
            sub_from = ind_from[y0:y1,x0:x1]
            pmap_from.append(sub_from)
            sub_to = ind_to[ys0:ys1,xs0:xs1]
            pmap_to.append(sub_to + k*n_fft*n_fft)
        self.pmap_from = np.array(pmap_from).flatten()
        self.pmap_from_doc = 'maps of subap pixels on fine-grid'
        self.pmap_to = np.array(pmap_to).flatten()
        self.pmap_to_doc = 'map of subap pixels within zero-padded block'
        # zipped version for the GPU
        self.pmap = np.array([pmap_from,pmap_to]).transpose().flatten().astype(np.int32)
        self.pmap_doc = 'Pupil map_from->map_to in a format needed for a GPU data transfer'
        
        if check[0]:
            q = np.zeros((N,N)).flatten()
            q[self.pmap_from] = self.pmap_from # 1.0
            #dimg.show(u.reshape((N,N)),title='pmap_from')
            q = u.Quantity(q.reshape((N,N)))
            q.name='pmap_from'
            dimg.show(q)

            q = np.zeros((n_subaps,n_fft,n_fft)).flatten()
            q[self.pmap_to] = self.pmap_to # 1.0
            #dimg.play(u.reshape((n_subaps,n_fft,n_fft)),label='pmap_to')
            q = u.Quantity(q.reshape((n_subaps,n_fft,n_fft))[0])
            q.name = 'pmap_to[0]'
            dimg.show(q)

        # ---------- CCD map ----------
        with u.set_enabled_equivalencies([(u.Unit('pix'),None)]):
            n_p,n_sa,n_fp = [int(x) for x in [self.n_p,self.n_sa,self.n_fp]]
        # n_p = self.n_p # ccd pixels across subap
        # n_sa = self.n_sa # fine pixels per subap
        # n_fp = self.n_fp # fine pixels per CCD pixel
        
        readoff = (np.arange(n_p)-n_p//2)*n_fp + n_fft//2 # places in the fine grid to read off CCD readings after pixel filtering
        x,y = np.meshgrid(readoff,readoff)
        readoff1d = (x+n_fft*y).flatten()
        mapi_from = np.zeros((n_subaps,n_p*n_p))
        for k in range(n_subaps):
            mapi_from[k,:] = k*n_fft*n_fft+readoff1d
        
        mapi_from = mapi_from.flatten().astype(np.int32)
        mapi_to = self.umap.astype(np.int32)
        #mapi_to = self.imap.astype(np.int32)
        self.mapi_from = mapi_from
        self.mapi_from_doc = 'map of filtered pixels in zero-padded block representing the CCD pixel box-car average'
        self.mapi_to = mapi_to
        self.mapi_to_doc = 'map to pixels in CCD array'
        # zipped version for the GPU
        self.ccd_map = np.array([mapi_from,mapi_to]).transpose().flatten().astype(np.int32)
        self.ccd_map_doc = 'CCD map_from->map_to in format needed for GPU data transfer'

        if check[1]:
            q = np.zeros((n_subaps,n_fft,n_fft)).flatten()
            q[self.mapi_from] = 1.0
            #dimg.play(u.reshape((n_subaps,n_fft,n_fft)),label='mapi_from')
            q = u.Quantity(q.reshape((n_subaps,n_fft,n_fft))[0])
            q.name='mapi_from'
            dimg.show(q)

            q = np.zeros_like(self.ccd).flatten()
            q[self.mapi_to] = 1.0
            #dimg.show(u.reshape(self.ccd.shape),title='CCD mapi_to')
            q = u.Quantity(q.reshape(self.ccd.shape))
            q.name='CCD mapi_to'
            dimg.show(q)
        # -----------------------------
        
        # --------- CCD pixel ---------
        pixel = np.ones((n_fp,n_fp))
        pixel = img.zeropad(pixel,(n_fft,n_fft))
        fpixel = np.fft.fft2(pixel).astype(np.complex64)
        self.pixel = u.Quantity(pixel)
        self.pixel.name='CCD pixel'
        self.pixel.dx = self.dtheta
        self.pixel.x0 = [-n_fp*self.dtheta/2]*2
        self.fpixel = u.Quantity(fpixel)
        self.fpixel.name='CCD pixel MTF'
        
        self.ccd_image_prepped = True

    def ccd_image(self,phi=None,graphic=False,add_noise=True,full_frame_noise=False,calibrate=False):
        """
        Create the CCD image of Hartmann dots
        given the fine-grid phase. Store the result as an instance variable w.ccd, where w is the Wfs object instance.
        The calculation is done entirely using the CPU. For the GPU version, see :meth:`ccd_image_gpu`
        
        :param Quantity phi: a phase screen, with shape = (w.N,w.N), sampled at w.du and with length units
        :param bool graphic: whether or not to display the CCD image
        :param bool add_noise: add read noise and photon noise to the image
        :param bool calibrate: do a photometric calibration with a flat wavefront - sets "photon_factor"
        
        The resulting CCD image is not returned, but stored in the instance varaible
        ``self.ccd``.
        """
        if calibrate:
            self.phi = None
            phi = None
            add_noise = False
            self.photon_factor = 1.0
            nph_save = self.nph
            self.nph = u.Quantity(1.,'ph/frame')
        elif not self.ccd.calibrated:
            raise Exception('CCD not calibrated')

        n,m = self.fpixel.shape
        n_subaps = self.n_subaps
        pixel_scale = self.pixel_scale
        ccd_tilt = self.ccd_tilt
        du = self.du
        wavelength = self.wavelength
        N = self.N
        photon_factor = self.photon_factor
        nph = self.nph.decompose().value
        
        # *** tx and ty need to be set to correct CCD steering ***
        if calibrate:
            tx,ty = ccd_tilt*pixel_scale
            self.phi_adjust = tilt(self.ap,[tx,ty]).to_radphase(wavelength)
            # x = (np.arange(n)-n//2)*du
            # x,y = np.meshgrid(x,x)
            # k = 2*np.pi/wavelength
        i = 1j
        if phi is not None:
            assert phi.shape == (N,N)
            if isinstance(phi,u.Quantity):
                self.phi = phi.to_radphase(self.wavelength)
            else:
                self.phi = u.Quantity(phi,'radian')
                self.phi.name='phi'
                self.phi.dx = du
        if self.phi is None:
            self.phi = u.Quantity(np.zeros((N,N)),'radian')
            self.phi._wavelength = self.wavelength
            self.phi.name='phi'
            self.phi.dx = du
        self.phi += self.phi_adjust
        # =============== GPU function goes here ===============
        with u.set_enabled_equivalencies(u.dimensionless_angles()):
            wf = np.zeros((n_subaps,n,n)).astype(np.complex)
            wf.flat[self.pmap_to] = self.ap.flat[self.pmap_from]/float(n*n)
            wf.flat[self.pmap_to] *= (np.exp(i*self.phi.flat[self.pmap_from])).view(type=np.ndarray)
            #wf *= (np.exp(i*k*(tx*x+ty*y))).view(type=np.ndarray)
            wf = np.fft.fft2(wf)
            wf = wf*wf.conj()
            wf = np.fft.fft2(wf)
            wf = wf*self.fpixel
            wf = np.fft.ifft2(wf)
            wf = np.real(wf)
            pix = wf.flat[self.mapi_from]*(photon_factor*nph)  # normalizes the pix.sum()=nph with a flat wavefront
            nn = pix.shape[0]
            
            if calibrate:
                self.photon_factor = 1./pix.sum()
                self.nph = nph_save
                self.ccd.calibrated = True
            
            self.ccd.read_noise_added = add_noise
            self.ccd.photon_noise_added = add_noise
            if add_noise:
                noise = np.random.normal(size=nn)*np.sqrt(pix)
                read_noise = self.ccd.read_noise.decompose().value
                noise += np.random.normal(size=nn)*read_noise
                pix += noise
                
            self.ccd[:] = self.ccd.bias
            dtype = self.ccd.dtype
            unit = self.ccd.unit
            nbits = int(self.ccd.bit_shift/u.bit)
            bias = np.right_shift(self.ccd.bias.value,nbits)
            
            if full_frame_noise:
                noise = u.Quantity(np.left_shift((read_noise*np.random.normal(size=self.ccd.shape)+bias).astype(int),nbits),unit,copy=False)
                self.ccd[:] = noise.astype(dtype)
                
            if self.ccd.dtype.kind == 'u':
                self.ccd.flat[self.mapi_to] = u.Quantity(np.left_shift((pix+bias).astype(dtype),nbits),unit,copy=False)
            else:
                self.ccd.flat[self.mapi_to] += pix.astype(dtype)
            
            #self.ccd[:] += self.ccd.bias #1400
            
            if hasattr(self,'cam'):
                self.cam.ccd.flat[self.cam.smap] = self.ccd.flat
            self.ccd.comp_by = 'CPU'
        # ======================================================
        if graphic:
            #         Live Graphic
            if not hasattr(self.ccd,'fig'):
                self.ccd.fig = plt.figure()
                self.ccd.im = plt.imshow(self.ccd,cmap='hot',interpolation='nearest')
            if not plt.fignum_exists(self.ccd.fig.number):
                self.ccd.fig = plt.figure()
                self.ccd.im = plt.imshow(self.ccd,cmap='hot',interpolation='nearest')
            self.ccd.im.set_array(self.ccd)
            plt.draw()

    def ccd_image_gpu_prep(self,phtt=None):
        """
        Initialize the GPU
        """
        # gpu buffers are global
        global gpu_ap, gpu_aps, gpu_wf, gpu_fpixel, gpu_ccd, gpu_wf_map, gpu_ccd_map, gpu_phtt
        global gpu_photon_factor, gpu_nph0
        # gpu functions are global
        global queue, plan
        global prg, cset, movi_mp, cmpri, cmsq, cmul, uszero, rzero, movi_cr, movi_cis, rplusr
        # gpu sizes are global
        global n_subaps, n_subaps_n_n, n_ccd, n_subaps_nsub_nsub, n_ccd_map, nind
        
        assert os.path.isfile(self.CLfile)
        n = self.n_fft.value
        n_subaps = self.n_subaps
        n_p = self.n_p.value
        n_subaps_n_n = (n_subaps*n*n,)
        n_ccd = self.ccd.shape[0]
        n_subaps_nsub_nsub = (n_subaps*n_p*n_p,)
        n_ccd_map = (len(self.ccd_map)/2,)
        
        device = 1 # the GPU
        if machine == 'LAB-GPU':
            device = 0
        clplatform = cl.get_platforms()[0]
        devs = clplatform.get_devices()
        ctx = cl.Context(devices=[devs[device]])
        queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)        
        plan = Plan((n, n), queue=queue)
        with open(self.CLfile) as f:
            code = f.read()
        prg = cl.Program(ctx,code).build()
        rplusr = prg.rplusr
        cset = prg.cset
        movi_mp = prg.movi_mp
        cmpri = prg.cmpri
        cmsq = prg.cmsq
        cmul = prg.cmul
        rzero = prg.rzero
        uszero = prg.uszero
        movi_cr = prg.movi_cr
        movi_cis = prg.movi_cis
        if self.ccd.dtype == np.float32:
            uszero = prg.rzero
            movi_cis = prg.movi_crs
        
        ap = self.ap.astype(np.float32)
        aps = np.zeros((n_subaps,n,n),dtype=np.float32).flatten()
        aps[self.pmap_to] = ap.flatten()[self.pmap_from]
        aps = aps.reshape((n_subaps,n,n))
        wf = np.zeros((n_subaps,n,n)).astype(np.complex64).flatten()
        fpixel = np.zeros((n_subaps,n,n)).astype(np.complex64)
        fpixel[:,:,:] = self.fpixel
        ccd_pix_type = self.ccd.dtype
        #ccd = np.zeros(self.ccd.shape,dtype=np.uint16)
        ccd = np.zeros(self.ccd.shape,dtype=ccd_pix_type)

        gpu_ap = cl_array.to_device(queue,ap)
        gpu_aps = cl_array.to_device(queue,aps)
        gpu_wf = cl_array.to_device(queue,wf)
        gpu_fpixel = cl_array.to_device(queue,fpixel)
        gpu_ccd = cl_array.to_device(queue,ccd)
        gpu_wf_map = cl_array.to_device(queue,self.pmap)
        gpu_ccd_map = cl_array.to_device(queue,self.ccd_map)
        
        if phtt is not None:
            gpu_phtt = cl_array.to_device(queue,phtt.astype(np.float32))
            self.has_tt_adjust = True
        else:
            self.has_tt_adjust = False
        
        nind = (len(gpu_wf_map)/2,)
        
        self.platform, self.ctx, self.prg = platform,ctx,prg
        self.GPU = self.ctx.__str__()
        
        self.ccd_image_gpu_prepped = True
        
    def ccd_image_gpu(self,phi=None):
        """
        Create the CCD image of Hartmann dots
        given the fine-grid phase. Store the result as an instance variable w.ccd, where w is the Wfs object instance.
        The calculation is done entirely using the GPU. For the CPU version, see :meth:`ccd_image`

        :param array phi: the wavefront phase, must be float32 type.
            Despite the use of a keyword, phi is required. This is done to make
            the function call compatible with :meth:`ccd_image`
        
        The resulting CCD image is not returned, but stored in w.ccd.
        """
        assert phi is not None
        assert phi.dtype == np.float32
        n,m = phi.shape
        n_ph = (n*m,)
        if phi.unit != u.rad:
            phi = phi.to_radphase(self.wavelength)
        if not self.ccd_image_gpu_prepped:
            print('<ccd_image_gpu> preparing with a one-time call to ccd_image_gpu_prep')
            self.ccd_image_gpu_prep()
        gpu_photon_factor = .1# self.photon_factor*(0.27/1.e6)
        gpu_ph = cl_array.to_device(queue,phi)
        if self.has_tt_adjust:
            rplusr(queue,n_ph,None,gpu_ph.data,gpu_phtt.data)
        cset(queue, n_subaps_n_n, None, gpu_wf.data, np.complex64(0.))
        movi_mp(queue, nind, None, gpu_ap.data, gpu_ph.data, gpu_wf_map.data, gpu_wf.data)
        cmpri(queue, n_subaps_n_n, None, gpu_wf.data)
        plan.execute(gpu_wf.data,batch=n_subaps,wait_for_finish=False)
        cmsq(queue, n_subaps_n_n, None, gpu_wf.data)
        plan.execute(gpu_wf.data,batch=n_subaps,wait_for_finish=False)
        cmul(queue, n_subaps_n_n, None, gpu_wf.data, gpu_fpixel.data)
        plan.execute(gpu_wf.data,batch=n_subaps,inverse=True,wait_for_finish=False)
        uszero(queue, (n_ccd*n_ccd,), None, gpu_ccd.data)
        movi_cis(queue, n_ccd_map, None, gpu_wf.data, gpu_ccd_map.data, np.float32(gpu_photon_factor), gpu_ccd.data)
        ccd = gpu_ccd.get()
        
        self.ccd.data = ccd.data
        self.ccd.bias_added = False
        self.ccd.read_noise_added = False
        self.ccd.photon_noise_added = False
        self.ccd.comp_by = 'GPU'
        
    def show(self,tw=None):
        """
        Create a diagnostic image that shows the positions
        of the subapertures and pupil overlayed
        
        :param deformable_mirror.tweeter tw: (optional) a tweeter object which, if supplied, will display dots at actuator locations
        :param bool display: If True, display the diagnostic image
        
        Returns a 2-d array, the diagnostic layout image
        
        .. image:: wfs-show.png
           :scale: 50%
        """
        ap,du = self.ap, self.du
        
        a = u.Quantity(ap)
        a.name = 'ShaneAO Wavefront Sensor subapertures layout'
        a.dx = self.du
        a.x0 = [-x*a.dx/2 for x in list(a.shape)]
        
        dimg.show(a)
        ax = plt.gca()
        d,D,Ds = self.d.to('m').value, self.d_pri.to('m').value, self.d_sec.to('m').value
        n_subaps, subapLocs = self.n_subaps, self.subapLocs
        
        for k,x,y in zip(range(n_subaps),subapLocs[0],subapLocs[1]):
            x = x*d
            y = y*d
            ax.annotate(str(k),(x,y),
                        horizontalalignment='center',
                        verticalalignment='center')
            _ = ax.add_patch(patches.Rectangle((x-d/2,y-d/2),d,d,color='lightGrey',edgeColor='red'))
        _ = ax.add_patch(patches.Circle((0.,0.),D/2,fill=False))
        _ = ax.add_patch(patches.Circle((0.,0.),Ds/2,fill=False))
        plt.draw()
        return ax

    def gen_zmodes(self,nmodes=None,Q=0.,W=1.,force=False):
        """
        Generate the Zernike mode set, up to `nmodes` number of modes, excluding piston.
        The modes are generated on a fine grid and stored as an InfoArray data cube self.mode_set,
        and with self.mode_set_kind = 'Zernike'.
        
        Then, sense each of the modes using the :meth:`gsense` method and store the results in
        self.mode_set_slopes.
        
        Then, compute the least-squares modal reconstructor using :meth:`mode_fit`
        
        :param int nmodes: number of modes to compute
        :param numpy.array Q: regularizer matrix
        :param numpy.array W: weighting matrix
        :param bool force: force creation of the mode set, even if it already exists
        
        """
        if (nmodes is None):
            nmodes = self.n_zernike_modes
        else:
            assert nmodes > 1
            self.n_zernike_modes = nmodes
        if not force:
            if hasattr(self,'z_set'):
                if nmodes == len(self.z_set):
                    self.mode_set = self.z_set
                    self.mode_set_kind = 'Zernike'
                    return
        self.z_mode_index_set = np.array(zernike.i2nm(range(1,nmodes)))
        dx = self.du
        nfine = self.N
        rad = self.du*self.N/2.
        pr = self.d_pri/2.
        r,th = zernike.polar_array(Rmax=rad,DS=dx,pr=pr,c=[-0.5*dx,-0.5*dx])
        self.z_set = []
        for i in range(0,nmodes-1):
            n = int(self.z_mode_index_set[0,i])
            m = int(self.z_mode_index_set[1,i])
            mode = zernike.zernike(n,m,r,th,limit=False)[:-1,:-1]
            self.z_set.append(mode)            
        self.nmodes = len(self.z_set)
        self.z_set = InfoArray(self.z_set,name='Zernike mode set',
                               dx=dx,dx_units=self.du_units,z_label = 'Zernike mode number',
                               x0y0=np.array([1.,1.])*(-nfine/2)*dx)
        self.mode_set = self.z_set
        self.mode_set_kind = 'Zernike'
        self.mode_set_slopes = self.gsense(self.mode_set)
        self.mode_fit(Q=Q,W=W)

    def gen_fmodes(self,tiptilt=False,Q=0.,W=1.,force=False):
        """
        Generate the Fourier mode set.
        The modes are generated on a fine grid and stored as an InfoArray data cube within the object
        as self.mode_set and with self.mode_set_kind = 'Fourier'
        
        Then, sense each of the modes using the :meth:`gsense` method and store the results in
        self.mode_set_slopes
        
        Then, compute the least-squares modal reconstructor using :meth:`mode_fit`

        :param int nmodes: number of modes to compute
        :param numpy.array Q: regularizer matrix
        :param numpy.array W: weighting matrix
        :param bool force: force creation of the mode set, even if it already exists

        """
        if not force:
            if hasattr(self,'f_set'):
                self.mode_set = self.f_set
                self.mode_set_kind = 'Fourier'
                return
        n = self.N
        n_across = self.n_across
        uf = np.zeros((n,n))
        f_set_r = []
        f_set_i = []
        f_map = np.zeros((n,n))-10*n_across
        mode_num = 0
        # tip and tilt
        if tiptilt:
            x = np.arange(-n/2,n/2)/float(n/2)
            x,y = np.meshgrid(x,x)
            f_set_r.append(x)
            f_set_r.append(y)
        # positive ky axix
        kx = 0
        ky_set = np.arange(1,n_across/2)
        for ky in ky_set:
            uf[ky,kx] = 1.
            u = np.fft.fft2(uf)
            uf[ky,kx] = 0.
            f_set_r.append(np.real(u))
            f_set_i.append(np.imag(u))
            f_map[ky,kx] = mode_num; mode_num += 1
        # right half Fourier plane
        kx_set = np.arange(1,n_across/2)
        ky_set = np.arange(-n_across/2+1,n_across/2)
        for kx in kx_set:
            for ky in ky_set:
                if (ky < 0):
                    ky = ky + n
                uf[ky,kx] = 1.
                u = np.fft.fft2(uf)
                uf[ky,kx] = 0.
                f_map[ky,kx] = mode_num; mode_num += 1
                f_set_r.append(np.real(u))
                f_set_i.append(np.imag(u))
        self.f_set = f_set_r + f_set_i
        self.nmodes = len(self.f_set) # this should be n_across**2 - 2*n_across (+2 tip/tilt)
        self.f_set = InfoArray(self.f_set,name='Fourier mode set',
                               dx=self.du,dx_units=self.du_units,z_label = 'Fourier mode number',
                               x0y0=np.array([1.,1.])*(-n/2)*self.du)
        self.mode_set = self.f_set
        self.mode_set_kind = 'Fourier'
        self.mode_set_slopes = self.gsense(self.mode_set)
        self.mode_fit(Q=Q,W=W)

    def gsense(self,phi):
        """
        Simulate the Hartmann sensor by calculating the
        local slopes on phase sheet phi averaged
        on subapertures. The slopes are stored in a vector self.slope_vector
        of length number of subaps x 2: all of the x slopes
        followed by all of the y slopes.
        
        The input array, phi, must be the same shape and on the same
        sampling as the fine grid of the wavefront sensor (e.g. ``self.ap``)
        
        :param numpy.array phi: phase sheet
        
        """
        assert isinstance(phi,(np.ndarray,InfoArray))
        if phi.ndim == 2:
            nz = 1
            n,m = phi.shape
        elif phi.ndim == 3:
            nz,n,m = phi.shape
        else:
            raise TypeError('phi must be a 2d or 3d array')
            
        if isinstance(phi,InfoArray):
            # check that it is on the same sammpling
            assert phi.dx == self.du, 'phi samples must match the Wfs fine scale'
            assert (n,m) == self.ap.shape, 'phi size must macth the Wfs fine sample array size'
            self.phi = phi.copy()
        else:
            assert (n,m) == self.ap.shape, 'phi size must macth the Wfs fine sample array size'
            self.phi = InfoArray(phi,name='phase',longName='phase',
                              dx=self.ap.dx,dx_units=self.ap.dx_units,
                              units='unknnown')
        
        mask = np.ones_like(self.ap)
        mask[0,:] *= 0.
        mask[n-1,:] *= 0.
        mask[:,n-1] *= 0.
        mask[:,0] *= 0.
        
        if nz == 1:
            phi_set = [phi]
        else:
            phi_set = list(phi)
            
        slopes = []
        for phi in phi_set:
            gx = -((np.roll(phi,1,axis=1) - phi) + (phi - np.roll(phi,-1,axis=1)))/(2.*self.du)
            gy = -((np.roll(phi,1,axis=0) - phi) + (phi - np.roll(phi,-1,axis=0)))/(2.*self.du)
            gx *= mask
            gy *= mask
            ns = self.n_subaps
            s = np.empty((ns*2))
            j = 0
            
            for box in self.subap_ind:
                y0,y1,x0,x1 = box
                sa = self.ap[y0:y1,x0:x1]
                gx_sa = gx[y0:y1,x0:x1]
                gy_sa = gy[y0:y1,x0:x1]
                nf = float(np.sum(sa))
                s[j] = np.sum(sa*gx_sa)/nf
                s[j+ns] = np.sum(sa*gy_sa)/nf
                j += 1
            
            slopes.append(s)
        
        return slopes

    def map_slopes(self):
        """move slopes from the slopes vector to 2-d numpy array grids.
        These are stored as an object instance variable, ``self.slope_map``
        """
        n = self.n_across
        slope_map = []
        for s in self.slopes:
            sx = np.zeros((n,n))
            sy = np.zeros((n,n))
            x,y = map(lambda u: (u-0.5).astype(int)+n/2,self.subapLocs)
            for k,i,j in zip(range(self.n_subaps),y,x):
                sx[i,j] = s[k]
                sy[i,j] = s[k+self.n_subaps]
            
            units = 'd '+self.phi.units+' / d '+self.phi.dx_units
            slope_x = InfoArray(sx,name='slope x',longName='Phase Slopes X',
                                dx = self.du, dx_units = self.du_units,
                                units = units)
            slope_y = InfoArray(sy,name='slope y',longName='Phase Slopes Y',
                                dx = self.du, dx_units = self.du_units,
                                units = units)
            slope_map.append(np.block([slope_x,slope_y]))
        
        self.slope_map = slope_map
        self.slope_map_doc = '2d renditions of xy slopes'
    
    def mode_fit(self,slopes=None,mode_kind=None,Q=0.,W=1.):
        """Generate a least-squares fit of Wfs modes to the given slope vectors.
        
        .. math::
        
            c = (H^T W H+Q)^{-1} H^T W s
        
        where :math:`s` is the slope vector, :math:`c` is the fit coefficients,
        :math:`H` is the matrix that maps modes to slopes,
        :math:`Q` is the regularizer matrix (defaults to 0) and :math:`W` is the weighting matrix.
        
        The mode_kind defaults to the object's stored mode set kind, otherwise
        a mode set of the specified kind will be generated first.
        
        :param numpy.ndarray slopes: is a :math:`2 \\times \\text{nsubaps}` vector of slopes, :math:`x` slopes followed by :math:`y` slopes.
        :param str mode_kind: either 'Zernike' or 'Fourier' (more to come)
        :param numpy.ndarray Q: regularizer matrix, defaults to :math:`I_{\\text{nmodes}\\times \\text{nmodes}}`
        :param numpy.ndarray W: weights matrix, defaults to :math:`I_{(2\\times \\text {nsubaps})\, \\times \, (2\\times \\text {nsubaps})}`
        
        The slope array can be 2d, in which case the mode fit will be applied to each column.

        If `slopes` = 1, then just calculate the reconstructor matrix
        
        .. math::
        
            R = (H^T W H+Q)^{-1} H^T W 
        
        (which is the same as if :math:`s = I`)
        
        If `slopes` is not specified, then use the (presumed precalculated) slopes stored in the
        instance variable ``self.slopes``
        
        If `slopes` is specified, then return the vector of fit coefficients :math:`c`.
        The calculated vector(s) of fit coefficients is also stored in the
        instance variable ``self.fit_coefs``
        
        See :meth:`gen_zmodes` or :meth:`gen_fmodes`.
        
        """
        if mode_kind is None:
            mode_kind = self.mode_set_kind
        else:
            assert mode_kind in ['Zernike','Fourier']
            if mode_kind == 'Zernike':
                self.gen_zmodes()
            elif mode_kind == 'Fourier':
                self.gen_fmodes()
            self.gsense(self.mode_set)
                
        if slopes is None: # use internal slopes
            s = np.matrix(self.s).T
        
        if isinstance(slopes, np.ndarray) and slopes.ndim == 1: # make 1-d array a column vector
            s = np.matrix(slopes).T
            
        H = np.matrix(self.mode_set_slopes).T
        R = (H.T*W*H+Q).I*H.T*W
        
        if isinstance(slopes,(int,float)) and slopes == 1:
            c = R
        else:
            c = R*s
            self.fit_coefs = c
            self.fit_coefs_doc = 'most recent mode fits to slopes'
        
        self.H = H
        self.H_doc = 'forward matrix modes to slopes'
        self.R = R
        self.R_doc = 'modal fit reconstructor matrix: slopes to modes'
        self.HQWR = [H,Q,W,R]
        self.HQWR_doc = 'least squares reconstructor: H forward matrix, Q reqularizer, W weights, R reconstructor'
        return c

    def recon_prep(self,centroider='COG',nt=4096):
        """Prepare the reconstructor by setting up centroider weights
        and a data recording system
        
        :param str centroid: centroider, either 'COG', 'BIN' or 'QUAD'
        :param int nt: the maximum number of time steps allowed in data recording. The ShaneAO real-time system allows up to 4096
        """
        if centroider.upper() == 'COG':
            xvec = [-1.5,-0.5,0.5,1.5]
        elif centroider.upper() == 'BIN':
            xvec = [-1,-1,0.,0.]
        elif centroider.upper() == 'QUAD':
            xvec = [0.,-1,0.,0.]
        else:
            raise Exception('centroider %s not supported'%centroider)
        ivec = [1.,1.,1.,1.]
        self.wx = np.block([xvec+[0]]*4+[[0]*5])
        self.wx_doc = 'centroider x weights'
        self.wy = self.wx.transpose()
        self.wy_doc = 'centroider y weights'
        self.wi = np.block([ivec+[0]]*4+[[0]*5])
        self.wi_doc = 'centroider intensity weights'
        self.s = np.zeros(self.n_subaps*2)
        self.s_doc = 'wfs slopes vector'
        self.r = 5.
        self.r_doc = 'centroider regulizer (prevents div by zero)'
        self.ns = self.n_subaps*2
        self.ns_doc = 'total number of sensor values (2 x #subaps + tt)'
        nt = nt + 1 # always one additional 'bit bucket' bogus value
        self.centroider = centroider
        if self.mode == '16x':
            self.telem_size = (nt,1382)
        elif self.mode == '8x':
            self.telem_size = (nt,1174)
        self.telem = np.zeros(self.telem_size)
        self.telem_index = 0
        self.telem_increment = 0
        self.loop = 'open'
        self.loop_doc = 'AO loop state'
        self.ttloop = 'open'
        self.ttloop_doc = 'AO Tip/Tilt loop state'
        self.rate = 1000
        self.rate_units = 'Hz'
        self.rate_doc = 'WFS frame rate'
    
    def recon(self):
        """Duplicate the function of one step of the real-time reconstructor:
        
        #) compute the slopes given the CCD image
        #) multiply slopes by reconstructor matrix, giving delta-actuators
        #) integrate and limit the actuator commands
        #) perform telemetry data recording
        
        There is a presumption that the CCD has recorded photons, so recon
        must be preceded by a call to ccd_image or ccd_image_gpu.
        (Note: presently, only step 1 is implemented)
        """
        if not hasattr(self,'telem'):
            self.recon_prep()
        n_subaps, n_p, wx, wy, wi,r = self.n_subaps, self.n_p.value, self.wx, self.wy, self.wi, self.r
        q = np.empty((n_subaps,n_p,n_p)).astype(np.uint16)
        q.flat = self.ccd.flat[self.mapi_to]
        q = q.astype(float)
        inten = np.clip(np.sum(q*wi,(1,2)),0.,None)+r
        sx = np.sum(q*wx,(1,2))/inten
        sy = np.sum(q*wy,(1,2))/inten
        self.s.flat[:] = np.block([sx,sy])
        
        self.telem[self.telem_index,0:self.ns] = self.s[:]
        self.telem_index += self.telem_increment
        if self.telem_index >= self.telem_size[0]:
            self.telem_increment = 0
            self.telem_index = 0

    def simulate(self,bs,comp='CPU',nt=100,loop = 'open',record_images=True, reconstruct=True, progress_bar=True, reset=True):
        '''Simulate thw wavefront sensing for some number
        of steps, given a :class:`atmos.BlowingScreeen` object
        to act as the source of aberrations
        
        :param atmos.BlowingScreen bs: The Blowing Screen of atmospheric aberration
        :param str comp: which computer to use to do the wave optics calculation: 'GPU' or 'CPU'
        :param int count: number of time steps to simulate
        :param bool record_images: save wfs images
        :param bool reconstruct: reconstruct (:meth:`recon`) and save centroids & actuator commands in telemetry data buffer
        :param bool progress_bar: print a progress bar during calculation
        :param bool reset: reset the telemetry pointer to the beginning of the buffer

        Data is recorded during the simulation by default. Keep in mind that
        the telemetry data buffer is set to length of  4096 data point. If you want
        different than that, pre-initialize it::
        
            >>> w.recon_prep(nt = something_other_than_4096 )
            >>> w.simulate()
        
        '''
        if reconstruct:
            if not hasattr(self,'telem'):
                self.recon_prep()
            self.telem_increment = 1 # starts telemetry recording
            if reset:
                self.telem_index = 0

        if comp == 'GPU':
            self.ccd_image_gpu_prep()
        ttgpu = np.array([-1.,-1])*self.pixel_scale*u.pix/2.
        phtt = tilt(self.ap,ttgpu)
        self.loop = loop
        h = []
        t0 = time.time()
        krange = range(nt)
        if progress_bar:
            krange = tqdm.tqdm(krange)
        for k in krange:
            ph = bs.next()
            if comp == 'GPU':
                self.ccd_image_gpu((ph+phtt).astype(np.float32))
            else:
                self.ccd_image(phi=ph,add_noise=False)
            if record_images:
                h.append(self.ccd.copy())
            if reconstruct:
                self.recon()
        
            #print('')
        t = time.time() - t0
        if reconstruct:
            self.simulation = True
            self.simtime = t*u.s
            self.simtime.doc = 'simulation wall clock time'
            self.simsteps = u.Quantity(nt)
            self.simsteps.doc = 'number of steps in simulation'
            
        run_summary = {'BlowingScreen':bs,
                       'Wfs':self,
                       'rate':[1./bs.dt.value,'Hz'],
                       'time_step':[bs.dt.value,'sec'],
                       'n_steps':nt,
                       'ccd_images':h,
                       'simulation_wall_clock_time':'%r seconds'%t,
        }
        return run_summary
        
    def save_telemetry(self,filename='data000.fits',directory='.',overwrite=False):
        """Save the telemetry data in a FITS file. Telemetry data
        is a :math:`n_d \\times n_t` array (:py:obj:`InfoArray <info_array.InfoArray>`) where
        :math:`n_d` is the number of telemetry items stowed (wfs centroids, actoator commands, etc.)
        and :math:`n_t` is the number of time steps saved. The attributes of the
        data array contain information about the data, such as rate, wavelength, AO loop status, etc.
        
        See http://lao.ucolick.org/ShaneAO/software-docs/telemetry.html for a description of what data is
        stored in telemetry arrays.
        
        See :py:func:`makeFitsHeader`
        for a descirption of how the :py:obj:`InfoArray <info_array.InfoArray>` attributes map to FITS Header structure.
        
        :param str filename: the name of the FITS file to store into.
        :param str directory: the path to the directory
        :param bool clobber: if true, overwrite any file with the same name, otherwise
            add a sequence number to the name (as indiated by digits at the end of the argument filename) or throw an error
        
        """
        if not overwrite: # prevent a filename collision
            fn,ext = os.path.splitext(filename)
            ffname = os.path.expanduser(os.path.join(directory,filename))
            q = -1
            while fn[q].isdigit():
                q -= 1
            q += 1
            if q:
                ind = int(fn[q:])
                format = '%%0%dd'%abs(q)
                while os.path.exists(ffname):
                    ind += 1
                    filename = fn[:q]+format%ind + ext
                    ffname = os.path.expanduser(os.path.join(directory,filename))
        else:
            ffname = os.path.expanduser(os.path.join(directory,filename))
        self.telem_filename = filename
        #hdu = makeFitsHeader(self.telem)
        hdu = makeFitsHeader(self,attr='telem') # could put the gist of self into telem.__dict__ then makeFitsHeader(self.telem)
        hdu.writeto(ffname,overwrite=overwrite)
        return ffname

def pprint(obj):
    """
    Pretty-print the object's contents
    """
    oprint.pprint(obj,short=True)
    print(DotDict(sorted(obj.__dict__.items())))

def copyattrs(source,dest,x=[]):
    x += ['_unit']
    d = dest.__dict__
    for key,val in source.__dict__.items():
        if key in x:
            continue
        d[key] = val

def copyattrs_from(self,other,x=[]):
    copyattrs(other,self,x=x)
    return self

def radiansphase(lam):
    '''creates an equivalence for unit conversion (see astropy.units)'''
    if isinstance(lam,u.Quantity):
        lam = lam.to('m').value
    return [(u.radian, u.m,
            lambda x: x*lam/(2*np.pi),
            lambda x: x*2*np.pi/lam
            )]
        
def to_radphase(self,wavelength):
    q = self.to('rad',radiansphase(wavelength))
    q._wavelength = wavelength
    copyattrs(self,q)
    return(q)

def to_height(self,unit):
    q = self.to(unit,radiansphase(self._wavelength))
    copyattrs(self,q,x=['_wavelength'])
    return(q)
    
u.Quantity.pprint = pprint
u.Quantity.show = dimg.show
u.Quantity.to_radphase = to_radphase
u.Quantity.to_height = to_height
u.Quantity.copyattrs = copyattrs_from

def tilt(ap,theta):
    """
    Create an example <tilt> wavefront for testing the wavefront sensor
    
    :param Quantity ap: the aperture
    :param Quantity theta: the tilt of the wavefront, (x,y)

    returns the tilted wavefront, as a Quantity with units of um
    """
    if isinstance(theta,list):
        theta = u.Quantity(theta)
    (n,m) = ap.shape
    du = ap.dx
    
    x = (np.arange(n)-n/2)*du.to('um')
    x,y = np.meshgrid(x,x)
    theta_x,theta_y = theta.to_value('rad')
    phi = theta_x*x + theta_y*y # lift
    phi.aberration = 'tilt'
    phi.dx = ap.dx
    phi.x0 = ap.x0
    phi.tilt = theta
    phi.name = 'wavefront with %s tilt'%theta
    return phi
    
def focus(ap,sag,edge_slope=None):
    """
    Create an example <focus> wavefront for testing the wavefront sensor
    
    :param Quantity ap: the aperture
    :param Quantity sag: the sag, in length units
    :param Quantity edge_slope: specify the edge slope instead of sag

    returns the focus wavefront, as a Quantity with units of sag units
    """

    (n,m),du = ap.shape, ap.dx
    
    x = (np.arange(n)-n/2)*du
    x,y = np.meshgrid(x,x)
    r = np.sqrt(x**2+y**2)
    R = (r*ap).max()
    with u.set_enabled_equivalencies(u.dimensionless_angles()):
        if edge_slope is not None:
            sag = 0.5*edge_slope.to('rad').value*R.to('um')
        else:
            edge_slope = (2*sag/R).to('arcsec')
    
    phi = sag*(r/R)**2
    phi -= np.average(phi,weights=ap)
    phi.sag = sag
    phi.edge_slope = edge_slope
    phi.aberration = 'focus'
    phi.name = 'wavefront: focus with %s sag'%sag
    phi.dx = ap.dx
    phi.x0 = ap.x0
    return phi

def makeFitsHeader(obj,attr=None):
    """make a FITS header given an object
    
    :param object obj: any object. Attributes of the object will be saved put in the FITS header.
    :param str attr: the object attribute, of class :class:`numpy.ndarray` or :class:`InfoArray <info_array.InfoArray>`
        that hss the data to be saved.
        If None, assumes the object itself is a :class:`numpy.ndarray`
        or :class:`InfoArray <info_array.InfoArray>`
    
    The attributes in the object are connverted to FITS header cards with a
    attribute to key translation and support for documentation as follows:
    
    1.  If the attribute has a corresponding _doc or _units attribute, those
        strings are included in the comments section of the corresponding FITS card, rather
        than forming additional cards.
    
    2.  Header keys are formed from the attribute names, converted to upper case,
        striped of underscores '_' and truncated to 8 characters. If there are duplicate Header keys
        after the key translation, then the
        last two characters of the key are set to a sequence number. (Hopefully) this
        makes all the keys unique and all the attributes get into the header. The original attribute name
        is also put into the comment section of the FITS card.
    
        For example **obj.param_dir** translates to "PARAMDIR" and **obj.wavelength** translates to "WAVELENG"
    
    3.  Only scalars (:py:obj:`int`, :py:obj:`float`, :py:obj:`Quantity`), booleans (:py:obj:`bool`), and strings
        (:py:obj:`str`, :py:obj:`unicode`) are put into FITS Headers.
    
    The resulting FITS Header cards have the structure::
    
        ATTRNAME=      value / original_attribute_name, units, doc
    
    """
    if attr is None:
        data = obj
    else:
        data = getattr(obj,attr)
    try:
        d = obj.__dict__
    except:
        d = {}
    keys = sorted(d.keys())
    rkeys = {}
    row_list = []
    for key in keys:
        if (key.endswith('_doc') or key.endswith('_units')):
            continue
        fkey = key.replace('_','')[:8].upper()  # generate an upper case key of 8 chars
        if fkey in [x['fkey'] for x in row_list]: # make sure key is unique
            count = 0
            if fkey in rkeys:
                count = rkeys[fkey]
            rkeys[fkey] = count+1
            fkey = '%s%02d'%(fkey[:6],count+1) # if not unique, tag with a number
        val,comment = d[key],key
        if type(val) not in [int,float,str,bool]:
            if isinstance(val,u.Quantity) and val.isscalar:
                try:
                    v,more_comment = (val.value,'{0.unit:FITS}'.format(val))
                except:
                    v,more_comment = (val.value,'%s'%val.unit)
                comment += ', '+more_comment
                if hasattr(val,'doc'):
                    comment += ', '+val.doc
                val = v
            else:
                continue
        if type(val) is float and val == float('Infinity'):
            val = 'Infinity'
        #comment = key
        if key+'_units' in keys:
            comment += ', '+d[key+'_units']
        if key+'_doc' in keys:
            comment += ', '+d[key+'_doc']                
        row_list.append({'fkey':fkey,'val':val,'comment':comment})
    hdu = pyfits.PrimaryHDU(data)
    for r in row_list:
        hdu.header.append((r['fkey'],r['val'],r['comment']))
    return hdu

# additional units definitions
    
def new_unit(name,represents=None):
    ureg = u.get_current_unit_registry()._registry
    if name in ureg:
        return
    u.__dict__[name] = u.def_unit(name,represents=represents)
    u.add_enabled_units(u.__dict__[name])

new_unit('DN',represents=u.ct)
new_unit('e-',represents=u.electron)
new_unit('frame')

def parse(s):
    q = s.split(';',1)
    q = [x.strip() for x in q]
    try:
        val,com = q
    except:
        val,com = q[0],None
    if val.startswith('('):
        r = re.match('\(.*\)',val)
        if r is not None:
            v = r.group(),val[r.end():].strip()
    else:
        v = val.split(' ',1)
    try:
        val,unit = v
    except:
        val,unit = v[0],None
    try:
        val = eval(val)
    except:
        val = q[0].strip('\'\"')
        unit = None
    if unit is not None:
        unit = re.sub('e-','electron',unit)
        val = u.Quantity(val,unit,dtype=type(val))
    else:
        try:
            val = u.Quantity(val,dtype=type(val))
        except:
            pass
    
    return [val,com]

def typize(d):
    r = {}
    for key,val in d.items():
        val,doc = parse(val)
        if doc is not None:
            if isinstance(val,u.Quantity):
                val.doc = doc
            else:
                r[key+'_doc'] = doc
        r[key] = val
    return DotDict(r)

#====================================

def test(phi=None,comp='CPU',show=False):
    """
    Create and test a wavefront sensor object
    
    :param InfoArray phi: (optional) phase wavefront to image to the wfs CCD
    :param str comp: device to use for the test: 'CPU', 'GPU', or 'both'
    :param bool show: if True, displays the wfs subaps and a CCD image
    
        .. image:: ccd_test.png
           :scale: 50%
    """
    global w,test_result
    
    if comp == 'both':
        test_result = []
        for comp in ['CPU','GPU']:
            test(phi=phi,comp=comp,show=show)
            test_result.append(w.ccd.copy())
        assert np.isclose(test_result[0].astype(float),test_result[1].astype(float),rtol=1.e-4,atol=10).all()
        return    
    print('<test> testing with %s'%comp)
    
    w = Wfs()
    if show: w.show()
    if phi is None:
        phi = u.Quantity(np.zeros((256,256)),'radian')
        phi.name='phi'
    if comp == 'GPU':
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        phi += tilt(w.ap,ttgpu,w.wavelength)
        phi = phi.astype(np.float32)
        w.ccd_image_gpu(phi=phi)
    else:
        w.ccd_image(phi=phi,add_noise=False)
    w.ccd.longName = '%s (%s)'%(w.ccd.name,w.ccd.comp_by)
    if show:
        w.ccd.show()
    else:
        return

def test2(comp='CPU',show=False):
    """
    Create and test a wavefront sensor object. This routine feeds tilt and
    focus wavefronts and displays the CCD differences from a flat wavefront.
    
    :param str comp: device to use for the test: 'CPU', 'GPU', or 'both'
    :param bool show: whether or not to display graphics
    
        .. image:: ccd_tilt_test.png
           :scale: 50%
        .. image:: ccd_focus_test.png
           :scale: 50%
    """
    global w, test_result
    if comp == 'both':
        tr = []
        for comp in ['CPU','GPU']:
            test2(comp=comp,show=show)
            tr.append(InfoArray(test_result).copy())
        test_result = tr
        assert np.isclose(test_result[0].astype(float),test_result[1].astype(float),rtol=1.e-4,atol=10).all()
        return    
    print('<test2> testing with %s'%comp)
    
    w = Wfs()
    
    # flat wavefront
    phi = u.Quantity(np.zeros((256,256)),'nm')
    phi.name='phi'
    if comp == 'GPU':
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        phi += tilt(w.ap,ttgpu)
        phi = phi.astype(np.float32)
        w.ccd_image_gpu(phi=phi)
    else:
        w.ccd_image(phi,add_noise=False)
    u0 = w.ccd.copy()

    # Tilt
    atilt = w.pixel_scale*u.pix*u.Quantity([1,0])
    phi = tilt(w.ap, atilt)
    if comp == 'GPU':
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        phi += tilt(w.ap,ttgpu)
        phi = phi.astype(np.float32)
        w.ccd_image_gpu(phi=phi)
    else:
        w.ccd_image(phi,add_noise=False)
    u1 = w.ccd.copy()
    u1.longName = 'CCD image tilt minus flat: 1 pixel x-tilt'+'(%s)'%w.ccd.comp_by
    
    # Focus
    phi = focus(w.ap, 0., edge_slope=w.pixel_scale*u.pix)
    if comp == 'GPU':
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        phi += tilt(w.ap,ttgpu,w.wavelength)
        phi = phi.astype(np.float32)
        w.ccd_image_gpu(phi=phi)
    else:
        w.ccd_image(phi,add_noise=False)
    u2 = w.ccd.copy()
    u2.longName = 'CCD image focus minus flat: 1 pixel slope at edge'+' (%s)'%w.ccd.comp_by

    test_result = []
    for uu in [u1,u2]:
        d = uu.astype(float)-u0.astype(float)
        d.name = uu.longName
        test_result.append(d)
    if show:
        for uu in test_result:
            uu.show()
    return
    
def test3(tt=[0.,0.]*u.arcsec,show=False,niter=50,do_gpu = False):
    """
    Test the ccd_image operation with CPU and GPU. Measure and compare the execution times
    for image calculation using the GPU vs the CPU.
    
    :param [float,float] tt: the x,y amount of tilt to put on the test wavefront, in arcsec
    :param bool show: whether or not to display graphics

    """
    global w,test_result
    
    test_result = []
    
    # CPU
    w = Wfs()
    phi = tilt(w.ap, tt)
    # learn
    for k in range(niter):
        w.ccd_image(phi)
    # test
    t0 = time.time()
    for k in range(niter):
        w.ccd_image(phi,add_noise=False)
    dt = time.time() - t0
    f_cpu = niter/dt
    w.ccd.name = 'Python/CPU computed CCD image'
    rs = '<test3> CPU ccd min: %r, max: %r, %d iterations in = %r sec (%.3f Hz)'%(w.ccd.min(),w.ccd.max(),niter,dt,f_cpu)
    test_result.append((rs,w.ccd.copy()))
    if show:
        w.ccd.show()
    print(rs)
    
    # GPU
    if do_gpu:
        w.ccd_image_gpu_prep()
        ttgpu = np.array([-1.,-1])*w.pixel_scale*u.pix/2.
        phi = tilt(w.ap, ttgpu + tt,w.wavelength).astype(np.float32)
        for k in range(50):
            w.ccd_image_gpu(phi)
        t0 = time.time()
        w.ccd_image_gpu(phi)
        dt = (time.time() - t0)
        dt_gpu = dt
        w.ccd.name = 'OpenCL/GPU computed CCD image'
        w.ccd.units = 'DN'
        w.ccd = w.ccd.astype(float)
        rs = '<test3> GPU ccd min: %r, max: %r, time=%r sec (%.3f Hz)'%(w.ccd.min(),w.ccd.max(),dt,1./dt)
        test_result.append((rs,w.ccd.copy()))
        if show:
            w.ccd.show()
        print(rs)
        print('<test3> speedup: %1.1f X'%(dt_cpu/dt_gpu))

def testa(r0=10*u.cm, L0=u.Quantity(float('Infinity'),'m'), seed=None, comp='CPU', show=False):
    """Test using an atmospheric aberration
    
    :param float r0: r0 (atmospheric turbulence strength parameter), in meters
    :param float L0: outer scale, in meters
    :param str comp: device to use for the test: 'CPU', 'GPU', or 'both'
    :param bool show: whether or not to display graphics

    """
    global w,test_result
    from sauce2.atmos import Screen,seeing

    if comp == 'both':
        test_result = []
        for comp in ['CPU','GPU']:
            testa(r0=r0,L0=L0,comp=comp,seed=1,show=show)
            test_result.append(w.ccd.copy())
        assert np.isclose(test_result[0].astype(float),test_result[1].astype(float),rtol=1.e-4,atol=10).all()
        return    
    print('<testa> testing with %s'%comp)
    
    w = Wfs()
    seeing.r0 = r0
    seeing.L0 = L0
    if comp == 'GPU':
        w.ccd_image_gpu_prep()
    lam0 = 0.5
    a = Screen(seeing=seeing, n=w.N, du=w.du)
    if seed is not None:
        a.set_seed(seed)
    a.gen_screen()
    ph = a.screen
    if comp == 'GPU':
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        ph += tilt(w.ap,ttgpu,w.wavelength)
        ph = ph.astype(np.float32)
        w.ccd_image_gpu(ph)
    else:
        w.ccd_image(phi=ph,add_noise=False)
    if show:
        w.ccd.show()
    
def testb(r0=10*u.cm, L0=u.Quantity(float('Infinity'),'m'), dt=10.*u.ms, comp='CPU', count=100, pgbs=None, verbose=False, show=False):
    """Test using a blowing screen of atmospheric aberrations.

    :param Quantity r0: r0 (atmospheric turbulence strength parameter)
    :param Quantity L0: outer scale (not working yet)
    :param Quantity dt: time step of the simulation
    :param str comp: device to use for the test: 'CPU', 'GPU', or 'both'
    :param int count: number of frames to generate
    :param BlowingScreen pgbs: (optional) a pre-generated blowing screen. If provided, will ignore r0 and L0 parameters
    :param bool verbose: provide verbose output
    :param bool show: whether or not to display graphics

    The results are stored in wfs.test_result as a 2-tuple: (summary_string,ccd_image_stream),
    where:
    
        * **summary_string** summarizes the execution time and frequency
        * **ccd_image_stream** a list of ccd images
    
    In the case where comp='both', wfs.test_result is a list of two such 2-tuples.
    """
    global w, b, test_result, ani
    from sauce2.atmos import BlowingScreen,seeing

    w = Wfs()
    if pgbs == None:
        seeing.r0 = r0
        seeing.L0 = L0
        b = BlowingScreen(dt=dt,tmax=4.*u.s,name='blowing screen',n=w.N,du=w.du)
    else:
        b = pgbs

    if comp == 'both':
        tr = []
        for comp in ['CPU','GPU']:
            b.reset()
            testb(pgbs=b, comp=comp,count=count,verbose=verbose,show=show)
            tr.append(test_result)
        test_result = tr
        return    
    print('<testb> testing with %s'%comp)
    
    ttgpu = np.array([-1.,-1])*w.pixel_scale*u.pix/2.
    phtt = tilt(w.ap,ttgpu).to_radphase(w.wavelength)
    if comp == 'GPU':
        w.ccd_image_gpu_prep(phtt=phtt)
    rec_ph = []
    rec_ccd = []
    b.screen = b.screen.to_radphase(w.wavelength)
    if comp == 'GPU':
        b.screen = b.screen.astype(np.float32)
        b.ap = b.ap.astype(np.float32)
    b.options.rotate_method = 'indirect'
    b.options.depiston = False
    b.seeing.wind_magnitude = 10.*u.m/u.s
    b.seeing.wind_direction = 25.*u.deg
    b._make_rotator()
    t0 = time.time()
    ph = b.next(ap=False)
    b.reset()
    for k in tqdm.tqdm(range(count)):
        ph = b.next(ap=False)
        if (k==0): ph *= 0.
        if comp == 'GPU':
            w.ccd_image_gpu(ph)
        else:
            w.ccd_image(phi=ph,add_noise=False)
        rec_ph.append(ph.copy())
        rec_ccd.append(w.ccd.copy())
        if verbose:
            print('<testb> k = %d/%d'%(k,count))
            sys.stdout.flush()
    t = time.time() - t0
    rs = '<testb> execution time using %s: %d frames in %r seconds (%.3f Hz)'%(comp,count,t,count/t)
    test_result = (rs,[rec_ph,rec_ccd])
    print(rs)
    if show:
        plt.ion()
        fig = plt.figure(figsize=(8,4))
        ani = dimg.SubplotAnimation([rec_ph,rec_ccd],cmap='bone',stride=7,fig=fig)

def testr(comp='CPU'):
    """test the reconstructor
    """
    global w, test_result

    if comp == 'both':
        test_result = []
        for comp in ['CPU','GPU']:
            testr(comp=comp)
            test_result.append(w.s.copy())
        assert np.isclose(test_result[0].astype(float),test_result[1].astype(float),rtol=1.e-4,atol=4.e-3).all()
        return    
    print('<testr> testing with %s'%comp)

    w = Wfs()
    phi = focus(w.ap,0.,edge_slope=0.1*w.pixel_scale*u.pix)
    if comp == 'CPU':
        w.ccd_image(phi=phi,add_noise=False)
    elif comp == 'GPU':
        w.ccd_image_gpu_prep()        
        ttgpu = np.array([-1.,-1])*w.pixel_scale/2.
        phi += tilt(w.ap,ttgpu,w.wavelength)
        w.ccd_image_gpu(phi=phi.astype(np.float32))
    w.recon_prep()
    w.recon()

def testrm(n = 100):
    """test the reconstructor many loop iterations
    (GPU only)
    """
    global w
    w = Wfs()
    w.ccd_image_gpu_prep()
    phi = focus(w.ap,0.,edge_slope=w.pixel_scale*u.pix)
    ttgpu = np.array([-1.,-1])*w.pixel_scale*u.pix/2.
    phi += tilt(w.ap,ttgpu)
    w.recon_prep()
    w.telem_increment = 1
    print('<testrm> runing for %d iterations'%n)
    sys.stdout.flush()
    t0 = time.time()
    for k in range(n):
        w.ccd_image_gpu(phi=phi.astype(np.float32))
        w.recon()
    t1 = time.time()
    print('<testrm> done, w.telem_index = %d'%w.telem_index)
    dt = t1-t0
    print('<testrm> total: %d in %r sec, %0.2f ms/iteration, %0.2f Hz'%(n,dt,dt/n/ms,n/dt))

def testbr(count=100, r0=10.*u.cm, L0=u.Quantity(float('Infinity'),'m'), dt=10.*u.ms, save=True):
    '''Test blowing wind, reconstructor, and data telemetry.
    
    :param int count: the number of loop iterations
    :param float r0: the seeing parameter, in meters
    :param float L0: the outer scale, in meters (can be 'Infinity')
    :param float dt: the time step, in milliseconds
    :param bool save: if True, save retults to a telemetry data file.
    '''
    global w
    from sauce2.atmos import BlowingScreen,seeing
    w = Wfs()
    w.recon_prep()
    print('<testbr> generating blowing screen'); sys.stdout.flush()
    seeing.r0 = r0
    seeing.L0 = L0
    b = BlowingScreen(dt=dt,tmax=4.*u.s,name='blowing screen',
                      n=w.N,du=w.du)
    ttgpu = np.array([-1.,-1])*w.pixel_scale*u.pix/2.
    phtt = tilt(w.ap,ttgpu)
    print('<testbr> running the simulation'); sys.stdout.flush()
    w.simulate(b,nt=count)
    # w.telem_increment = 1
    # for k in range(count):
    #     ph = b.next()*nm_to_rad
    #     w.ccd_image_gpu((ph+phtt).astype(np.float32))
    #     w.recon()
    if save:
        print('<testbr> saving telemetry...'); sys.stdout.flush()
        bkeys = ['r0','L0','dt','v']
        w.r0 = b.seeing.r0
        w.L0 = b.seeing.L0
        w.dt = b.dt
        w.v = b.v
        w.save_telemetry(filename='temp_data_000.fits',overwrite=True)
        print('<testbr> ...saved to file %s'%w.telem_filename); sys.stdout.flush()
    
def testanal(filename = None):
    """analyze the data in a telemetry data file.
    
    :param str filename: the name of the telemetry file. If None, the default name is the one stored as ``telem_filename`` in the test object, otherwise 'temp_data_000.fits'
    """
    if filename is None:  # use the last file saved by testbr
        try:
            filename = w.telem_file
        except:
            filename = os.path.join(sauce_dir,'tests','temp_data_000.fits')
    print('<testanal> reading file %s'%filename)
    d = InfoArray.fromFits(filename)
    mode = d.header['MODE']
    ns = d.header['NSUBAPS']
    centroider, r0, v = 'unknown', 0., -1.
    if 'CENTROID' in d.header: centroider = d.header['CENTROID']
    if 'R0' in d.header: r0 = d.header['R0'] # seeing (simulation)
    if 'V' in d.header: v = d.header['V'] # wind velocity (simulation)
    telem_index = d.header['TELEMIND'] # points to the next empty slot
    s = d[1:telem_index-1,:(ns*2)]
    s_rms = s.std(axis=0).mean()  # rms along time axis, average rms over subaps
    print('rms tilt: %0.4g pixels, cent = %s, r0 = %0.2g m, v = %0.2g m/s'%(float(s_rms),centroider,r0,v))
