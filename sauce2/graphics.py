'''Assist in loading Matplotlib in
multiple enviromments. Find the appropriate
backend before trying to load it
(and possibly ruining the python session)
including checking for the non-Framework
situation of a virtual environment.

To use:
from graphics import plt
'''

import os
import sys
import time
import platform

machines_without_graphics = ['real.ucolick.org']
machine = platform.node()
preferred_backends = {'-mac-':{'std':'TkAgg','virtual':'TkAgg'}}
operating_system = platform.platform()

# Notes on virtual environment on a mac
# python not installed as a Framework on a mac:
#   A work-around is to use the system python /opt/local/bin/python after
#   setting the environment variable PYTHONHOME=$HOME/[virtual dir].
#   see: https://blog.rousek.name/2015/11/29/adventure-with-matplotlib-virtualenv-and-macosx/
# To get the TkAgg backend and Tkinter:
#   The python in virtual environment is not configured for Tkinter
#   link to the system tkinter library:
#   cd [virtual dir]/lib/python2.7/site-package
#   ln -s /opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/_tkinter.so .
#   see: http://lab.openpolis.it/using-tkinter-inside-a-virtualenv.html
#   

if (machine not in machines_without_graphics):
    for key,agg in preferred_backends.iteritems():
        if key in operating_system.lower()+' '+machine.lower():
            break
        else:
            agg = None
        
    import matplotlib
    if hasattr(sys, 'real_prefix'): # it's a virtual environment
        if agg is not None:
            matplotlib.use(agg['virtual'])
        else:
            if 'Frameworks' in sys.executable:  # okay, we're using the framework python
                pass
            elif matplotlib.get_backend() != 'MacOSX': # it's okay, we're using another backend
                pass
            else:  # in the virtual environment, running non framework python, and MacOSX backend, so...
                try:
                    import Tkinter # try using TkAgg, which will work if we have Tkinter (_tkinter.so in [virtual dir]/lib/python2.7/site-packages)
                    matplotlib.use('TkAgg')
                    print('<dimg> virtual environment, python not installed as a Framework')
                except:
                    matplotlib.use('pdf')
                    print('<dimg> virtual environment, can"t find a graphic backend.')
    else: # not a virtual environment
        if agg is not None:
            matplotlib.use(agg['std'])
        
    print('<graphics> using %s graphic backend'%matplotlib.get_backend())
    import matplotlib.pyplot as plt
    plt.ion()

else:
    print('<graphics> WARNING graphics not supported on %s with python %s'%(platform.node(),platform.python_version()))
    plt = None
    