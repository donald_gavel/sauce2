"""
info_array - define class and methods for the 'Information'-packed
ndarray. it inherits all the metods of the numpy array class
and adds the ability to set other attributes, pretty-print, and smart graphing.
"""
import sys
import os
if 'sphinx' in sys.modules:
    import dummy_np as np
else:
    import numpy as np
import img
import pyfits
from fits import protectFile
import time
from common_units import units, ufloat, as_ufloat
ufloat_imported = True
from oprint import pprint
import warnings
from termcolor import colored

def epl():
    '''just-in-time loader for interactive plotting
    '''
    global plt,dimg
    import dimg
    plt = dimg.plt

def copyattrs(sobj,dobj):
    '''copy all attributes from one object to another
    
    :param object sobj: source object
    :param object dobj: destination object
    '''
    d = sobj.__dict__
    keys = d.keys()
    for key in keys:
        setattr(dobj,key,d[key])

class InfoArray(np.ndarray):
    """
    A sub class of `numpy.ndarray <http://docs.scipy.org/doc/numpy/reference/arrays.ndarray.html>`_, with useful additional attributes like **name**, **dx**, and **units.**
    
    (See `numpy array subclassing <http://docs.scipy.org/doc/numpy-1.10.1/user/basics.subclassing.html>`_)
    
    to use::
    
        a = InfoArray(np.zeros((2,2)),name='blah')
        a.dx = ...
    
    InfoArrays are containers in that they can contain any number of attributes. Useful ones are:
    
    :ivar str name: The name of the array
    :ivar str units: The units of the values in the array
    :ivar float dx: The physical size of one pixel in the array
    :ivar str dx_units: The units of the pixel size
    :ivar list axis_names: A list of strings containing the axis names
        The first and second are the horizontal and vertical axes correspondingly.
        An optionall third is the radial (for radial average plots).
    :ivar list x0y0: Physical position of the [0,0] pixel (defaults to -shape*dx/2)
    
    """
    
    special_keys = ['name','units','dx','dx_units','dy','dy_units','x0y0','axis_names']

    def __new__(cls, input_array=[], name=None,**kwargs):
        obj = np.asarray(input_array).view(cls)
        if isinstance(input_array,InfoArray):
            copyattrs(input_array,obj)
        else:
            if hasattr(input_array,'_unit'):
                obj._unit = input_array._unit
                obj.units = '%r'%input_array._unit
            else:
                obj.units = ''
            obj.dx = 1.0
            obj.dx_units = ''
        if name is not None:
            obj.name = name
        obj.__dict__.update(kwargs)
        if hasattr(obj,'units') and isinstance(obj.units,ufloat):
            if obj.units.kind == 'phase':
                obj.wavelength = obj.units.wavelength
            obj.units = obj.units.unit
        if hasattr(obj,'wavelength') and obj.wavelength is not None:
            assert obj.wavelength.kind == 'length','<InfoArray.__new__> wavelength must be a length, not %s'%obj.wavelength.kind
        if not hasattr(obj,'name'):
            obj.name = ''
        if isinstance(input_array,list) and len(input_array)>0:
            if isinstance(input_array[0],InfoArray):
                copyattrs(input_array[0],obj)
                if name is not None: obj.name = name
                obj.__dict__.update(kwargs)
        return obj
    
    @classmethod
    def fromFits(cls,filename):
        """Create an InfoArray and read in data from a FITS file
        
        :param str filename: name of the FITS file to read from
        """
        return cls()._readfits(filename)
        
    def __array_finalize__(self, obj):
        if obj is None:
            return
        if isinstance(obj,InfoArray):
            copyattrs(obj,self)
        
    def copy(self):
        """Return a deep copy of the InfoArray
        
        :rtype: InfoArray
        """
        obj = InfoArray(self.view(np.ndarray).copy())
        copyattrs(self,obj)
        return obj
    
    def __repr__(self):
        r = self._pprint() + '\n'
        r += '\n'
        r += super(self.__class__,self).__repr__()
        r += '\n'
        return r
    
    def __getattr__(self,name):
        """implemented for the 'mag' and 'phase' attributes
        """
        if name in ('mag','abs'):
            return np.abs(self)
        elif name in ('phase','angle'):
            return np.angle(self)
        elif name in ('hdr'):
            self.pprint()
        else:
            raise AttributeError, '%s is not an attribute of %s'%(name,str(type(self)))
    
    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        # print '<InfoArray.__array_ufunc__>'
        # print 'ufunc = %r'%ufunc
        # print 'method = %r'%method
        # print '~'*80
        # print 'kwargs = %r'%kwargs['out']
        # print '~'*80
        nargs = len(inputs)
        if not hasattr(self,'units'):
            self.units = ''
            
        units = self.units
        other_units = None
        if nargs == 2:
            if not isinstance(inputs[0],InfoArray):
                other = inputs[0]
            else:
                other = inputs[1]
            if isinstance(other,InfoArray):
                other_units = other.units
        else:
            pass
        
        # print '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
        # print '-------- SELF ---------'
        # print self
        # if isinstance(self,InfoArray): self.pprint()
        # print '-------- OTHER ---------'
        # print other
        # if isinstance(other,InfoArray): other.pprint()
        # print '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
                
        if nargs == 2:
            wavelength = self.wavelength if hasattr(self,'wavelength') else None
            if isinstance(other,(InfoArray,ufloat)):
                other_wavelength = other.wavelength if hasattr(other,'wavelength') else None
            else:
                other_wavelength = None
            if ufunc in [np.add, np.subtract, np.divide]:
                if isinstance(other,InfoArray):
                    other = other.to_units(self.units,wavelength=wavelength,strict=True)
                elif isinstance(other,ufloat):
                    other = other.to_units(self.units,wavelength=wavelength)
                inputs = (inputs[0],other)
            elif ufunc in [np.multiply]:
                if isinstance(other,InfoArray):
                    if not (self.units == '' or other.units == ''):
                        raise NotImplementedError,"<InfoArray.__array_ufunc__> '%r' %r '%r'"%(self.units,ufunc,other.units)
                    else:
                        if self.units == '':
                            self.units = other.units
                elif isinstance(other,ufloat):
                    if not (self.units == '' or other.unit == ''):
                        raise NotImplementedError,"<InfoArray.__array_ufunc__> '%r' %r '%r'"%(self.units,ufunc,other.unit)
                    else:
                        if self.units == '':
                            self.units = other.unit
            
        args = []
        for arg in inputs:
            if isinstance(arg,InfoArray):
                args.append(arg.view(np.ndarray))
            else:
                args.append(arg)
        
        outputs = kwargs.pop('out', None)
        out_no = []
        if outputs:
            out_args = []
            for j, output in enumerate(outputs):
                if isinstance(output, InfoArray):
                    out_no.append(j)
                    out_args.append(output.view(np.ndarray))
                else:
                    out_args.append(output)
            kwargs['out'] = tuple(out_args)
        else:
            outputs = (None,) * ufunc.nout

        results = super(InfoArray, self).__array_ufunc__(ufunc, method, *args, **kwargs)

        if results is NotImplemented:
            return NotImplemented

        if results.dtype == self.dtype and isinstance(results,np.ndarray):
            results = results.view(InfoArray)
            try:
                copyattrs(self,results)
            except:
                print colored('copyattrs warning','red',attrs=['bold'])
            if ufunc == np.divide:
                results.units = ''
                if hasattr(results,'wavelength'):
                    results.wavelength = None
            
            results.info = {'ufunc':ufunc,'nargs':nargs,'self.units':self.units,'other.units':other_units}
        
        return results
    
    def bogus__array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        # see https://docs.scipy.org/doc/numpy/user/basics.subclassing.html
        #print '<InfoArray.__array_ufunc__>'
        #print 'ufunc = %r'%ufunc
        #print 'method = %r'%method
        args = []
        in_no = []
        other_units = None
        other_type = None
        for i, input_ in enumerate(inputs):
            other_type = type(input_)
            other_units = None
            if isinstance(input_, InfoArray):
                in_no.append(i)
                if input_.units != '':
                    other_units = input_.units
                args.append(input_.view(np.ndarray))
            elif isinstance(input_, ufloat):
                other_units = input_.unit
                args.append(input_)
            else:
                args.append(input_)

        outputs = kwargs.pop('out', None)
        out_no = []
        if outputs:
            out_args = []
            for j, output in enumerate(outputs):
                if isinstance(output, InfoArray):
                    out_no.append(j)
                    out_args.append(output.view(np.ndarray))
                else:
                    out_args.append(output)
            kwargs['out'] = tuple(out_args)
        else:
            outputs = (None,) * ufunc.nout

        info = {}
        if in_no:
            info['inputs'] = in_no
        if out_no:
            info['outputs'] = out_no

        results = super(InfoArray, self).__array_ufunc__(ufunc, method,
                                                 *args, **kwargs)
        if results is NotImplemented:
            return NotImplemented

        if method == 'at':
            if isinstance(inputs[0], InfoArray):
                inputs[0].info = info
            return

        if ufunc.nout == 1:
            results = (results,)

        results = tuple((np.asarray(result).view(InfoArray)
                         if output is None else output)
                        for result, output in zip(results, outputs))
        if results and isinstance(results[0], InfoArray):
            info['ufunc'] = ufunc
            info['method'] = method
            info['self_units'] = self.units
            info['other_type'] = other_type
            info['other_units'] = other_units
            copyattrs(self,results[0])
            results[0].info = info
            if other_units:
                if ufunc == np.multiply:
                    if results[0].units == '':
                        results[0].units = other_units
                    else:
                        raise NotImplementedError,'<InfoArray.__array_ufunc__> units x units'
                elif ufunc == np.divide:
                    if results[0].units == other_units:
                        results[0].units = ''
                    else:
                        raise NotImplementedError,'<InfoArray.__array_ufunc__> units must match in divide'
                elif ufunc in [np.add,np.subtract]:
                    if results[0].units == other_units:
                        pass
                    else:
                        raise NotImplementedError,'<InfoArray.__array_ufunc__> units must match in add and subtract'
                else:
                    if results[0].units != '':
                        raise NotImplementedError,'<InfoArray.__array_ufunc__> operation %r cannot be performed on units'%ufunc
            else:
                if results[0].units != '':
                    raise NotImplementedError,'<InfoArray.__array_ufunc__> %r %r %r'%(results[0].units,ufunc,other_units)

        return results[0] if len(results) == 1 else results
    
    def to_magphase(self,inplace=False):
        """convert from a real-imag representation to a magnitude-phase
        representation.
        
        :param bool inplace: If True, change values inplace and return nothing. If False, return a new array.
        """
        if self.dtype == float: # need to add a complex part (can't do this in place)
            r = InfoArray(self + 1j*0)
            r.is_magphase = True
            return r
        else:
            if not hasattr(self,'is_magphase'): self.is_magphase = False
            assert not self.is_magphase
            mag = np.sqrt(self.real**2 + self.imag**2)
            phase = np.arctan2(self.imag,self.real)
            if inplace:
                self[:] = mag + 1j*phase
                self.is_magphase = True
                return
            else:
                r = InfoArray(mag+1j*phase)
                r.is_magphase = True
                return r
    
    def to_realimag(self,inplace=False):
        """convert from a magnitude-phase representation to a real-imag representation
        
        :param bool inplace: If True, change values inplace and return nothing. If False, return a new array.
        """
        assert self.is_magphase and self.dtype == complex
        re = self.real*np.cos(self.imag)
        im = self.real*np.sin(self.imag)
        if inplace:
            self[:] = re + 1j*im
            self.is_magphase = False
            return
        else:
            r = InfoArray(re + 1j*im)
            r.is_magphase = False
            return r
    
    def to_units_old(self,new_unit,wavelength=None,inplace=False):
        """scale the array to be in new units
        
        :param str new_unit: new units
        :param (float,str) lam: if the new units are 'radians', a ufloat wavelength (See :class:`ufloat` below)        
        :param bool inplace: If True, change values inplace and return nothing. If False, return a new array.
        """
        if inplace:
            r = self
        else:
            r = self.copy()
        if self.units == 'radians':
            fac = self.units_wavelength.to_units('meters')/(2.*np.pi)
            if hasattr(self,'units_wavelength'):
                del(r.units_wavelength)
        else:
            fac = units[self.units]
        if new_unit == 'radians':
            assert wavelength is not None, '<InfoArray.to_units> wavelength (lam) must be specified for radians units'
            fac /= wavelength.to_units('meters')/(2.*np.pi)
            r.units_wavelength = wavelength
        else:
            fac /= units[new_unit]
        r.units = new_unit
        if inplace:
            self[:] *= fac
            return
        else:
            r = r*fac
            return r
        
    def to_units(self, new_unit,wavelength=None,inplace=False,strict=False):
        """scale the array to be in new units
        
        :param str new_unit: new units
        :param ufloat wavelength: if the new units are 'radians' or 'waves', a ufloat wavelength (See :class:`ufloat` below)        
        :param bool inplace: If True, change values inplace and return nothing. If False, return a new array.

        If the original array does not have units (self.unit=''),
        then this simply 'unitizes' the array, without scaling the data,
        unless strict = True.
        """
        if inplace:
            r = self
        else:
            r = self.copy()
        if not strict:
            if self.units == '':
                print colored('<InfoArray.to_units> WARNING using to_units to set units of unitless InfoArrays is deprecated. Use A.units=... instead.','red',attrs=['bold'])
                r.units = new_unit
                if units.u[new_unit].kind == 'phase':
                    assert wavelength.kind == 'length','<InfoArray.to_units> wavelength must be a length, not %s'%wavelength.kind
                    r.wavelength = wavelength
                return r
        if self.units not in units.u:
            _ = ufloat(1.,self.units)
        if new_unit not in units.u:
            _ = ufloat(1.,new_unit)
        a = units.u[self.units]
        if a.kind == 'phase':
            a = a.at(self.wavelength)
        if isinstance(new_unit,ufloat):
            if new_unit.kind == 'phase':
                wavelength = new_unit.wavelength
            new_unit = new_unit.unit
        if units.u[new_unit].kind == 'phase':
            assert isinstance(wavelength,ufloat) and wavelength.kind == 'length','<InfoArray.to_units> wavelength must be a ufloat of kind length'
            b = a.to_units(new_unit,wavelength=wavelength)
        else:
            b = a.to_units(new_unit)
        cf = float(b)/float(a)
        try:
            r[:] *= cf
        except:
            raise TypeError,'cannot convert units of a %r array'%self.dtype
        r.units = new_unit
        if wavelength:
            r.wavelength = wavelength
        else:
            r.wavelength = None
        return r
    
    def minmax(self):
        """Calculate the miimum and maximum of the array
        
        :return: a list of [minimumm, maximum]

        """
        return tuple(np.array([self.min(),self.max()]))

    def calc_stats(self,ap=None):
        '''calculate the peak to valley and rms values
        of the screen and put them in the screen attributes
        '''
        if ap is None:
            ap = np.ones(self.shape)
        wavelength = self.wavelength if hasattr(self,'wavelength') else None
        self.stats_peak = ufloat((self*ap).max(),self.units,wavelength)
        self.stats_valley = ufloat((self*ap).min(),self.units,wavelength)
        self.stats_peakToValley = self.stats_peak - self.stats_valley
        self.stats_rms = ufloat(img.rms(np.array(self),ap),self.units,wavelength)
        
        return self

    def pprint(self,full=False):
        """Pretty-print the object. This consists
        of reporting on the object's attributes.

        :param boolean full: if True, **pprint** also prints out the contents of the array.
            Default is to print only the "header" (attributes) information.

        """
        print self._pprint(full=full)
        
    def _pprint(self,print_width=40,aslist=False,rshort=False,full=False):
        cr = '\n'
        bling = 50*'='
        rs = []
        rs.append(bling)
        if hasattr(self,'longName'):
            rs.append(self.longName)
        else:
            if hasattr(self,'name'):
                if isinstance(self.name,str):
                    if len(self.name) > 0:
                        rs.append(self.name)
        rs.append('%s dtype = %s'%(str(type(self)),str(self.dtype)))
        #rs.append(str(type(self)))
        rs.append(bling)
        rs.append('shape: '+str(self.shape))
        if rshort:
            if aslist:
                return rs
            else:
                rs = '\n'.join(rs)
                return rs
        if full:
            rs.append(str(self))
        tab = ' '*4
        d = self.__dict__
        keys = sorted(d.keys())
        for key in keys:
            if key.endswith('_units'): continue
            val = d[key]
            units = None
            if isinstance(val,(str,unicode)): pv = "'"+val+"'"
            else: pv = val.__repr__()
            #else: pv = str(val)
            if len(pv) > print_width:
                pv = str(val.__class__)
                if isinstance(val,(np.ndarray,np.matrix)):
                    pv += ' '+str(val.shape)
            if key+'_units' in keys:
                units = d[key+'_units']
            line = key+':'+tab+pv
            if units is None:
                pass
            else:
                line += ' '+units
            if isinstance(val,InfoArray):
                if (hasattr(val,'longName')):
                    line += " '"+val.longName+"'"
                else:
                    line += " '"+val.name+"'"
            else:
                pass
            rs.append(line)
        rs.append(bling)
        if aslist:
            return rs
        else:
            rs = '\n'.join(rs)
            return rs
    
    def makeFitsHeader(self):
        '''as much as possible, take the attributes of the InfoArray and
        make a FITS header from them. The result is attached as attribute
        :attr:`hdu`. This will trigger :meth:`writefits` to write that header to
        the FITS file
        '''
        self.hdu = makeFitsHeader(self)
        
    def writefits(self,filename,clobber=False,protect=False,make_header = True):
        """write the InfoArray to a fits file
        
        :param str filename: name of file to write to
        :param bool clobber: If True, will write over an existing file
        :param bool protect: If True, will first save the existing file to a subdirectory, with timestamp appended to its file name
        :param bool make_header: If True, make a FITS header from the objects attributes. If False, make the header only from the :attr:`special_keys` group of attributes
        """
        if make_header:
            self.makeFitsHeader()
            hdu = self.hdu
        else:
            hdu = pyfits.PrimaryHDU(self)
        if hasattr(self,'header'):
            hdr = self.header
            hdu.header = hdr
        else:
            hdr = hdu.header
        for key in self.__dict__:
            if key in InfoArray.special_keys:
                if key == 'x0y0':
                    hdr['X0'] = (self.x0y0[0],'x axis origin')
                    hdr['Y0'] = (self.x0y0[1],'y axis origin')
                elif key == 'axis_names':
                    hdr['NAMEX'] = (self.axis_names[0],'x axis name')
                    hdr['NAMEY'] = (self.axis_names[1],'y axis name')
                else:
                    hdr[key.upper()] = (self.__dict__[key],key)
        tl = time.localtime(time.time())
        day = time.strftime('%Y-%m-%d',tl)
        tim = time.strftime('%H:%M:%S',tl)
        fnam = os.path.basename(filename)
        hdr['FILE'] = (fnam,'file name')
        hdr['DATE'] = (day,'File generation date')
        hdr['TIME'] = (tim,'file generation time hh:mm:ss')
        hdr['GENER'] = ('info_array.py','writer code')
        if os.path.isfile(filename):
            if clobber:
                pass
            elif protect:
                protectFile(filename)
            elif not protect and not clobber:
                raise IOError,'<info_array.writefits> file already exits: '+filename
        hdu.writeto(filename,clobber=True)
    
    def _readfits(self,filename):
        """read the data from a fits file.
        Do not call this directly, instead use the InfoArray.fromFits constructor
        """
        if not os.path.isfile(filename):
            raise IOError('<info_array.readfits> no file named '+filename)
        hdu = pyfits.open(filename)
        data = hdu[0].data
        hdr = hdu[0].header
        hdu.close()
        a = InfoArray(data)
        a.header = hdr
        special_keys = InfoArray.special_keys
        #special_keys = ['name','units','dx','dx_units','dy','dy_units','x0y0','axis_names']
        special_keys += ['file','date','time','gener']
        for key in special_keys:
            if key in hdr:
                a.__dict__[key] = hdr[key]
        if 'X0' in hdr and 'Y0' in hdr:
            a.x0y0 = (hdr['X0'],hdr['Y0'])
        if 'NAMEX' in hdr and 'NAMEY' in hdr:
            a.axis_names = (hdr['NAMEX'],hdr['NAMEY'])
        return a
        
    def plot(self,kind='image',fontsize=14,grid=True,oplot=False,scale='lin',fuzz=1e-9,**kwargs):
        """Display the InfoArray data as an image or lineout plot
        
        :param str kind: plot kind: 'image','lineout',or 'radial'
        :param int fontsize: for the titles, axis labels, and tick labels
        :param boolean grid: draw axis grid
        :param boolean oplot: whether or not to plot over a prevous graph (only for line plots)
        :param str scale: plot scale: 'lin','sqrt','log','loglog'
        :param float fuzz: a minimum value for log scale to keep the image contrast reasonable (used only for 'image', scale='log')
        :param \*\*kwargs: is sent on to dimg.show in the kind='image' case
        """
        epl()
        scales = ['lin','sqrt','log','loglog']
        assert scale in scales,'scale must be one of '+scales
        if self.ndim == 3:
            nf,n,m = self.shape
        else:
            nf = 1
            n,m = self.shape
        if isinstance(self,np.matrix):
            valid_kinds = ['image']
            necessary_keys = set(['name'])
        else:
            valid_kinds = ['image','lineout','radial']
            necessary_keys = set(['units','dx','dx_units','name'])
        assert kind in valid_kinds,'kind must be one of '+valid_kinds
        keys = self.__dict__.keys()
        assert necessary_keys.issubset(set(keys)),'Object must have attributes '+str(list(necessary_keys))
        if hasattr(self,'longName'):
            name = self.longName
        else:
            name = self.name
        if isinstance(self,np.matrix):
            x0,y0 = 0,0
            if (hasattr(self,'axis_names')):
                xlabel = self.axis_names[1]
                ylabel = self.axis_names[0]
            else:
                xlabel = 'column index'
                ylabel = 'row index'
            origin = 'upper'
            xlim = [x0,x0+n]
            ylim = [y0+n,y0]
        else:
            dx = self.dx
            dx_units = self.dx_units
            if hasattr(self,'x0y0'):
                x0,y0 = self.x0y0
            else:
                x0,y0 = -(float(m)/2)*dx-dx/2, -(float(n)/2)*dx-dx/2
            origin = 'lower'
            xlim = [x0,x0+m*dx]
            ylim = [y0,y0+n*dx]
            if 'dy_units' in keys:
                dy_units = self.dy_units
            else:
                dy_units = self.dx_units
            if 'axis_names' in keys:
                xlabel = self.axis_names[0]
                if dx_units != '':
                    xlabel += ', '+dx_units
                ylabel = self.axis_names[1]
                if dy_units != '':
                    ylabel += ', '+dy_units
                if len(self.axis_names)>2:
                    rlabel = self.axis_names[2]+', '+dx_units
                else:
                    rlabel = dx_units
            else:
                xlabel = dx_units
                ylabel = dy_units
                rlabel = dx_units
        
        data = np.array(self)
        if data.dtype == complex:
            print '<Object.plot> data is COMPLEX - showing the absolute value'
            data = np.abs(data)
            name = name+' (ABSOLUTE VALUE)'
        
        if not oplot and kind is not 'image':
            plt.figure()
        
        if self.units is not None and self.units != '':
            name += ', '+self.units
        
        if kind == 'radial':
            name += ' radial average'
            x = np.arange(xlim[0],xlim[1],dx)
            y = np.arange(ylim[0],ylim[1],dx)
            x,y = np.meshgrid(x,y)
            r = np.sqrt(x**2+y**2)
            bins = np.arange(0,(n/2)*dx,dx)
            i = np.digitize(r.flatten(),bins)
            c = np.bincount(i)
            q = np.zeros(n/2+1)
            for j,y in zip(i,data.flatten()):
                q[j] += y
            q /= c
            q = q[:-1]
            plt.plot(bins,q)
            if not oplot:
                ax = plt.gca()
                ax.set_xlabel(rlabel,fontsize=fontsize)
                ax.set_ylabel(self.units,fontsize=fontsize)
                ax.tick_params(axis='both',which='major',labelsize=fontsize,direction='out',length=6,width=1,
                               labeltop=False,labelbottom=True,labelleft=True,labelright=False)
                plt.title(name,y=1.04)
            if scale == 'log':
                ax.set_yscale('log')
            if scale == 'loglog':
                ax.set_xscale('log')
                ax.set_yscale('log')
                
        if kind == 'lineout':
            y = data[n/2,:]
            x = np.arange(xlim[0],xlim[1],dx)
            plt.plot(x,y)
            if not oplot:
                ax = plt.gca()
                ax.set_xlabel(xlabel,fontsize=fontsize)
                ax.set_ylabel(self.units,fontsize=fontsize)
                ax.tick_params(axis='both',which='major',labelsize=fontsize,direction='out',length=6,width=1,
                               labeltop=False,labelbottom=True,labelleft=True,labelright=False)
                plt.title(name,y=1.04)
            if scale == 'log':
                ax.set_yscale('log')

        if kind == 'image':
            if scale == 'log':
                data = np.log10(np.clip(np.abs(data),fuzz,np.inf))
            if scale == 'sqrt':
                data = np.sqrt(np.abs(data))
            if nf == 1:
                dimg.show(data,extent = xlim+ylim,origin=origin,title=name,**kwargs)
                ax = plt.gca()
                if not oplot:
                    ax.set_xlabel(xlabel,fontsize=fontsize)
                    ax.set_ylabel(ylabel,fontsize=fontsize)
                    ax.tick_params(axis='both',which='major',labelsize=fontsize,direction='out',length=6,width=1,
                                   labeltop=True,labelbottom=True,labelleft=True,labelright=True)
                    plt.title(name,y=1.08)
            else:
                if not hasattr(self,'z_label'):
                    self.z_label = 'time'
                dimg.play(data,**kwargs)

        if not oplot:
            ax = plt.gca()
            ax.tick_params(axis='both',which='minor',direction='out',length=4,width=1)
            plt.minorticks_on()
        
        plt.grid(grid)
        plt.draw()
        
    def graph(self,**kwargs):
        kwargs['kind'] = 'lineout'
        self.plot(**kwargs)
        
    def display(self,**kwargs):
        kwargs['kind'] = 'image'
        self.plot(**kwargs)
        
    def show(self,**kwargs):
        """Display the InfoArray data as an image or lineout plot
        
        :param str kind: plot kind: 'image','lineout',or 'radial'
        :param int fontsize: for the titles, axis labels, and tick labels
        :param boolean grid: draw axis grid
        :param boolean oplot: whether or not to plot over a prevous graph (only for line plots)
        :param str scale: plot scale: 'lin','sqrt','log','loglog'
        :param float fuzz: a minimum value for log scale to keep the image contrast reasonable (used only for 'image', scale='log')
        :param \*\*kwargs: is sent on to dimg.show in the kind='image' case
        """
        self.plot(**kwargs)

class InfoMatrix(np.matrix,InfoArray):
    """
    A sub class of `numpy.matrix <http://docs.scipy.org/doc/numpy-1.10.1/reference/generated/numpy.matrix.html>`_, with useful additional attributes like **name** and **units**.

    (See `numpy array subclassing <http://docs.scipy.org/doc/numpy-1.10.1/user/basics.subclassing.html>`_)
        
    to use::
    
        a = InfoMatrix(np.zeros((2,2)),name='blah')
        a.units = ...
    
    InfoArrays are containers in that they can contain any number of attributes. Useful ones are:
    
    :ivar str name: The name of the array
    :ivar str units: The units of the values in the array
    :ivar float dx: The physical size of one pixel in the array
    :ivar str dx_units: The units of the pixel size
    :ivar list axis_names: A list of strings containing the axis names
        The first and second are the horizontal and vertical axes correspondingly.
        An optionall third is the radial (for radial average plots).
    
    """
    def __new__(cls, input_array=[], name='',**kwargs):
        obj = np.asarray(input_array).view(cls)
        if type(input_array) is InfoArray:
            copyattrs(input_array,obj)
            setattr(obj,'name',name)
                
        else:
            obj.name = name
            obj.units = ''

        obj.__dict__.update(kwargs)
        return obj
    
    def __array_finalize__(self, obj):
        if obj is None: return
        if isinstance(obj,InfoMatrix):
            copyattrs(obj,self)
        
    def copy(self):
        """Make a deep copy of the InfoMatrix
        """
        obj = InfoMatrix(self.view(np.ndarray).copy())
        copyattrs(self,obj)
        return obj
    
    def pprint(self,full=False):
        """Pretty-print the InfoMatrix. This consists
        of reporting on the object's attributes.
        
        :param boolean full: if True, **pprint** also prints out the contents of the array.
            Default is to print only the "header" (attributes) information.

        """
        print self._pprint(full=full)
        
    def _pprint(self,print_width=40,aslist=False,rshort=False,full=False):
        cr = '\n'
        bling = 50*'='
        rs = []
        rs.append(bling)
        if hasattr(self,'longName'):
            rs.append(self.longName)
        else:
            if hasattr(self,'name') and self.name is not None:
                rs.append(self.name)
        rs.append(str(type(self)))
        rs.append(bling)
        rs.append('shape: '+str(self.shape))
        if rshort:
            if aslist:
                return rs
            else:
                rs = '\n'.join(rs)
                return rs
        if full:
            rs.append(str(self))
        tab = ' '*4
        d = self.__dict__
        keys = sorted(d.keys())
        for key in keys:
            if key.endswith('_units'): continue
            val = d[key]
            units = None
            if isinstance(val,(str,unicode)): pv = "'"+val+"'"
            else: pv = val.__repr__()
            #else: pv = str(val)
            if len(pv) > print_width:
                pv = str(val.__class__)
                if isinstance(val,(np.ndarray,np.matrix)):
                    pv += ' '+str(val.shape)
            if key+'_units' in keys:
                units = d[key+'_units']
            line = key+':'+tab+pv
            if units is None:
                pass
            else:
                line += ' '+units
            if isinstance(val,(InfoArray,InfoMatrix)):
                if (hasattr(val,'longName')):
                    line += " '"+val.longName+"'"
                else:
                    line += " '"+val.name+"'"
            else:
                pass
            rs.append(line)
        rs.append(bling)
        if aslist:
            return rs
        else:
            rs = '\n'.join(rs)
            return rs
       
    def plot(self,kind='image',fontsize=14,grid=True,oplot=False,scale='lin',fuzz=1e-9,**kwargs):
        """Display the InfoMatrix data as an image or lineout plot
        
        :param str kind: plot kind: 'image','lineout',or 'radial'
        :param int fontsize: for the titles, axis labels, and tick labels
        :param boolean grid: draw axis grid
        :param boolean oplot: whether or not to plot over a prevous graph (only for line plots)
        :param str scale: plot scale: 'lin','sqrt','log','loglog'
        :param float fuzz: a minimum value for log scale to keep the image contrast reasonable (used only for 'image', scale='log')
        :param \*\*kwargs: is sent on to dimg.show in the kind='image' case
        """
        scales = ['lin','sqrt','log','loglog']
        assert scale in scales,'scale must be one of '+scales
        n,m = self.shape
        if isinstance(self,np.matrix):
            valid_kinds = ['image']
            necessary_keys = set(['name'])
        else:
            valid_kinds = ['image','lineout','radial']
            necessary_keys = set(['units','dx','dx_units','name'])
        assert kind in valid_kinds,'kind must be one of '+valid_kinds
        keys = self.__dict__.keys()
        assert necessary_keys.issubset(set(keys)),'Object must have attributes '+str(list(necessary_keys))
        if hasattr(self,'longName'):
            name = self.longName
        else:
            name = self.name
        if isinstance(self,np.matrix):
            x0,y0 = 0,0
            if (hasattr(self,'axis_names')):
                xlabel = self.axis_names[1]
                ylabel = self.axis_names[0]
            else:
                xlabel = 'column index'
                ylabel = 'row index'
            origin = 'upper'
            xlim = [x0,x0+n]
            ylim = [y0+n,y0]
        else:
            dx = self.dx
            dx_units = self.dx_units
            x0,y0 = -(float(m)/2)*dx-dx/2, -(float(n)/2)*dx-dx/2
            origin = 'lower'
            xlim = [x0,x0+m*dx]
            ylim = [y0,y0+n*dx]
            if 'dy_units' in keys:
                dy_units = self.dy_units
            else:
                dy_units = self.dx_units
            if 'axis_names' in keys:
                xlabel = self.axis_names[0]+', '+dx_units
                ylabel = self.axis_names[1]+', '+dy_units
                if len(self.axis_names)>2:
                    rlabel = self.axis_names[2]+', '+dx_units
                else:
                    rlabel = dx_units
            else:
                xlabel = dx_units
                ylabel = dy_units
                rlabel = dx_units
        
        data = np.array(self)
        if np.iscomplex(data).any():
            print '<Object.plot> data is COMPLEX - showing the absolute value'
            data = np.abs(data)
            name = name+' (ABSOLUTE VALUE)'
        
        if not oplot and kind is not 'image':
            plt.figure()
        
        name += ', '+self.units
        
        if kind == 'radial':
            name += ' radial average'
            x = np.arange(xlim[0],xlim[1],dx)
            y = np.arange(ylim[0],ylim[1],dx)
            x,y = np.meshgrid(x,y)
            r = np.sqrt(x**2+y**2)
            bins = np.arange(0,(n/2)*dx,dx)
            i = np.digitize(r.flatten(),bins)
            c = np.bincount(i)
            q = np.zeros(n/2+1)
            for j,y in zip(i,data.flatten()):
                q[j] += y
            q /= c
            q = q[:-1]
            plt.plot(bins,q)
            if not oplot:
                ax = plt.gca()
                ax.set_xlabel(rlabel,fontsize=fontsize)
                ax.set_ylabel(self.units,fontsize=fontsize)
                ax.tick_params(axis='both',which='major',labelsize=fontsize,direction='out',length=6,width=1,
                               labeltop=False,labelbottom=True,labelleft=True,labelright=False)
                plt.title(name,y=1.04)
            if scale == 'log':
                ax.set_yscale('log')
            if scale == 'loglog':
                ax.set_xscale('log')
                ax.set_yscale('log')
                
        if kind == 'lineout':
            y = data[n/2,:]
            x = np.arange(xlim[0],xlim[1],dx)
            plt.plot(x,y)
            if not oplot:
                ax = plt.gca()
                ax.set_xlabel(xlabel,fontsize=fontsize)
                ax.set_ylabel(self.units,fontsize=fontsize)
                ax.tick_params(axis='both',which='major',labelsize=fontsize,direction='out',length=6,width=1,
                               labeltop=False,labelbottom=True,labelleft=True,labelright=False)
                plt.title(name,y=1.04)
            if scale == 'log':
                ax.set_yscale('log')

        if kind == 'image':
            if scale == 'log':
                data = np.log10(np.clip(np.abs(data),fuzz,np.inf))
            if scale == 'sqrt':
                data = np.sqrt(np.abs(data))
            dimg.show(data,extent = xlim+ylim,origin=origin,title=name,**kwargs)
            ax = plt.gca()
            if not oplot:
                ax.set_xlabel(xlabel,fontsize=fontsize)
                ax.set_ylabel(ylabel,fontsize=fontsize)
                ax.tick_params(axis='both',which='major',labelsize=fontsize,direction='out',length=6,width=1,
                               labeltop=True,labelbottom=True,labelleft=True,labelright=True)
                plt.title(name,y=1.08)

        if not oplot:
            ax.tick_params(axis='both',which='minor',direction='out',length=4,width=1)
            plt.minorticks_on()
        
        plt.grid(grid)
        plt.draw()
        
    def graph(self,**kwargs):
        kwargs['kind'] = 'lineout'
        self.plot(**kwargs)
        
    def display(self,**kwargs):
        kwargs['kind'] = 'image'
        self.plot(**kwargs)
        
    def show(self,**kwargs):
        self.plot(**kwargs)

if not ufloat_imported:
    class ufloat(float):
        """a float that can have units and other attributes
        """
        def __new__(cls,value,unit=None,doc=None):
            """create a float with a value and optional unit.
            
            :param float value: the floating point value
            :param str unit: the unit. must be a member of :py:data:`common_units.units <common_units.units>`
            
            See :mod:`common_units`
            """
            f = super(ufloat, cls).__new__(cls,value)
            if unit is not None:
                f.unit = unit
            if doc is not None:
                f.doc = doc
            return f
    
        def __repr__(self):
            r = super(ufloat,self).__repr__()
            if hasattr(self,'unit'):
                r += ' '+ self.unit
                if hasattr(self,'units_wavelength'):
                    r += ' at %r wavelength'%self.units_wavelength
            return r
        
        def pprint(self):
            '''pretty-print the object
            '''
            pprint(self)
        
        def to_units(self,new_unit,lam=None):
            '''return a ufloat with units converted
            '''
            if hasattr(self,'unit'):
                if self.unit == 'radians':
                    fac = self.units_wavelength.to_units('meters')/(2.*np.pi)
                else:
                    fac = units[self.unit]
            else:
                fac = 1.
            if new_unit == 'radians':
                fac /= lam.to_units('meters')/(2.*np.pi)
                r = ufloat(float(self)*fac,'radians')
                r.units_wavelength = lam
                return r
            else:
                fac /= units[new_unit]
                return ufloat(float(self)*fac,new_unit)
        
        def to_float(self):
            '''return a float whose value is scaled by the units
            '''
            if hasattr(self,'unit'):
                return float(self)*units[self.unit]
            else:
                return float(self)
            
        def asfits(self,sep=','):
            '''generate FITS card fields from the value, units, and doc attributes
            
            :param str sep: separator between units and doc in the comment
            
            returns (value, comment) tupple
            '''
            val = float(self)
            comment = ''
            if hasattr(self,'unit'):
                comment += self.unit
            if hasattr(self,'doc'):
                comment += '%s %s'%(sep,self.doc)
            return val,comment
        
        def equal(self,other):
            '''test equality of two :class:`ufloat` objects. This tests
            the numerical equality after accounting for the units
            
            :param ufloat other: the :class:`ufloat` object to compare self to
            
            Note that direct comparison to a (unitless) float is forbidden. This is to force
            explicit consideration of the units.
            '''
            if type(other) is float:
                print 'cannot test equality of ufloat to float'
                raise Exception
            return np.isclose(self.to_float(),other.to_float())
        
        def __eq__(self,other):
            '''check equality with this binary operator. For example
            
            .. code-block:: python
            
                a = ufloat(5.0,'mm')
                b = ufloat(0.5,'cm')
                a == b   <-- True
            
            :param ufloat other: the :class:`ufloat` object to compare self to
            
            Note that direct comparison to a (unitless) float is forbidden. This is to force
            explicit consideration of the units.        
            '''
            return self.equal(other)
    
    def as_ufloat(v,unit):
        if isinstance(v,ufloat):
            return v.to_units(unit)
        else:
            return ufloat(v,unit)

def makeFitsHeader(obj,attr=None):
    """helper function to make a fits header given an object, or
    an attribute within an object.
    Object can be any python object that has attributes. Attributes
    are screened so as to include only FITS header-file representable
    itmes like strings, floats, booleans. The :class:`ufloat` type is handled
    appropriately.
    
    :param object obj: any object
    :param str attr: the object attribute to save as data,
                    if None, assumes the object is a numpy.ndarray
                    or :class:`InfoArray`
    
    returns a pyfits header defintion unit (HDU)
    """
    if attr is None:
        data = obj
    else:
        data = getattr(obj,attr)
    try:
        d = obj.__dict__
    except:
        d = {}
    keys = sorted(d.keys())
    rkeys = {}
    row_list = []
    for key in keys:
        if not (key.endswith('_doc') or key.endswith('_units')):
            fkey = key.replace('_','')[:8].upper()  # generate an upper case key of 8 chars
            if fkey in [x['fkey'] for x in row_list]: # make sure key is unique
                count = 0
                if fkey in rkeys:
                    count = rkeys[fkey]
                rkeys[fkey] = count+1
                fkey = '%s%02d'%(fkey[:6],count+1) # if not unique, tag with a number
            val,comment = d[key],key
            if type(val) not in [int,float,str,bool]:
                if isinstance(val,ufloat):
                    val,more_comment = val.asfits()
                    comment += ', '+ more_comment
                else:
                    continue
            if type(val) is float and val == float('Infinity'):
                val = 'Infinity'
            #comment = key
            if key+'_units' in keys:
                comment += ', '+d[key+'_units']
            if key+'_doc' in keys:
                comment += ', '+d[key+'_doc']                
            row_list.append({'fkey':fkey,'val':val,'comment':comment})
    hdu = pyfits.PrimaryHDU(data)
    if hasattr(data,'header'):
        hdu.header = data.header
    for r in row_list:
        hdu.header.append((r['fkey'],r['val'],r['comment']))
    return hdu

def play(a,title=None,labels=None,**kwargs):
    '''play a movie of the list of InfoArrays.
    Use the 'name' and 'label' parameters of each InfoArray as the title above each frame,
    unless overridden by 'title' and 'labels' in the arguments
    '''
    assert isinstance(a,list)
    epl()
    
    if title is not None:
        assert isinstance(title,str)
        kwargs['label'] = title
    else:
        name = a[0].name
        if np.array([x.name == name for x in a]).all():
            kwargs['label'] = name
        else:
            kwargs['label'] = ''

    fallback_labels = ['%s frame %d'%(x.name,k) for k,x in enumerate(a)]
        
    if labels is not None:
        assert isinstance(labels,list)
        if len(labels) == 0: labels = ['']*len(a)
        assert np.array([isinstance(x,str) for x in labels]).all()
        kwargs['times'] = labels
    else:
        if np.array([hasattr(x,'label') for x in a]).all():
            labels = [x.label for x in a]
            kwargs['times'] = labels
        else:
            kwargs['times'] = fallback_labels

    dimg.play(a,**kwargs)

def test(show=False):
    a = InfoArray([[1.,2],[3,4]],name='Test',dx=1,dx_units='DN')
    a.units = 'integers'
    a.pprint()
    if show: a.show()
    ac = a.copy()
    ac[0,0] = 2.
    assert ((ac - a) == InfoArray([[1.,0],[0,0]])).all()
    
    b = InfoMatrix([[1.,2],[3,4]],name='TestMatrix')
    b.units = 'integers'
    b.pprint()
    if show: b.show()
    print 'passed'
    
def test2():
    '''check if ufloat works correctly
    '''
    from termcolor import colored
    good = colored('PASS','green')
    
    s = ufloat(5.0,'cm',doc='five centimeters')
    s.name = 's'
    s.pprint()
    q = s.to_units('mm')
    q.name = 'q'
    q.pprint()
    print 's = %r'%s
    print 'q = %r'%q
    try:
        print q == 50.
    except:
        print good
    assert s.equal(q); print good
    assert s == q; print good
    assert q == s; print good
    assert q.to_float() == 0.05; print good
    try:
        print 50. == s
    except:
        print good

def test3(verbose=False):
    '''check units conversion in InfoArrays
    '''
    global a,b,A,B
    from termcolor import colored
    import traceback
    good = colored('PASS','green')
    bad = colored('\nFAIL','red')
    u = units.u
    
    a = InfoArray([2.],units='cm')
    print a.__repr__()
    assert a.units == 'cm',bad; print good
    b = a.to_units('mm')
    print b.__repr__()
    assert b.units == 'mm' and b[0] == 20.,bad; print good
    a = InfoArray([3.])
    b = a.to_units('cm')
    print b.__repr__()
    assert b.units == 'cm' and b[0] == 3.,bad; print good
    a = InfoArray([3.+1.j],units='cm')
    b = a.to_units('mm')
    print b.__repr__()
    assert b.units == 'mm' and b[0] == 30.+10.j,bad; print good
    a = InfoArray([1])
    a = a.to_units('cm',inplace=True)
    print a.__repr__()
    assert a.units == 'cm' and a[0] == 1,bad; print good
    A = InfoArray([2.],units='radian',wavelength=ufloat(1.,'micron'))
    print A.__repr__()
    B = A.to_units('nm')
    print B.__repr__()
    assert B.units == 'nm' and B.wavelength is None and np.isclose(B[0],318.30988618),bad; print good
    A = B.to_units('waves',wavelength=1.*u.micron)
    print A.__repr__()
    try:
        if verbose: print '\n***This should produce an error***\n'
        A = B.to_units('waves')
        print bad
    except:
        if verbose: traceback.print_exc()
        print good
    try:
        if verbose: print '\n***This should produce an error***\n'
        A = B.to_units('waves',wavelength=1.*u.radian)
        print bad
    except:
        if verbose: traceback.print_exc()
        print good

def test4():
    '''check the fits writing
    Did the ufloat get the correct header card?
    '''
    import warnings
    H = InfoArray([1.],
                  name='H',
                  units = 'CCD pixels per nm',
                  doc = 'wfs slopes response to Zernikes',
                  r0 = ufloat(5.0,'cm'),)
    bling = '*'*20
    print bling+' original H '+bling
    H.pprint()
    print ''
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        H.writefits('tmp_H.fits',clobber=True)

    Hr = InfoArray.fromFits('tmp_H.fits')
    print bling+' H read back in '+bling
    Hr.pprint()
    print ''
    print bling+' H.header '+bling
    print Hr.header.__repr__()
    print 'passed'

def test5():
    '''check that units play nice
    '''
    from sauce2.common_units import u
    A = InfoArray([1.,2.],units='meters')
    B = A.copy()
    B.units = ''
    A[:] *= 1.
    A+4.*u.cm*B+A
    try:
        C = A+B
        success = True
    except Exception as e:
        success = False
    
    if success:
        raise Exception,"C=A+B test should have failed but it didn't"
    
    b = 2.*u.micron
    Q = np.ones((4,4))
    W = Q*b
    
    try:
        B = A*b
        success = True
    except:
        success = False
    
    if success:
        raise Exception,"B=A*b test should have failed but it didn't"
    
    C = A*B
    print 'passed'
