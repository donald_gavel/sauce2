# scope.py
"""
============
Oscilloscope
============

Emulates an oscilloscope.
"""
import numpy as np
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import matplotlib.animation as animation

plt.ion()

def bc(w,s):
    xi = 1./np.sqrt(1+s**2)
    b = -2*xi*w
    c = w**2
    return b,c,-b/2 + np.sqrt(-(b/2)**2+c)*1j

class Scope(object):
    def __init__(self, ax, maxt=10., dt=0.1, scroll = True):
        self.ax = ax
        self.dt = dt
        self.maxt = maxt
        self.n = round(maxt/dt)
        self.state = [0.]*3
        self.t = 0.
        self.tdata = np.arange(0,self.n*self.dt,self.dt)
        self.ydata = [0]*self.n
        self.line = Line2D(self.tdata, self.ydata)
        self.ax.add_line(self.line)
        self.ax.set_ylim(-1.1, 1.1)
        self.ax.set_xlim(0, self.maxt)
        self.scroll = scroll
        self.color = 0
        self.p = 0 # pointer
        w = 0.95
        s = 2.
        xi = 1./np.sqrt(1+s**2)
        b = -2*xi*w
        c = w**2
        self.b = b
        self.c = c

    def update(self, w):
        colors = ['blue','green']
        if self.p != -1:
            self.p += 1
        if self.p < self.n and self.p > -1:
            pass
        else:  # scroll the arrays
            if self.scroll:
                #self.tdata = np.roll(self.tdata,-1) # [self.tdata[-1]]
                #self.tdata[-1] = self.t
                self.ydata = np.roll(self.ydata,-1) # [self.ydata[-1]]
                self.p = -1
                #self.ax.set_xlim(self.tdata[0], self.tdata[0] + self.n*self.dt)
                #self.ax.figure.canvas.draw()
            else:
                self.p = self.p%self.n
                self.color = (self.color+1)%2
        self.state = np.roll(self.state,-1)
        self.state[-1] = - self.state[-3]*self.c - self.state[-2]*self.b + w
        self.ydata[self.p] = self.state[-1]
        self.t += self.dt
        self.line.set_data(self.tdata, self.ydata)
        self.line.set_color(colors[self.color])
        return self.line,


def emitter(p=0.2):
    'return a random value with probability p, else 0'
    while True:
        v = np.random.rand(1)
        if v > p:
            yield 0.
        else:
            yield np.random.rand(1)-0.5

def forever():
    '''always yield 1
    '''
    while True:
        yield 1

# pass a generator in "emitter" to produce data for the update func
def go():
    global ani, fig, ax, scope

    fig, ax = plt.subplots()
    scope = Scope(ax)
    plt.grid(True)
    ani = animation.FuncAnimation(fig, scope.update, emitter, interval=3, blit=True)

