Wavefront Sensor
****************
.. automodule:: wfs

Classes
-------

.. autoclass:: Wfs
    :members:
    :private-members:
    :special-members:
    
..
..    :members: __init__, ccd_image, ccd_image_gpu, show, pprint, set_gs_brightness,
..              gen_zmodes, gen_fmodes, gsense, map_slopes,
..              mode_fit, recon, recon_prep, save_telem

Functions
---------

.. autofunction:: tilt

.. autofunction:: focus

.. autofunction:: makeFitsHeader

Test functions
--------------
To run all the tests:

.. code-block:: bash

    pytest -v -s sauce2/tests

.. automodule:: sauce2.tests.test_wfs

.. autofunction:: test

.. autofunction:: test2

.. autofunction:: test3

.. autofunction:: testa

.. autofunction:: testb

.. autofunction:: testr

.. autofunction:: testbr

.. autofunction:: testanal
