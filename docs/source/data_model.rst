.. _data_model:

Data Model
==========

Input Data
----------

Sauce2 uses a number of data files to define the
simulation. These are stored as FITS file in the
directory **data** in the distribution.

Output Data
-----------

FITS
^^^^

Data produced by sauce2 are typically arrays of telemetry data.
These arrays are, in general, stored on disk as FITS data files.
They can be in any directory. A simple database of the
telemetry files in a given directory
can be established with (unix command):

.. code::

    flog
    
which forms a csv-file out of the FITS Headers. To read this
back in to a :py:class:`pandas.DataFrame`:

.. code::

    >>> df = pandas.read_csv('log.csv',sep = ';')

In this way, the entire dataset is defined by the directory, which
contain the FITS files and the database of data attributes, log.csv.

HDF5
^^^^

There is also an option also to store output data in HDF files.
This is work in progress, but the idea is all telemetry data
are stored in one HDF file, with a database of dataset attributes
also stored as a :py:class:`pandas.DataFrame` within the HDF file.

Training Sets
-------------

Datasets for :index:`machine learning` are stored in standard
"Training", "Validation", and "Test" sets as suitable for
input to `Tensorflow <https://www.tensorflow.org>`_ and :py:mod:`pytorch <torch>` packages.
This is work in progress.