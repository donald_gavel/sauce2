Info Array
**********

.. automodule:: info_array

Classes
-------

.. autoclass:: InfoArray
    :members:

.. autoclass:: InfoMatrix
    :members:

Functions
---------

.. autofunction:: copyattrs

.. autofunction:: makeFitsHeader
