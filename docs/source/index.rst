.. sauce documentation master file, created by
   sphinx-quickstart on Sat Jan  6 17:55:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. |br| raw:: html

   <br />
   
Sauce2
======

.. py:module:: sauce2

The **Sauce2** project applies machine learning to adaptive optics. The
goal is a robotic, self-optimizing AO system.

.. toctree::
   :maxdepth: 2
   
   description
   simulation
   neural_net
   data_model

.. toctree::
    :hidden:
    
    NOTES

A project description and the source code is available at |br|
https://bitbucket.org/donald_gavel/sauce2

:ref:`notes`