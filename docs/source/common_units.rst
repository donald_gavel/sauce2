Common Units
============

.. py:module:: sauce2

.. automodule:: common_units

Classes
-------

Units
~~~~~

.. autoclass:: Units
    :members:
    
ufloat
~~~~~~

.. autoclass:: ufloat
    :members:
    :special-members:

.. autofunction:: as_ufloat

Functions
---------

.. autofunction:: load_units

.. autofunction:: load_ufloats

Data
----

.. autodata:: u

.. autodata:: units
