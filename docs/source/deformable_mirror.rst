Deformable Mirror
*****************

.. automodule:: dm
    :members:
    :private-members:
    :exclude-members: write, c_write, test_c_write
    
    