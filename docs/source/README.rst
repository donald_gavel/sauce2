

.. contents::
    :local:

Goals
-----

The **Sauce2** project applies machine learning to adaptive optics. The
goal is a robotic, self-optimizing AO wavefront corrector.

**Sauce2** will have

-  a simulator, which generates atmospheric aberrations, calculates
   light propagation, and wraps the real-time AO system controller.
-  a machine learning framework that

   -  identifies the atmospheric parameters (r0, wind, etc.) and adjusts
      the controller accordingly
   -  replicates the function of the real-time controller

For success in machine learning it is important to provide lots of
training data and be able to run the simulation quickly. We plan to
obtain orders of magnitude improvement in simulator speed by using a GPU
for the compute-intensive operations both in the simulator and the
neural nets. Ultimately the goal will be to encode the real time
controller on the GPU as well. There should still be a clear separation
between the atmosphere/optics simulation part and the real-time
controller part so that the controller part is easily ported to a live
system.

Phase 1
~~~~~~~

We generate a phase screen and resulting image on the wavefront sensor
CCD, using GPU code. The GPU will run highly parallel code relying on
indirect data referencing for low latency data transfer. The indirect
reference maps are pre-generated using Python objects that define the
problem geometry. Also, we will implement the DM and phase correction
step in the GPU.

Phase 2
~~~~~~~

Implement the linear matrix multiply reconstructor in the GPU. Close the
loop for a full simulation.

Phase 3
~~~~~~~

Set up neural nets for learning r0, wind speed, and optimal turning
parameters (frame rate, loop gains) from AO telemetry data. Create
training sets and train the neural net. Create validation sets and test
the self-tuning performance.

Phase 4
~~~~~~~

Implement a neural net reconstructor for the feedback control. Train and
validate using the simulator.

Getting and Installing the Code
-------------------------------

Requirements:

- Python2.7
- OpenCL

Install directly, using pip

.. code::

    pip install sauce2

or, clone from the git repository
https://bitbucket.org/donald_gavel/sauce2
and install locally (we suggest doing this in a virtualenv)::

    cd sauce2
    pip install -e .

If you've downloaded the sources, you can run the tests::

    pytest -s -v sauce2/tests

**First run**

To see some nice movie graphics::

    python
    >>> from sauce2 import wfs
    >>> wfs.testb(dt=10., comp='GPU', show=True)


Optional: Running in a Jupyter notebook
---------------------------------------

There is a Jupyter notebook file, WFS\_imaging\_using\_GPU.ipynb,
distributed with the package. This has experiments with the GPU that led
to developing the WFS imaging model.

Troubles?
---------

No Python 2?
~~~~~~~~~~~~

You get a message:

    sauce2 requires Python '>=2.7,<3.0' but the running Python is 3.6.4

**Reason** You are using Python3, or the pip for version 3. Sauce2
only works with Python version 2.7

**Work-around 1** Install and use Python2.7 and the pip for Python 2.

**Work-around 2** Create and work within a virtual environment for
Python 2

.. code:: bash

    virtualenv ~/virtual
    source ~/virtual/bin/activate
    cd sauce2
    pip install -e .

See https://virtualenv.pypa.io/en/stable/

Virtual environment on a mac - no graphics?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You get the message

    "RuntimeError: Python is not installed as a framework. The Mac OS X
    backend will not be able to function correctly if Python is not
    installed as a framework"

**Reason** Graphics, specifically Matplotlib, will have trouble if
Python (the Pyton in the virtual environment) was not installed as a
*framework*, which is often the default case.

**Work-around 1** Use the system python, which is installed as a
framework, after setting the PYTHONHOME environment variable.

.. code:: bash

    export PYTHONHOME=$HOME/[virtual dir]
    /opt/local/bin/python

See:
https://blog.rousek.name/2015/11/29/adventure-with-matplotlib-virtualenv-and-macosx/

**Work-around 2** Avoid the use of MacOS backend entirely and use the
TkAgg backend instead. This requires the Tkinter library, which is
usually not installed with virtualenv Pytons. But fixing that is easy if
you know the location of the system Tkinter library.

.. code:: bash

       cd [virtual dir]/lib/python2.7/site-package
       ln -s /opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/_tkinter.so

See: http://lab.openpolis.it/using-tkinter-inside-a-virtualenv.html

If you don't have the file \_tkinter.so, then you need to install
tkinter:

.. code:: bash

    port install py27-tkinter

**Work-around 3** You can run entirely within a Jupyter notebook. Run

.. code:: python

    %matplotlib notebook

as the first command in the notebook.

OpenCL
~~~~~~

No headers
^^^^^^^^^^

When installing OpenCL, you get the message

    fatal error: CL/cl.h: No such file or directory

or

    /usr/bin/ld: cannot find -lOpenCL

**Reason** You need to install the OpenCL headers and libraries:
(Ubuntu example)

.. code:: bash

    sudo apt install ocl-icd-libopencl1
    sudo apt install opencl-headers
    sudo apt install clinfo
    sudo apt install ocl-icd-opencl-dev

See
https://askubuntu.com/questions/850281/opencl-on-ubuntu-16-04-intel-sandy-bridge-cpu

Wrong library
^^^^^^^^^^^^^

When you try to run the tests in python, you get the message

    ImportError:
    /usr/local/lib/python2.7/dist-packages/pyopencl/\_cffi.so: symbol
    clSetKernelArgSVMPointer, version OPENCL\_2.0 not defined in file
    libOpenCL.so.1 with link time reference

**Reason** You either haven't installed the GPU hardware drivers, or
the system is looking for their libraries in the wrong place.

**Fix 1** Install the drivers using the instructions for your
particular GPU model. Also, see https://wiki.tiker.net/OpenCLHowTo

**Fix 2** I ran into a case where there was a wrong library having the
same name, libOpenCL.so.1, associated with CUDA-8, and it was on the
dynamic loader path ahead of the correct library, associated with the
generic OpenCL. I deleted the one I didn't want (but it keeps comming
back!). You can adjust the files in /etc/ld.so.conf.d then run ldconfig
to remove the duplication within the load path. I didn't want to mess
with this, so I appended to the user's LD\_LIBRARY\_PATH:

.. code:: bash

    export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/:$LD_LIBRARY_PATH

to point to the correct library at
``/usr/lib/x86_64-linux-gnu/libOpenCL.so``.
