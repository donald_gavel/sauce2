.. _notes:

=====================================
Notes on generating the documentation
=====================================

To generate the documentation using Sphinx:

.. code-block:: bash

    cd sauce2/docs
    make html

Nursing along
-------------

I find Sphinx to be poorly documented and lacking in examples.
It is particularly bad about cross-reference links failing
silently. Here are my suggestions on how to nurse
the build.

Imports
~~~~~~~

Modules get imported during a docs build. This sometimes causes issues.
To prevent Sphinx from importing a module, wrap with

.. code-block:: python

    if not 'sphinx' in sys.modules:
        import ...

The scipy module gives trouble and so is wrapped (see atmos.py)
The :class:`numpy.ndarray` class is a superclass of :class:`InfoArrays <info_array.InfoArray>`, and
this gives Sphinx headaches. I defined a dummy_np
module and imported it as np in a wrapper (see info_array.py)

Cross Reference
~~~~~~~~~~~~~~~

Do not put "sauce2...." in any of the .. automodule: targets.
Otherwise :class:`wfs.Tweeter` reference links do not work.
They would have to be written
:class:`sauce2.wfs.Tweeter` in every link.

New Module
~~~~~~~~~~

You must rebuild the entire docs if you add another module
(make clean). Otherwise the cross-links dont all work and
contents sidebar doesn't have all the modules.

Code blocs in code documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You cannot use .. code-block inside the documentation strings
of source code (!!). This only works in the .rst files.
Instead, use the double-colon and indent approach to
setting off blocks of code::

    for example::
    
        some code
        some code

OOPS! Yes you can. You need to have TWO colons after .. code-block::

    .. code-block:: python
    
        some code
        some code

Intersphinx
-----------

Intersphinx enables inserting live cross-references to modules
outside of the :mod:`sauce2` module.

The intersphinx mapping is defined in **source/conf.py**

intersphinx_mapping is a dictionary containing references to "inventory files" of a specific
format which must be located at the target url (first entry in the tuple). I have no idea what the key
in each dictionary entry
(so called "unique identifier") does; it seems to be ignored entirely. For example,
an intersphinx_mapping entry::

    'pytorch': ('http://pytorch.org/docs/0.3.0/', None)
    
**does not** mean I can refer
to ``:mod:`pytorch```, I still have to use ``:mod:`torch``` which makes :mod:`torch`. The work around is
``:mod:`pytorch <torch>``` which makes :mod:`pytorch <torch>`. I guess this behavior has
some logic to it, being that torch is
the actual name of the module, but the point is the intersphinx documentation
doesn't clearly explain the meaning of "unique identifier" and whether the value
of the key means anything.
Oddly it appears it can be set to *anything*.

Sphinx references
-----------------
* Sphinx reference `<http://www.sphinx-doc.org/en/stable/markup/inline.html#referencing-downloadable-files>`_
* ReST reference `<https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#headings>`_
* Sphinx-domain special markups (:mod:, :class:, :func:, :meth:, :attr:, etc.) `<http://www.sphinx-doc.org/en/stable/domains.html#role-py:class>`_
* intersphinx `<http://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html>`_