.. _notes:

=======================================
Notes on Sphinx-generated documentation
=======================================

To generate the documentation:

.. code-block:: bash

    make html

Nursing along
-------------

Imports
~~~~~~~

Modules get imported during a docs build. This sometimes causes issues.
To prevent Sphinx from importing a module, wrap with

.. code-block:: python

    if not 'sphinx' in sys.modules:
        import ...

The scipy module gives trouble and so is wrapped (see atmos.py)
The :class:`numpy.ndarray` class is a superclass of :class:`InfoArrays <info_array.InfoArray>`, and
this gives Sphinx headaches. I defined a dummy_np
module and imported it as np in a wrapper (see info_array.py)

Cross Reference
~~~~~~~~~~~~~~~

Do not put "sauce2...." in any of the .. automodule: targets.
Otherwise :class:`wfs.Tweeter` reference links do not work.
They would have to be written
:class:`sauce2.wfs.Tweeter` in every link.

New Module
~~~~~~~~~~

You must rebuild the entire docs if you add another module
(make clean). Otherwise the cross-links dont all work and
contents sidebar doesn't have all the modules.

Intersphinx
-----------

Intersphinx enables inserting live cross-references to modules
outside of the :module:`sauce2` module.

The intersphinx mapping is defined in **source/conf.py**

Sphinx references
-----------------
* Sphinx reference `<http://www.sphinx-doc.org/en/stable/markup/inline.html#referencing-downloadable-files>`_
* ReST reference `<https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html#headings>`_
