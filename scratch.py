# https://stackoverflow.com/questions/4092927/generating-movie-from-python-without-saving-individual-frames-to-files
import matplotlib.animation as animation
import numpy as np
from pylab import *


dpi = 100

def ani_frame(q):
    q = normalize(q)
    N = len(q)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #ax.set_aspect('equal')
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    im = ax.imshow(q[0],cmap='gray',interpolation='nearest')
    im.set_clim([0,1])
    #fig.set_size_inches([5,5])


    tight_layout()


    def update_img(n):
        tmp = q[n]
        im.set_data(tmp)
        return im

    #legend(loc=0)
    ani = animation.FuncAnimation(fig,update_img,N,interval=30)
    writer = animation.writers['ffmpeg'](fps=30)

    ani.save('demo.mp4',writer=writer,dpi=dpi)
    return ani

def ani_frame2(q1,q2):
    q1 = normalize(q1)
    q2 = normalize(q2)
    N = len(q1)
    assert N == len(q2)
    fig = plt.figure()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222)
    #ax.set_aspect('equal')
    for ax in [ax1,ax2]:
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

    im1 = ax1.imshow(q1[0],cmap='gray',interpolation='nearest')
    im2 = ax2.imshow(q2[0],cmap='gray',interpolation='nearest')
    im1.set_clim([0,1])
    im2.set_clim([0,1])

    tight_layout(pad=0,h_pad=0,w_pad=0)

    def update_img(n):
        im1.set_data(q1[n])
        im2.set_data(q2[n])
        return (im1,im2)

    #legend(loc=0)
    ani = animation.FuncAnimation(fig,update_img,N,interval=30)
    writer = animation.writers['ffmpeg'](fps=30)

    ani.save('demo.mp4',writer=writer,dpi=dpi)
    return ani

def ani_frame3(sh,ch,save_file = 'demo.mp4'):
    '''generate an animation of the screens and ccd images
    with a plot of wf rms along the bottom
    See:
    https://scientificallysound.org/2016/06/09/matplotlib-how-to-plot-subplots-of-unequal-sizes/
    '''
    N = len(sh)
    assert N == len(ch)
    fig = plt.figure(figsize=(5,5.*(3./4.)))
    ax1 = plt.subplot2grid((3,2),(0,0),rowspan=2)
    ax2 = plt.subplot2grid((3,2),(0,1),rowspan=2)
    ax3 = plt.subplot2grid((3,2),(2,0),colspan=2)
    ax1.get_xaxis().set_visible(False)
    ax1.get_yaxis().set_visible(False)
    ax2.get_xaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    
    gs1 = ax1.get_subplotspec().get_gridspec()
    gs2 = ax2.get_subplotspec().get_gridspec()
    gs3 = ax3.get_subplotspec().get_gridspec()
    gs1.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
    gs2.update(left=0,right=1,top=1,bottom=0,hspace=0,wspace=0)
    gs3.update(top=.7)
    im1 = ax1.imshow(sh[0],cmap='gray')
    im2 = ax2.imshow(ch[0][40:120,40:120],cmap='gray')
    rms = [s.std() for s in sh]
    line, = ax3.plot(rms)
    plt.ylabel('RMS, nm')
    ax3.grid()
    
    def update_img(n):
        im1.set_data(sh[n])
        im2.set_data(ch[n][40:120,40:120])
        line.set_data(range(n),rms[0:n])
        return (im1,im2,line)
    
    ani = animation.FuncAnimation(fig,update_img,N,interval=30)
    writer = animation.writers['ffmpeg'](fps=30)

    ani.save(save_file,writer=writer,dpi=dpi)
    return ani

def rebin( a, newshape ):
        '''Rebin an array to a new shape.
        '''
        assert len(a.shape) == len(newshape)

        slices = [ slice(0,old, float(old)/new) for old,new in zip(a.shape,newshape) ]
        coordinates = mgrid[slices]
        indices = coordinates.astype('i')   #choose the biggest smaller integer index
        return a[tuple(indices)]

def normalize(q):
    '''normalize a list of arrays to range from 0 to 1
    '''
    mx = [float(qq.astype(np.float).max()) for qq in q]
    mn = [float(qq.astype(np.float).min()) for qq in q]
    mx = max(mx)
    mn = min(mn)
    return [(qq.astype(np.float)-mn)/(mx-mn) for qq in q]

 