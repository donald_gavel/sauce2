from setuptools import setup, find_packages
setup(
    name = 'sauce2',
    packages = find_packages(),
    version = '0.1.1',
    description = 'GPU based AO simulator',
    author = 'Donald Gavel',
    author_email = 'donald.gavel@gmail.com',
    url = 'https://bitbucket.org/donald_gavel/sauce2', # use the URL to the github repo
    keywords = [], # arbitrary keywords
    classifiers = [],
    include_package_data = True,
    install_requires = ['ConfigParser;python_version<"3"',
                        'configparser;python_version>="3"',
                        'matplotlib',
                        'numpy>=1.14',
                        'scipy',
                        'pandas',
                        'tqdm',
                        'termcolor',
                        'pprint',
                        'astropy',
                        'pyopencl',
                        'pytest',
                        'pyfft;python_version<"3"',
                        'reikna;python_version>="3"',
                        ],
)
