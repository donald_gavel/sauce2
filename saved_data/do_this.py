'''
Telemetry data simulation runs for open-loop wavefront sensor and
blowing atmospheric layer, variation of r0
Directory 'saved_data'
'''

from sauce2 import wfs,atmos
w = wfs.Wfs()
for r0 in [0.05,0.07,0.1,0.12,0.15]:
    for k in range(10):
        bs = atmos.BlowingScreen(n=w.N,du=w.du, r0 = r0)
        run_summary = w.simulate(bs,nt=4096)
        w.save_telemetry('Data_0000.fits',directory='saved_data')
