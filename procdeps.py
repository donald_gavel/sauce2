"""
procdeps will help find the dependencies of a package.
It prints a list of required packages that can be pasted in the "install_requires =" argument
to the setup(...) command in setup.py

Provide a list of supplied modules in the package so that these are not put in the requires list.

The routine first uses snakefood (sfood command) to generate a tree of included dependencies.
Then the list if filtered removing supplied modules and modules that are not available on PyPI
(such as bultin modules).

Copy the printed output into setup.py
"""

import os
import imp
from pprint import pprint
import requests
from termcolor import colored

# to create the deps.txt file:
# sfood sauce2 > deps.txt
os.system('sfood --external sauce2 > deps.txt')

supplied_modules = ['atmos','common_units','demo','demo_fft','dimg','dotdict','fits']
f = open('deps.txt')
vl = []
for line in f:
    u = eval(line)
    v = u[1][1]
    if '/' in v:
        continue
    if v.endswith('.so') or v.endswith('.py'):
        v=v[:-3]
    if v in supplied_modules:
        continue
    vl.append(v)
f.close()
q = sorted(list(set(vl)))
print 'original list'
pprint(q)
# check that they are on PyPI
# https://stackoverflow.com/questions/38158672/python3-pip-module-check-if-packages-exist-on-pypi
install_requires = []
for module_name in q:
    response = requests.get("http://pypi.python.org/pypi/{}/json".format(module_name))
    if response.status_code == 200:
        install_requires.append(module_name)
        print colored('%s available'%module_name,'green')
    else:
        print colored('can not get %s from PyPI'%module_name,'red')
print 'final list'
print('supplied_modules = [')
for m in supplied_modules:
    print('    %s,'%m)
print(']')
print('install_requires = [')
for m in install_requires:
    print('    %s,'%m)
print(']')
